# HedgeTools

HedgeTools is a plugin for QGIS designed to extract and characterize hedges, aiming to streamline field efforts by providing information about hedge health. 

It is developed by the landscape ecology laboratory [Dynafor](https://www.dynafor.fr/).

## Extraction

HedgeTools allows the extraction of tree cover and various vegetation shapes such as forests, groves, hedges, and lone trees. For this purpose, a raster with infrared and red bands is required, along with a Digital Elevation Model (DEM) and a Digital Surface Model (DSM).

## Data Preparation

From a polygon layer of hedges, HedgeTools creates various geometric representations. The median axis is generated to represent hedges as a linear feature. Point geometries are then created to indicate the start and end of hedges from a topological point of view. Finally, polygons are cut at each point to standardize hedge representation.

## Data Transformation

If the topological representation doesn't suit your needs, HedgeTools allows the user to change it based on various criteria: orientation, interface changes, constant length.

## Metrics

HedgeTools computes various metrics at different levels. There are metrics at the hedge level (length, width), context level (topographic position, distance to forests), landscape level (density), and network level (connectivity metrics).

## Documentation

Documentation will be released shortly to assist users in using the plugin.

## Bugs & Reports

HedgeTools is developed by a small team of junior developers. It may have various bugs and unexpected results. Please feel free to create issues on this [repository](https://forgemia.inra.fr/dynafor/webapps/eagle-hedges/-/issues).

## Installation

While HedgeTools aims to be in the QGIS official repository, for now, you can download the `hedge_tools` folder as a zip and add it manually to QGIS with the following steps:

**Plugins > Manage and Install Plugins… > Install from ZIP**

## License

HedgeTools is released under the MIT License. The license can be found in the `LICENSE.txt` file.

## Project Status

This project is relatively young. The team aims to enhance it and expand its functionalities in the following years.
