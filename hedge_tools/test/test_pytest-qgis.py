def test_method_0():
    """
    Test with geomorphon computation
    """
    polygons = utils.create_layer(self.polygons, copy_feat=True, 
                                    copy_field=True, data_provider="ogr", 
                                    path=os.path.join(self.workspace, "polygons_0.gpkg"))
    arcs = utils.create_layer(self.polygons, copy_feat=True, 
                                copy_field=True, data_provider="ogr", 
                                path=os.path.join(self.workspace, "arcs_0.gpkg"))
    nodes = utils.create_layer(self.polygons, copy_feat=True, 
                                copy_field=True, data_provider="ogr", 
                                path=os.path.join(self.workspace, "nodes_0.gpkg"))

    params = {"POLYGON": polygons,
                "ARC": arcs,
                "NODE": nodes,
                "DEM": self.dem,
                "USE_GEOMORPHON": 0}
    processing.run(self.alg_name, params)

    if "slope_pos" in polygons.fields().names():
        results = [f["slope_pos"] for f in polygons.getFeatures() if f["slope_pos"] != NULL]
    
    self.assertIn('slope_pos', polygons.fields().names())
    self.assertEqual(len(results), len(polygons.featureCount()))
