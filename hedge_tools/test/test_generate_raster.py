import unittest
from qgis.core import *
from qgis.analysis import QgsNativeAlgorithms

import sys

QgsApplication.setPrefixPath("/usr", True)
QgsApplication.processingRegistry().addProvider(QgsNativeAlgorithms())

processing_path = "/usr/share/qgis/python/plugins"
if processing_path not in sys.path:
    sys.path.append(processing_path)

qgs = QgsApplication([], False) # True for running GUI
qgs.initQgis()

# After qgis initialisation
# Do not sure if needed as processing is imported in ht_function
import processing # Not necessary if i used my wrapper in ht
from processing.core.Processing import Processing # Necessary to add other providers evenin ht (native, gdal, grass, saga)
Processing.initialize()

import sys
sys.path.append("/home/gabriel/hedge_tools/1_Qgis-plugin/eagle-hedges")
from hedge_tools.tools.classes import GenerateRaster as gR

class TestGenerateRaster(unittest.TestCase):

    def setUp(self):
        self.input_A = QgsRasterLayer("test/test_data/dem_5m.tif")
        self.input_B = QgsRasterLayer("test/test_data/dem_5m.tif")
        self.generate_raster = gR.GenerateRaster((self.input_A, 1), 
                                                 (self.input_B, 1))

    def test_define_nb_alg_default(self):
        nb_alg = self.generate_raster._define_nb_alg()
        self.assertEqual(nb_alg, 1)

    def test_define_nb_alg_target_res(self):
        nb_alg = self.generate_raster._define_nb_alg(target_res=10.0)
        self.assertEqual(nb_alg, 2)

    def test_define_nb_alg_compute_mask(self):
        nb_alg = self.generate_raster._define_nb_alg(compute_mask=True)
        self.assertEqual(nb_alg, 2)

    def test_define_nb_alg_compute_sieve(self):
        nb_alg = self.generate_raster._define_nb_alg(compute_sieve=True)
        self.assertEqual(nb_alg, 2)

    def test_define_nb_alg_extent_rectangle(self):
        extent = QgsRectangle(0, 0, 10, 10)
        nb_alg = self.generate_raster._define_nb_alg(extent=extent)
        self.assertEqual(nb_alg, 2)

    def test_define_nb_alg_extent_geometry(self):
        extent = QgsGeometry.fromWkt("POLYGON((0 0, 0 10, 10 10, 10 0, 0 0))")
        nb_alg = self.generate_raster._define_nb_alg(extent=extent)
        self.assertEqual(nb_alg, 2)

    def test_define_nb_alg_polygonize(self):
        nb_alg = self.generate_raster._define_nb_alg(polygonize=True)
        self.assertEqual(nb_alg, 2)

    def test_define_nb_alg_all_options(self):
        extent = QgsRectangle(0, 0, 10, 10)
        nb_alg = self.generate_raster._define_nb_alg(
            target_res=10.0,
            compute_mask=True,
            compute_sieve=True,
            extent=extent,
            polygonize=True
        )
        self.assertEqual(nb_alg, 6)
        extent = QgsRectangle(0, 0, 10, 10)
        nb_alg = self.generate_raster._define_nb_alg(
            target_res=10.0,
            compute_mask=False,
            compute_sieve=False,
            extent=extent,
            polygonize=False
        )
        self.assertEqual(nb_alg, 2)

    def test_define_nb_alg_with_mask(self):
        extent = QgsRectangle(0, 0, 10, 10)
        nb_alg = self.generate_raster._define_nb_alg(
            target_res=10.0,
            compute_mask=True,
            compute_sieve=False,
            extent=extent,
            polygonize=False
        )
        self.assertEqual(nb_alg, 3)

    def test_define_nb_alg_with_sieve(self):
        extent = QgsRectangle(0, 0, 10, 10)
        nb_alg = self.generate_raster._define_nb_alg(
            target_res=10.0,
            compute_mask=False,
            compute_sieve=True,
            extent=extent,
            polygonize=False
        )
        self.assertEqual(nb_alg, 4)

    def test_define_nb_alg_with_polygonize(self):
        extent = QgsRectangle(0, 0, 10, 10)
        nb_alg = self.generate_raster._define_nb_alg(
            target_res=10.0,
            compute_mask=False,
            compute_sieve=False,
            extent=extent,
            polygonize=True
        )
        self.assertEqual(nb_alg, 3)

if __name__ == '__main__':
    unittest.main()

    def test_define_nb_alg_extent_none(self):
        nb_alg = self.generate_raster._define_nb_alg(extent=None)
        self.assertEqual(nb_alg, 1)

    def test_define_nb_alg_extent_empty_geometry(self):
        extent = QgsGeometry()
        nb_alg = self.generate_raster._define_nb_alg(extent=extent)
        self.assertEqual(nb_alg, 1)

if __name__ == '__main__':    
    unittest.main()

