# coding=utf-8
"""Safe Translations Test.

.. note:: This program is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation; either version 2 of the License, or
     (at your option) any later version.

"""
import os
import unittest

from qgis.core import *
import processing
from hedge_tools.tools.vector import attribute_table as at
from hedge_tools.tools.vector import utils
from .utilities import get_qgis_app
QGIS_APP = get_qgis_app()

DATA_FOLDER = os.path.join(os.path.dirname(__file__), "test_data")

class RelativeOrientationTest(unittest.TestCase):
    """Test the relative orientation algorithm in Hedge tools"""

    def __init__(self):
        """
        Load data and algorithm name
        """
        super().__init__()
        self.workspace, self.folder_object = utils.create_temp_workspace(prefix="ht_test_")

        self.vector_layer = os.path.join(DATA_FOLDER, "topological.gpkg")
        self.polygons = QgsVectorLayer(self.vector_layer + "|layername=polygons", "polygons", "ogr")
        self.arcs = QgsVectorLayer(self.vector_layer + "|layername=arcs", "arcs", "ogr")
        self.nodes = QgsVectorLayer(self.vector_layer + "|layername=nodes", "nodes", "ogr")

        self.dem = QgsRasterLayer(os.path.join(DATA_FOLDER, "dem_5m.tif"), "dem", "gdal")

        self.alg_name = "hedgetools:relativeorientation"


    def test_method_0(self):
        """
        Test with geomorphon computation
        """
        polygons = utils.create_layer(self.polygons, copy_feat=True, 
                                      copy_field=True, data_provider="ogr", 
                                      path=os.path.join(self.workspace, "polygons_0.gpkg"))
        arcs = utils.create_layer(self.polygons, copy_feat=True, 
                                  copy_field=True, data_provider="ogr", 
                                  path=os.path.join(self.workspace, "arcs_0.gpkg"))
        nodes = utils.create_layer(self.polygons, copy_feat=True, 
                                   copy_field=True, data_provider="ogr", 
                                   path=os.path.join(self.workspace, "nodes_0.gpkg"))

        params = {"POLYGON": polygons,
                  "ARC": arcs,
                  "NODE": nodes,
                  "DEM": self.dem,
                  "USE_GEOMORPHON": 0}
        processing.run(self.alg_name, params)

        if "slope_pos" in polygons.fields().names():
            results = [f["slope_pos"] for f in polygons.getFeatures() if f["slope_pos"] != NULL]
        
        self.assertIn('slope_pos', polygons.fields().names())
        self.assertEqual(len(results), len(polygons.featureCount()))

    def test_method_1(self):
        """
        Test with topographic position field
        """
        polygons = utils.create_layer(self.polygons, copy_feat=True, 
                                      copy_field=True, data_provider="ogr", 
                                      path=os.path.join(self.workspace, "polygons_1.gpkg"))
        arcs = utils.create_layer(self.polygons, copy_feat=True, 
                                  copy_field=True, data_provider="ogr", 
                                  path=os.path.join(self.workspace, "arcs_1.gpkg"))
        nodes = utils.create_layer(self.polygons, copy_feat=True, 
                                   copy_field=True, data_provider="ogr", 
                                   path=os.path.join(self.workspace, "nodes_1.gpkg"))

        params = {"POLYGON": polygons,
                  "ARC": arcs,
                  "NODE": nodes,
                  "DEM": self.dem,
                  "USE_GEOMORPHON": 1,
                  "GEOMORPHON_FIELD": "topo_pos"}
        processing.run(self.alg_name, params)

        if "slope_pos" in self.polygons.fields().names():
            results = [f["slope_pos"] for f in polygons.getFeatures() if f["slope_pos"] != NULL]
        
        self.assertIn('slope_pos', polygons.fields().names())
        self.assertEqual(len(results), len(polygons.featureCount()))

    def test_method_2(self):
        """
        Test without topographic position
        """
        polygons = utils.create_layer(self.polygons, copy_feat=True, 
                                      copy_field=True, data_provider="ogr", 
                                      path=os.path.join(self.workspace, "polygons_2.gpkg"))
        arcs = utils.create_layer(self.polygons, copy_feat=True, 
                                  copy_field=True, data_provider="ogr", 
                                  path=os.path.join(self.workspace, "arcs_2.gpkg"))
        nodes = utils.create_layer(self.polygons, copy_feat=True, 
                                   copy_field=True, data_provider="ogr", 
                                   path=os.path.join(self.workspace, "nodes_2.gpkg"))

        params = {"POLYGON": polygons,
                  "ARC": arcs,
                  "NODE": nodes,
                  "DEM": self.dem,
                  "USE_GEOMORPHON": 2}
        processing.run(self.alg_name, params)

        if "slope_pos" in self.polygons.fields().names():
            results = [f["slope_pos"] for f in polygons.getFeatures() if f["slope_pos"] != NULL]
        
        self.assertIn('slope_pos', polygons.fields().names())
        self.assertEqual(len(results), len(polygons.featureCount()))

    def test_without_direction(self):
        """
        Test with hedge orientation being compited inside algorithm
        """
        polygons = utils.create_layer(self.polygons, copy_feat=True, 
                                      copy_field=True, data_provider="ogr", 
                                      path=os.path.join(self.workspace, "polygons_2.gpkg"))
        arcs = utils.create_layer(self.polygons, copy_feat=True, 
                                  copy_field=True, data_provider="ogr", 
                                  path=os.path.join(self.workspace, "arcs_2.gpkg"))
        nodes = utils.create_layer(self.polygons, copy_feat=True, 
                                   copy_field=True, data_provider="ogr", 
                                   path=os.path.join(self.workspace, "nodes_2.gpkg"))

        if "direction" in arcs.fields().names():
            at.delete_fields(arcs, ["direction"])

        params = {"POLYGON": polygons,
                  "ARC": arcs,
                  "NODE": nodes,
                  "DEM": self.dem,
                  "USE_GEOMORPHON": 2}
        processing.run(self.alg_name, params)

        if "slope_pos" in self.polygons.fields().names():
            results = [f["slope_pos"] for f in polygons.getFeatures() if f["slope_pos"] != NULL]
        
        self.assertIn('slope_pos', polygons.fields().names())
        self.assertEqual(len(results), len(polygons.featureCount()))

        # Deletion of temp folder maybe put somewhere else ? 
        # maybe outside class and put workspace in init argument
        utils.delete_temp_workspace(self.folder_object)

if __name__ == '__main__':
    unittest.main()
