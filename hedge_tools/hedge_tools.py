# -*- coding: utf-8 -*-

"""
/***************************************************************************
 HedgeTools
                                 A QGIS plugin
 Hedge caracterisation from polygon layer.
 Generated by Plugin Builder: http://g-sherman.github.io/Qgis-Plugin-Builder/
                              -------------------
        begin                : 2022-01-24
        copyright            : (C) 2022 by Gabriel Marquès / Dynafor
        email                : gabriel.marques@toulouse-inp.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

__author__ = 'Gabriel Marquès / Dynafor'
__date__ = '2022-01-24'
__copyright__ = '(C) 2022 by Gabriel Marquès / Dynafor'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'

import os
import sys
import inspect
import processing

from qgis.core import QgsProcessingAlgorithm, QgsApplication
from qgis.PyQt.QtCore import QCoreApplication
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QAction, QMenu
from .hedge_tools_provider import HedgeToolsProvider
from . import resources

cmd_folder = os.path.split(inspect.getfile(inspect.currentframe()))[0]

if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)


def tr(string):

    return QCoreApplication.translate("@default", string)


class HedgeToolsPlugin(object):

    def __init__(self, iface):
        self.iface = iface
        self.provider = HedgeToolsProvider()
        QgsApplication.processingRegistry().addProvider(self.provider)

        #  Translator = pour la traduction dans uen autre version/langue mais comment créer le qm ??
        # # initialize locale
        # try:
        #     locale = QSettings().value("locale/userLocale", "en", type=str)[0:2]
        # except Exception:
        #     locale = "en"
        # locale_path = os.path.join(
        #     self.plugin_dir,
        #     "i18n",
        #     "shapeTools_{}.qm".format(locale))
        # if os.path.exists(locale_path):
        #     self.translator = QTranslator()
        #     self.translator.load(locale_path)
        #     QCoreApplication.installTranslator(self.translator)
    
    def initGui(self):
        # self.initProcessing()
        # Init icon
        icon = QIcon(":/plugins/hedge_tools/images/hedge_tools.png")

        ###################Extraction###################

        # Initialize data preparation menu
        menuExt = QMenu()
        menuExt.setObjectName("htExtraction")
        menuExt.setIcon(icon)

        # Initialize DHM tool
        self.geneManageTilesAction = menuExt.addAction(icon, tr("0 - Manage raster tiles"),
                                                  self.manageTiles)
        self.geneManageTilesAction.setObjectName("htManageTiles")

        # Initialize DHM tool
        self.geneDHMAction = menuExt.addAction(icon, tr("1 - Generate DHM"),
                                                  self.geneDHM)
        self.geneDHMAction.setObjectName("htGeneDHM")

        # Initialize NDVI tool
        self.geneNDVIAction = menuExt.addAction(icon, tr("2 - Generate NDVI"),
                                                  self.geneNDVI)
        self.geneNDVIAction.setObjectName("htGeneNDVI")

        # Initialize tree cover tool
        self.geneTreeCoverAction = menuExt.addAction(icon, tr("3 - Generate tree cover"),
                                                  self.geneTreeCover)
        self.geneTreeCoverAction.setObjectName("htGeneTreeCover")

        # Initialize CWA tool
        self.preProcessCWAAction = menuExt.addAction(icon, tr("4 - Preprocessing: categorize wooded area"),
                                                  self.preProcessCWA)
        self.preProcessCWAAction.setObjectName("htPreProcessCWA")

        # Initialize CWA tool
        self.catWoodedAction = menuExt.addAction(icon, tr("5 - Categorize wooded area [bêta]"),
                                                  self.catWooded)
        self.catWoodedAction.setObjectName("htCatWooded")

        # Initizalize split by network tool
        self.splitByNetworkAction = menuExt.addAction(icon, tr("6 - Split by network [optional]"),
                                                      self.splitByNetwork)
        self.splitByNetworkAction.setObjectName("htsplitByNetwork")

        # Initizalize aggregation tool
        self.aggregationAction = menuExt.addAction(icon, tr("7 - Aggregation [optional]"),
                                                      self.aggregation)
        self.aggregationAction.setObjectName("htaggregation")

        # Initialize categorize wooded area tool
        self.extractionAction = QAction(icon, tr("0 - Extraction [optional]"),
                                       self.iface.mainWindow())
        self.extractionAction.setMenu(menuExt)
        self.iface.addPluginToVectorMenu("Hedge Tools", self.extractionAction)
        ###################Data preparation###################

        # Initialize data preparation menu
        menuDp = QMenu()
        menuDp.setObjectName("htDataPreparation")
        menuDp.setIcon(icon)

        # Initialize create median axis tool
        self.topologicalArcAction = menuDp.addAction(icon,
                                                     tr("1 - Create topological arc"),
                                                     self.topologicalArc)
        self.topologicalArcAction.setObjectName("htTopologicalArc")

        # Initialize create nodes tool
        self.createNodesAction = menuDp.addAction(icon, tr("2 - Create topological nodes"),
                                                self.createNodes)
        self.createNodesAction.setObjectName("htCreateNodes")

        # Initialize split hedges tool
        self.createPolygonsAction = menuDp.addAction(icon, tr("3 - Create polygons from median axis"),
                                                  self.createPolygons)
        self.createPolygonsAction.setObjectName("htCreatePolygons")

        # Initialize split hedges tool
        self.simplifyMedianAxisAction = menuDp.addAction(icon, tr(
            "4 - Simplify median axis [optionnal]"),
                                                       self.simplifyMedianAxis)
        self.simplifyMedianAxisAction.setObjectName("htSimplifyMedianAxis")

        # Add the data preparation tools to the menu
        self.dataPrepAction = QAction(icon, tr("1 - Data preparation"),
                                      self.iface.mainWindow())
        self.dataPrepAction.setMenu(menuDp)
        self.iface.addPluginToVectorMenu("Hedge Tools", self.dataPrepAction)

        ###################Data transformation###################

        # Initialize data transformation menu
        menuDt = QMenu()
        menuDt.setObjectName("htDataTransformation")
        menuDt.setIcon(icon)

        # Initialize split by orientation tool
        self.splitByOriAction = menuDt.addAction(icon,
                                                 tr("Split by orientation"),
                                                 self.splitByOri)
        self.splitByOriAction.setObjectName("htsplitbyorientation")

        # Initialize split by length tool
        self.splitByDistAction = menuDt.addAction(icon,
                                                 tr("Split by distance"),
                                                 self.splitByDist)
        self.splitByDistAction.setObjectName("htsplitbydistance")

        # Initialize split by interface tool
        self.splitByItfAction = menuDt.addAction(icon,
                                                 tr("Split by interface"),
                                                 self.splitByItf)
        self.splitByItfAction.setObjectName("htsplitbyinterface")

        # Add the data transformation tools to the menu
        self.dataTransformation = QAction(icon, tr("2 - Data transformation"),
                                      self.iface.mainWindow())
        self.dataTransformation.setMenu(menuDt)
        self.iface.addPluginToVectorMenu("Hedge Tools", self.dataTransformation)

        ###################Hedges caracterisation menu###################
        # Initialize hedges caracterisation menu
        # menuCarac = QMenu()
        # menuCarac.setObjectName("htHedgesCaracterisation")

        ###################Morphology###################

        # Initialize morphology menu
        menuMorpho = QMenu()
        menuMorpho.setObjectName("htHedgesMorphology")
        menuMorpho.setIcon(icon)

        # Initialize compute length tool
        self.computeLengthAction = menuMorpho.addAction(icon,
                                                        tr("Length"),
                                                        self.computeLength)
        self.computeLengthAction.setObjectName("htcomputeLength")

        # Initialize compute width tool
        self.computeWidthAction = menuMorpho.addAction(icon,
                                                       tr("Width"),
                                                       self.computeWidth)
        self.computeWidthAction.setObjectName("htcomputeWidth")

        # Initialize compute orientation tool
        self.computeOriAction = menuMorpho.addAction(icon,
                                                     tr("Orientation"),
                                                     self.computeOri)
        self.computeOriAction.setObjectName("htcomputeOri")

        # Initialize compute shape metrics tool
        self.computeShapeAction = menuMorpho.addAction(icon,
                                                       tr("Shape metrics"),
                                                       self.computeShapeM)
        self.computeShapeAction.setObjectName("htcomputeShape")


        # Add the morphology tools to the menu
        self.hedgesMorphology = QAction(icon, tr("3 - Hedges level: morphology"),
                                      self.iface.mainWindow())
        self.hedgesMorphology.setMenu(menuMorpho)
        self.iface.addPluginToVectorMenu("Hedge Tools", self.hedgesMorphology)

        ###################Physiognomy###################

        # Initialize physiognomy menu
        menuPhysio = QMenu()
        menuPhysio.setObjectName("htHedgesPhysiognomy")
        menuPhysio.setIcon(icon)

        # Initialize compute height metrics tool
        self.computeHeightDHMAction = menuPhysio.addAction(icon,
                                                        tr("Height metrics from DHM"),
                                                        self.computeHeightMDHM)
        self.computeHeightDHMAction.setObjectName("htcomputeHeightDHM")

        # Initialize compute strata proportion tool
        self.computeStrataPropDHMAction = menuPhysio.addAction(icon,
                                                        tr("Strata proportion from DHM"),
                                                        self.computeStrataDHM)
        self.computeStrataPropDHMAction.setObjectName("htcomputeStrataDHM")

        # Add the physiognomy tools to the menu
        self.hedgesPhysiognomy = QAction(icon, tr("4 - Hedges level: physiognomy"),
                                      self.iface.mainWindow())
        self.hedgesPhysiognomy.setMenu(menuPhysio)
        self.iface.addPluginToVectorMenu("Hedge Tools", self.hedgesPhysiognomy)

        # menuCarac.addMenu("Hedges morphology")
        # menuCarac.addMenu("Hedges physiognomy")
        # Add sub menu to hedges caracterisation
        # icon = QIcon(self.plugin_dir + "/images/logo_ht.svg")
        # self.hedgesCaracterisation = QAction(icon, tr("Hedges caracterisation"),
        #                                      self.iface.mainWindow())
        #
        # # self.hedgesCaracterisation.setMenu(menuCarac)
        # self.iface.addPluginToVectorMenu("Hedge Tools", self.hedgesCaracterisation)

        ###################Hedges context menu###################
        # Initialize hedges caracterisation menu
        # menuCarac = QMenu()
        # menuCarac.setObjectName("htHedgesCaracterisation")

        ###################Geographic###################
        # Initialize geographic menu
        menuGeo = QMenu()
        menuGeo.setObjectName("htHedgesGeo")
        menuGeo.setIcon(icon)

        # Initialize topographic position tool
        self.topoPosAction = menuGeo.addAction(icon,
                                               tr("Topographic position"),
                                               self.topoPos)
        self.topoPosAction.setObjectName("httopoPosition")

        # Initialize relative orientation tool
        self.relativeOriAction = menuGeo.addAction(icon,
                                               tr("Hedge relative orientation to slope"),
                                               self.relativeOri)
        self.relativeOriAction.setObjectName("htrelativeOri")

        # Initialize interface type tool
        # self.interfaceTypeAction = menuGeo.addAction(icon,
        #                                        tr("Interface type"),
        #                                        self.interfaceType)
        # self.interfaceTypeAction.setObjectName("htinterfaceType")

        # Initialize distance to forest tool
        self.distToForestAction = menuGeo.addAction(icon,
                                               tr("Shortest distance to forest"),
                                               self.distToForest)
        self.distToForestAction.setObjectName("htdistToForest")

        # Initialize distance to forest tool
        # self.embankmentDitchAction = menuGeo.addAction(icon,
        #                                        tr("Embankment and ditch"),
        #                                        self.embankmentDitch)
        # self.embankmentDitchAction.setObjectName("htembankmentDitch")

        # Add the geographic tools to the menu
        self.geoContext = QAction(icon, tr("5 - Context level: geographic"),
                                  self.iface.mainWindow())
        self.geoContext.setMenu(menuGeo)
        self.iface.addPluginToVectorMenu("Hedge Tools", self.geoContext)

        # Initialize network menu
        menuNetwork = QMenu()
        menuNetwork.setObjectName("htHedgesNetwork")
        menuNetwork.setIcon(icon)

        # Initialize forest connexion tool
        self.forestConnectionAction = menuNetwork.addAction(icon,
                                               tr("Connection to a forest"),
                                               self.forestConnection)
        self.forestConnectionAction.setObjectName("htforestConnection")

        # Initialize subgraph creation tool
        self.subgraphCreationAction = menuNetwork.addAction(icon,
                                               tr("Subgraphs creation"),
                                               self.subgraphCreation)
        self.subgraphCreationAction.setObjectName("htsubgraphcreation")
               
        # Initialize connectivity metrics tool
        self.connectivityMetricsAction = menuNetwork.addAction(icon,
                                               tr("Connectivity metrics"),
                                               self.connectivityMetrics)
        self.connectivityMetricsAction.setObjectName("htconnectivitymetrics")

        # Add the network tools to the menu
        self.networkContext = QAction(icon, tr("6 - Landscape level: network"),
                                      self.iface.mainWindow())
        self.networkContext.setMenu(menuNetwork)
        self.iface.addPluginToVectorMenu("Hedge Tools", self.networkContext)

        # Initialize network menu
        menuGrid = QMenu()
        menuGrid.setObjectName("htHedgesGrid")
        menuGrid.setIcon(icon)

                # Initialize landscape metrics tool
        self.landscapeMetricsAction = menuGrid.addAction(icon,
                                               tr("Landscape metrics"),
                                               self.landscapeMetrics)
        self.landscapeMetricsAction.setObjectName("htlandscapemetrics")

        # Add the network tools to the menu
        self.gridContext = QAction(icon, tr("7 - Landscape level: grid"),
                                      self.iface.mainWindow())
        self.gridContext.setMenu(menuGrid)
        self.iface.addPluginToVectorMenu("Hedge Tools", self.gridContext)

    def unload(self):
        self.iface.removePluginVectorMenu("Hedge Tools", self.extractionAction)
        self.iface.removePluginVectorMenu("Hedge Tools", self.dataPrepAction)
        self.iface.removePluginVectorMenu("Hedge Tools", self.dataTransformation)
        self.iface.removePluginVectorMenu("Hedge Tools", self.hedgesMorphology)
        self.iface.removePluginVectorMenu("Hedge Tools", self.hedgesPhysiognomy)
        self.iface.removePluginVectorMenu("Hedge Tools", self.geoContext)
        self.iface.removePluginVectorMenu("Hedge Tools", self.networkContext)
        self.iface.removePluginVectorMenu("Hedge Tools", self.gridContext)

        QgsApplication.processingRegistry().removeProvider(self.provider)

    def manageTiles(self):
        processing.execAlgorithmDialog("hedgetools:managerastertiles", {})
    
    def geneDHM(self):
        processing.execAlgorithmDialog("hedgetools:generatedhm", {})

    def geneNDVI(self):
        processing.execAlgorithmDialog("hedgetools:generatendvi", {})

    def geneTreeCover(self):
        processing.execAlgorithmDialog("hedgetools:generatetreecover", {})  

    def preProcessCWA(self):
        processing.execAlgorithmDialog("hedgetools:preprocesscwa")
        
    def catWooded(self):
        processing.execAlgorithmDialog("hedgetools:categorizewoodedarea", {})

    def splitByNetwork(self):
        processing.execAlgorithmDialog("hedgetools:splitbynetwork")
    
    def aggregation(self):
        processing.execAlgorithmDialog("hedgetools:aggregation")

    def topologicalArc(self):
        processing.execAlgorithmDialog("hedgetools:topologicalarc", {})

    def createNodes(self):
        processing.execAlgorithmDialog("hedgetools:createnodes", {})

    def createPolygons(self):
        processing.execAlgorithmDialog("hedgetools:createpolygons", {})

    def simplifyMedianAxis(self):
        processing.execAlgorithmDialog("hedgetools:simplifymedianaxis", {})

    def splitByOri(self):
        processing.execAlgorithmDialog("hedgetools:splitbyorientation", {})

    def splitByDist(self):
        processing.execAlgorithmDialog("hedgetools:splitbydistance", {})

    def splitByItf(self):
        processing.execAlgorithmDialog("hedgetools:splitbyinterface", {})

    def computeLength(self):
        processing.execAlgorithmDialog("hedgetools:computelength", {})

    def computeWidth(self):
        processing.execAlgorithmDialog("hedgetools:computewidth", {})

    def computeOri(self):
        processing.execAlgorithmDialog("hedgetools:computeori", {})

    def computeShapeM(self):
        processing.execAlgorithmDialog("hedgetools:computeshapemetrics")

    def computeHeightMDHM(self):
        processing.execAlgorithmDialog("hedgetools:computeheightmetricsDHM")

    def computeStrataDHM(self):
        processing.execAlgorithmDialog("hedgetools:computestrataproportionDHM")

    def topoPos(self):
        processing.execAlgorithmDialog("hedgetools:topographicposition")

    def relativeOri(self):
        processing.execAlgorithmDialog("hedgetools:relativeorientation")

    # def interfaceType(self):
    #     processing.execAlgorithmDialog("hedgetools:interfacetype")

    def distToForest(self):
        processing.execAlgorithmDialog("hedgetools:distancetoforest")

    # def embankmentDitch(self):
    #     processing.execAlgorithmDialog("hedgetools:embankmentdicth")

    def forestConnection(self):
        processing.execAlgorithmDialog("hedgetools:forestconnection")

    def subgraphCreation(self):
        processing.execAlgorithmDialog("hedgetools:subgraphcreation")

    def connectivityMetrics(self):
        processing.execAlgorithmDialog("hedgetools:connectivitymetrics")

    def landscapeMetrics(self):
        processing.execAlgorithmDialog("hedgetools:landscapemetrics")