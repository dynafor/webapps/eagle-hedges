# -*- coding: utf-8 -*-

"""
/***************************************************************************
 MedianAxisVoronoi
                                 A QGIS plugin
 Offers methods to rectiffy end of median axis and simplify with polygons constraint
 Generated by Plugin Builder: http://g-sherman.github.io/Qgis-Plugin-Builder/
                              -------------------
        begin                : 2022-01-22
        copyright            : (C) 2022 by Gabriel Marquès
        email                : gabriel.marques@toulouse-inp.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

__author__ = 'Gabriel Marquès'
__date__ = '2022-01-22'
__copyright__ = '(C) 2022 by Gabriel Marquès'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'

from qgis.PyQt.QtCore import (QCoreApplication,
                              QVariant)
from qgis.core import (QgsProcessing,
                       QgsFeatureSink,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterFeatureSink,
                       QgsProcessingParameterNumber,
                       QgsGeometry,
                       QgsFeatureRequest,
                       QgsLineString,)

from qgis.analysis import *
from qgis.core.additions.edit import edit
from qgis.PyQt.QtGui import QIcon
from hedge_tools import resources # Only need in hedge_tools.py normaly but just to keep track of import 

import processing
from hedge_tools.tools.vector import geometry as g
from hedge_tools.tools.vector import utils
import numpy as np
import sys


class MedianAxisSimplificationAlgorithm(QgsProcessingAlgorithm):
    """
    Allows the user to :
    - Modify end of median axis given two distance parameters
    - Simplify median axis with polygon intersection constraint

    Parameters
    ---
    INPUT_ARC (QgisObject : QgsVectorLayer) : LineString : Median axis layer.
    INPUT_POLYGON (QgisObject : QgsVectorLayer) : Polygon : Polygon layer.
    INPUT_NODE (QgisObject : QgsVectorLayer) : Point : Node layer.
    INPUT_DEV (float) : Default : 1,5. Maximal allowed distance between an arc vertex and
                      the new segment after otential vertex deletion

    Return
    ---
    OUTPUT (QgisObject : QgsVectorLayer) : Linestring    : Modified median axis.
    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.
    INPUT_POLY = "INPUT_POLY"
    INPUT_ARC = "INPUT_ARC"
    INPUT_NODE = "INPUT_NODE"
    INPUT_DEV = "INPUT_DEV"
    OUTPUT_ARC = "OUTPUT_ARC"

    def initAlgorithm(self, config):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        # We add the input vector features source. It can have any kind of
        # geometry.
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.INPUT_POLY,
                self.tr("Polygon vector layer"),
                [QgsProcessing.TypeVectorPolygon]
            )
        )

        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.INPUT_ARC,
                self.tr("Arc vector layer"),
                [QgsProcessing.TypeVectorLine]
            )
        )

        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.INPUT_NODE,
                self.tr("Node vector layer"),
                [QgsProcessing.TypeVectorPoint]
            )
        )

        # Threshold for simplification
        self.addParameter(
            QgsProcessingParameterNumber(
                name=self.INPUT_DEV,
                description=self.tr("Maximal shift between deleted vertex and new segment (Simplification)"),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=5,
                optional=True,
                minValue=0,
                maxValue=100
            )
        )

        # We add a feature sink in which to store our processed features (this
        # usually takes the form of a newly created vector layer when the
        # algorithm is run in QGIS).
        self.addParameter(
            QgsProcessingParameterFeatureSink(
                self.OUTPUT_ARC,
                self.tr("Rectified arcs")
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        TODO : Delete 2 last node of each ma and extend line could fix bias in MA
                --> Check tentative rectificaiton in qgis
                Polygons hole case missing MA could come from dangle removal.
        """

        # Init
        input_poly = self.parameterAsVectorLayer(parameters,  self.INPUT_POLY, context)
        input_arc = self.parameterAsVectorLayer(parameters,  self.INPUT_ARC, context)
        input_node = self.parameterAsVectorLayer(parameters, self.INPUT_NODE, context)

        # Init progress bar
        count = input_poly.featureCount()
        start = 0
        step = 100

        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Starting simplification")

        arc_layer = utils.create_layer(input_arc, copy_feat=True,
                                        copy_field=True)
        # Main
        geom_map = {}

        for current, arc in enumerate(arc_layer.getFeatures()):

            # Creating geometry and static primitive
            arc_geom = arc.geometry()
            linestring = arc_geom.constGet()
            exp = "pid = %d" % arc["pid"]
            req = QgsFeatureRequest().setFilterExpression(exp)
            poly = next(input_poly.getFeatures(req))
            poly_geom = poly.geometry()

            # Simplification
            try:
                result = g.constrained_douglas_peucker(poly_geom, linestring, parameters[self.INPUT_DEV])
                result_geom = QgsGeometry().fromPolyline(result)

                # Add new geom to geom_map
                geom_map[arc.id()] = result_geom
            except RecursionError:
                print(arc.id())

            # Set progress
            feedback.setProgress(start + int((current / count) * step))
            # Check for cancellation
            if feedback.isCanceled():
                return {}

        # Modify geometries
        arc_layer.dataProvider().changeGeometryValues(geom_map)

        # Add arc_layer to sink
        (sink_lines, lines_id) = self.parameterAsSink(parameters,
                                                      self.OUTPUT_ARC,
                                                      context,
                                                      arc_layer.fields(),
                                                      arc_layer.wkbType(),
                                                      arc_layer.sourceCrs())

        features = arc_layer.getFeatures()
        for feature in features:
            sink_lines.addFeature(feature, QgsFeatureSink.FastInsert)

        return {"OUTPUT_ARC": lines_id}
    
    def icon(self):
        """
        Should return a QIcon which is used for your provider inside
        the Processing toolbox.
        """
        return QIcon(":/plugins/hedge_tools/images/hedge_tools.png")
    
    def shortHelpString(self):
        """
        Returns a localised short help string for the algorithm.
        """
        return self.tr("This algorithm simplifies the geometry of linear \
                        median axis (i.e. the output of step 3 - Create \
                        polygons from median axis) \n \
                        It allows to simplify median axis by smoothing\
                        the geometry with Douglas-Peucker algorithm \
                        while forcing the arcs \
                        to remain within their respective polygon. If no \
                        solution is found, the geometry is not simplified.")

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'simplifymedianaxis'

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr("4 - Simplify median axis [optional]")

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr("1 - Data preparation")

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'datapreparation'

    def tr(self, string):
        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        return MedianAxisSimplificationAlgorithm()
