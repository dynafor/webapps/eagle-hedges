# -*- coding: utf-8 -*-

"""
/***************************************************************************
 TopologicalNodes
                                 A QGIS plugin
 This module creates nodes with graph theory from a linear layer.
 Generated by Plugin Builder: http://g-sherman.github.io/Qgis-Plugin-Builder/
                              -------------------
        begin                : 2022-01-10
        copyright            : (C) 2022 by Gabriel Marquès
        email                : marques.g@hotmail.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

__author__ = "Gabriel Marquès"
__date__ = "2022-01-10"
__copyright__ = "(C) 2022 by Gabriel Marquès"

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = "$Format:%H$"

from qgis.PyQt.QtCore import (QCoreApplication,
                              QVariant)
from qgis.core import (QgsProcessing,
                       QgsFeatureSink,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterFeatureSink,
                       QgsGeometry,
                       QgsSpatialIndex,
                       QgsFeatureRequest,
                       QgsFeature,
                       NULL)

from qgis.analysis import *
from qgis.core.additions.edit import edit
from qgis.PyQt.QtGui import QIcon
from hedge_tools import resources # Only need in hedge_tools.py normaly but just to keep track of import 

from hedge_tools.tools.vector import attribute_table as at
from hedge_tools.tools.vector import geometry as g
from hedge_tools.tools.vector import utils
from hedge_tools.tools.classes import HedgeGraph as hG

class TopologicalNodesAlgorithm(QgsProcessingAlgorithm):
    """
    Create the O, T, X and L nodes from a line layer.

    Parameters
    ---
    INPUT (QgisObject : QgsVectorLayer) : Median axis layer path. Contain line features

    Return
    ---
    OUTPUT (QgisObject : QgsVectorLayer) : Node : Path of the layer containing the nodes derivated
    from INPUT layer
    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    INPUT = "INPUT"
    OUTPUT = "OUTPUT"

    def initAlgorithm(self, config):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        # Adding the input vector features source. It must be a line layer
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.INPUT,
                self.tr("Line vector layer"),
                [QgsProcessing.TypeVectorLine]
            )
        )

        # We add a feature sink in which to store our processed features (this
        # usually takes the form of a newly created vector layer when the
        # algorithm is run in QGIS).
        self.addParameter(
            QgsProcessingParameterFeatureSink(
                self.OUTPUT,
                self.tr("Topological nodes vector layer (output)")
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """
        input_layer = self.parameterAsVectorLayer(parameters, self.INPUT, context)

        idx = input_layer.fields().indexFromName("pid")
        if idx == -1:
            feedback.pushWarning(f"{input_layer.name()} must have a pid field from "
                                 "1 - Create topological arc tool")
            return {}

        feedback.setProgress(1)

        feedback.pushInfo("Computing nodes")

        # Extract first and last vertex from polyline graph
        unique = input_layer.uniqueValues(idx)
        count = len(unique)

        features = []
        for current, id in enumerate(unique):
            req = QgsFeatureRequest().setFilterExpression("pid = %d" % id)
            geoms = []
            for feat in input_layer.getFeatures(req):
                geom = feat.geometry()
                # If the line is a loop we explode it to construct correct topology
                if geom.asPolyline()[0] == geom.asPolyline()[-1]:
                    geoms += g.line_to_segments(geom)
                else:
                    geoms.append(geom)

            graph = hG.HedgeGraph(geoms)
            features += graph.vertices_to_nodes(True)

            # Set progress
            feedback.setProgress(int((current / count) * 30))
            # Check for cancellation
            if feedback.isCanceled():
                return {}

        features = self.delete_d2_vertices(features)

        # Store result in a layer and create id fields
        node_layer = utils.create_layer(input_layer, geom_type="Point")
        idx_fid, idx_vid, _ = at.create_fields(node_layer, [("fid", QVariant.Int),
                                                            ("vid", QVariant.Int),
                                                            ("Degree", QVariant.Int)])
        node_layer.dataProvider().addFeatures(features)
        attr_map = {f.id(): {idx_fid: f.id(), idx_vid: f.id()}
                    for f in node_layer.getFeatures()}
        node_layer.dataProvider().changeAttributeValues(attr_map)

        feedback.pushInfo("Creating node output and updating input fields")

        # update input_layer *ghost update* to add fk_fields
        index = QgsSpatialIndex(input_layer.getFeatures())
        idx_vid_1, idx_vid_2 = at.create_fields(input_layer, 
                                               [("vid_1", QVariant.Int),
                                                ("vid_2", QVariant.Int)])
        arc_attr_map = {f.id(): {idx_vid_1: None, idx_vid_2: None}
                        for f in input_layer.getFeatures()}

        # Compute node_type
        idx_type = at.create_fields(node_layer, [("Node_type", QVariant.String)])[0]
        node_attr_map = {}

        count = node_layer.featureCount()

        for current, node in enumerate(node_layer.getFeatures()):
            if node["Degree"] == 1:
                node_attr_map[node.id()] = {idx_type: "O"}
            elif node["Degree"] == 3:
                node_attr_map[node.id()] = {idx_type: "T"}
            elif node["Degree"] == 4:
                node_attr_map[node.id()] = {idx_type: "X"}
            elif node["Degree"] > 4:
                node_attr_map[node.id()] = {idx_type: "M"}
            else:
                node_attr_map[node.id()] = {idx_type: "Other"}

            # Build geometry engine
            node_const = node.geometry().constGet()
            engine = QgsGeometry.createGeometryEngine(node_const)
            engine.prepareGeometry()

            # Get potential neighbour
            candidate_ids = index.intersects(node.geometry().boundingBox())
            request = QgsFeatureRequest().setFilterFids(candidate_ids)

            for arc in input_layer.getFeatures(request):
                arc_const = arc.geometry().constGet()
                # Get real neighbour
                if engine.intersects(arc_const):
                    if arc_attr_map[arc.id()][idx_vid_1] is None:
                        arc_attr_map[arc.id()][idx_vid_1] = node["vid"]
                    elif arc_attr_map[arc.id()][idx_vid_2] is None:
                        arc_attr_map[arc.id()][idx_vid_2] = node["vid"]
        
            # Set progress
            feedback.setProgress(30 + int((current / count) * 70))
            # Check for cancellation
            if feedback.isCanceled():
                return {}
            
        arc_attr_map = self.handles_closed_loops(arc_attr_map, idx_vid_1, idx_vid_2)
        node_layer.dataProvider().changeAttributeValues(node_attr_map)
        input_layer.dataProvider().changeAttributeValues(arc_attr_map)

        # Add to sink
        (sink_nodes, node_id) = self.parameterAsSink(parameters,
                                                     self.OUTPUT,
                                                     context,
                                                     node_layer.fields(),
                                                     node_layer.wkbType(),
                                                     node_layer.sourceCrs())

        features = node_layer.getFeatures()
        for feature in features:
            sink_nodes.addFeature(feature, QgsFeatureSink.FastInsert)

        return {"OUTPUT": node_id}

    def delete_d2_vertices(self, features:list[QgsFeature])->list[QgsFeature]:
        """
        From a list of node features delete the degree 2 ones.
        Those were created inside loop as we explode the line for handling them.

        Parameters
        ----------
        features: list of node features

        Returns
        -------
        features: list of node features without d2 vertices
        """
        d2_vertices = []
        for feature in features:
            if feature["Degree"] == 2:
                d2_vertices.append(feature)

        features = filter(lambda i: i not in d2_vertices, features)

        return features
    
    def handles_closed_loops(self, attr_map:dict[int:dict[int:int]], 
                             idx_vid_1: int, idx_vid_2:int
                             )->dict[int:dict[int:int]]:
        """
        Update attr_map vid_2 key if value is NULL with vid_1 value

        Parameters
        ----------
        attr_map:
        idx_vid_2:

        Returns
        -------
        attr_map:
            Updated attr_map
        """
        for v in attr_map.values():
            if v[idx_vid_2] == NULL:
                v[idx_vid_2] = v[idx_vid_1]

        return attr_map

    def icon(self):
        """
        Should return a QIcon which is used for your provider inside
        the Processing toolbox.
        """
        return QIcon(":/plugins/hedge_tools/images/hedge_tools.png")
    
    def shortHelpString(self):
        """
        Returns a localised short help string for the algorithm.
        """
        return self.tr("This algorithm computes the topological nodes that \
                        will delimit a hedgerow. \n \
                        - The degree 1 nodes (line extremities) are \
                        labeled O. \n \
                        - The degree 3 nodes (intersection of 3 lines) are \
                        labeled T. \n \
                        - The degree 4 or more nodes (intersection of 4 \
                        lines) are labeled X. \n \
                        - The degree 5 and more are labeled as M \
                        (multiple). \n \
                        The 'Vid' field is created and used as the unique \
                        identifier in the node layer. \n \
                        The input layer is also updated with two new fields \
                        (vid_1 and vid_2) to facilitate communication between \
                        the layers.")

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "topologicalnodes"

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr("2 - Create topological nodes")

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr("1 - Data preparation")

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "datapreparation"

    def tr(self, string):
        return QCoreApplication.translate("Processing", string)

    def createInstance(self):
        return TopologicalNodesAlgorithm()
