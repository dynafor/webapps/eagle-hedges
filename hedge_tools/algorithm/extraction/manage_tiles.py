#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 18 11:28:46 2023

@author: dynafor
"""

from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (QgsProcessingAlgorithm,
                       QgsProcessingParameterExtent,
                       QgsProcessingParameterFolderDestination,
                       QgsProcessingParameterFile,
                       QgsProcessing)
from hedge_tools.tools.raster import utils
from qgis.PyQt.QtGui import QIcon
from hedge_tools import resources # Only need in hedge_tools.py normaly but just to keep track of import 

class ManageRasterTilesProcessingAlgorithm(QgsProcessingAlgorithm):
    """
    This is an example algorithm that takes a vector layer and
    creates a new identical one.

    It is meant to be used as an example of how to create your own
    algorithms and explain methods and variables used to do it. An
    algorithm like this will be available in all elements, and there
    is not need for additional work.

    All Processing algorithms should extend the QgsProcessingAlgorithm
    class.
    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    EXTENT = 'EXTENT'
    DIR_SRC_DSM = 'DIR_SRC_DSM'
    DIR_COPY_DSM = 'DIR_COPY_DSM'
    DIR_SRC_DEM = 'DIR_SRC_DEM'
    DIR_COPY_DEM = 'DIR_COPY_DEM'
    DIR_SRC_ORTHO = 'DIR_SRC_ORTHO'
    DIR_COPY_ORTHO = 'DIR_COPY_ORTHO'

    def icon(self):
        """
        Should return a QIcon which is used for your provider inside
        the Processing toolbox.
        """
        return QIcon(":/plugins/hedge_tools/images/hedge_tools.png")

    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        return ManageRasterTilesProcessingAlgorithm()

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'managerastertiles'

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr('0 - Manage raster tiles')

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr('0 - Extraction [optional]')

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'extraction'

    def shortHelpString(self):
        """
        Returns a localised short helper string for the algorithm. This string
        should provide a basic description about what the algorithm does and the
        parameters and outputs associated with it..
        """
        return self.tr("This algorithm intersects a vector layer extent \
                       with the extent of all rasters within directories, and \
                       copies them in an other directory if they \
                       intersect. \n \
                       The 3 possible directories are for Digital Surface \
                       Model (DSM), Digital Elevation Model (DEM), or \
                       Ortho IRC. \n \
                       It can be used for only 1 or 2 data types, but for \
                       any of them both source and copy directories must be \
                       provided. \n \
                       The total extent of the DSM tiles must be included \
                       within the DEM and NDVI ones.")
                    
    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        self.addParameter(
            QgsProcessingParameterExtent(
                self.EXTENT,
                self.tr('Extent of the study area'),
                optional=False
            )
        )
        
        self.addParameter(
            QgsProcessingParameterFile(
                self.DIR_SRC_DSM,
                self.tr('DSM source directory path'),
                # [QgsProcessing.Folder]
                behavior=QgsProcessingParameterFile.Folder,
                optional=True
            )
        )
        
        self.addParameter(
            QgsProcessingParameterFolderDestination(
                self.DIR_COPY_DSM,
                self.tr('DSM copy directory path'),
                optional=True
            )
        )
        
        self.addParameter(
            QgsProcessingParameterFile(
                self.DIR_SRC_DEM,
                self.tr('DEM source directory path'),
                # [QgsProcessing.Folder]
                behavior=QgsProcessingParameterFile.Folder,
                optional=True
            )
        )
        
        self.addParameter(
            QgsProcessingParameterFolderDestination(
                self.DIR_COPY_DEM,
                self.tr('DEM copy directory path'),
                optional=True
            )
        )
        
        self.addParameter(
            QgsProcessingParameterFile(
                self.DIR_SRC_ORTHO,
                self.tr('Ortho IRC source directory path'),
                # [QgsProcessing.Folder]
                behavior=QgsProcessingParameterFile.Folder,
                optional=True
            )
        )
        
        self.addParameter(
            QgsProcessingParameterFolderDestination(
                self.DIR_COPY_ORTHO,
                self.tr('Ortho IRC copy directory path'),
                optional=True
            )
        )

    def checkParameterValues(self, parameters, context):
        
        if parameters[self.DIR_SRC_DSM] != None and parameters[self.DIR_COPY_DSM] == 'TEMPORARY_OUTPUT':
            return (False, "It is obligatory to provide a DSM tiles copy \
                            directory if a DSM tiles source directory is \
                            provided.")
        
        if parameters[self.DIR_SRC_DSM] == None and parameters[self.DIR_COPY_DSM] != 'TEMPORARY_OUTPUT':
            return (False, "It is obligatory to provide a DSM tiles source \
                            directory if a DSM tiles copy directory is \
                            provided.")
                            
        if parameters[self.DIR_SRC_DEM] != None and parameters[self.DIR_COPY_DEM] == 'TEMPORARY_OUTPUT':
            return (False, "It is obligatory to provide a DEM tiles copy \
                            directory if a DEM tiles source directory is \
                            provided.")
        
        if parameters[self.DIR_SRC_DEM] == None and parameters[self.DIR_COPY_DEM] != 'TEMPORARY_OUTPUT':
            return (False, "It is obligatory to provide a DEM tiles source \
                            directory if a DEM tiles copy directory is \
                            provided.")
                            
        if parameters[self.DIR_SRC_ORTHO] != None and parameters[self.DIR_COPY_ORTHO] == 'TEMPORARY_OUTPUT':
            return (False, "It is obligatory to provide an Ortho tiles copy \
                            directory if an Ortho tiles source directory is \
                            provided.")
        
        if parameters[self.DIR_SRC_ORTHO] == None and parameters[self.DIR_COPY_ORTHO] != 'TEMPORARY_OUTPUT':
            return (False, "It is obligatory to provide an Ortho tiles source \
                            directory if an Ortho tiles copy directory is \
                            provided.")
        
        return (True, '')
    
    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """
        extent = self.parameterAsExtent(parameters, self.EXTENT, context)

        path_dir_source_dsm = \
            self.parameterAsFile(parameters, self.DIR_SRC_DSM, context)
        
        param_path_dir_copy_dsm = parameters[self.DIR_COPY_DSM]
        
        path_dir_source_dem = \
            self.parameterAsFile(parameters, self.DIR_SRC_DEM, context)
        
        param_path_dir_copy_dem = parameters[self.DIR_COPY_DEM]
        
        path_dir_source_ortho = \
            self.parameterAsFile(parameters, self.DIR_SRC_ORTHO, context)
        
        param_path_dir_copy_ortho = parameters[self.DIR_COPY_ORTHO]

        utils.manage_raster_files_extraction_tools(extent = extent,
                                                   path_dir_dsm = path_dir_source_dsm,
                                                   path_dir_copy_dsm = param_path_dir_copy_dsm,
                                                   path_dir_dem = path_dir_source_dem,
                                                   path_dir_copy_dem = param_path_dir_copy_dem,
                                                   path_dir_ortho = path_dir_source_ortho,
                                                   path_dir_copy_ortho = param_path_dir_copy_ortho,
                                                   feedback = feedback)

        # Return the results of the algorithm. In this case our only result is
        # the feature sink which contains the processed features, but some
        # algorithms may return multiple feature sinks, calculated numeric
        # statistics, etc. These should all be included in the returned
        # dictionary, with keys matching the feature corresponding parameter
        # or output names.

        return {self.DIR_COPY_DSM: param_path_dir_copy_dsm,
                self.DIR_COPY_DEM: param_path_dir_copy_dem,
                self.DIR_COPY_ORTHO: param_path_dir_copy_ortho}