#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
/***************************************************************************
 GenerateDHM
                                 A QGIS plugin
 From a DEM and a DSM generate a DHM by substracting the DSM to the DEM.
 Resolution of either the DEM or the DSM can be chosen.
 Generated by Plugin Builder: http://g-sherman.github.io/Qgis-Plugin-Builder/
                              -------------------
        begin                : 2024-01-05
        copyright            : (C) 2022 by Gabriel Marques / Dynafor
        email                : gabriel.marques@toulouse-inp.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

__author__ = "Gabriel Marques / Dynafor"
__date__ = "2024-01-05"
__copyright__ = "(C) 2024 by Gabriel Marques / Dynafor"

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = "$Format:%H$"

from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (QgsProcessing,
                       QgsProcessingAlgorithm,
                       QgsProcessingException,
                       QgsProcessingParameterRasterLayer,
                       QgsProcessingParameterEnum,
                       QgsProcessingParameterBoolean,
                       QgsProcessingParameterNumber,
                       QgsProcessingParameterRasterDestination,
                       QgsRasterLayer,
                       QgsProcessingParameterFeatureSink,
                       QgsVectorLayer,
                       QgsGeometry,
                       QgsFeatureSink,
                       QgsProcessingUtils)

from processing.algs.gdal.GdalUtils import GdalUtils

from typing import Union  # until python 3.10 we need to use that instead of |

from hedge_tools.tools.classes import GenerateTreeCover as gTC

from qgis.PyQt.QtGui import QIcon
# Only need in hedge_tools.py normaly but just to keep track of import
from hedge_tools import resources


class GenerateTreeCoverProcessingAlgorithm(QgsProcessingAlgorithm):
    """
    Combine a thresholded DHM and a thresholded NDVI \
    to create a tree cover raster.

    Parameters
    ---

    Return
    ---

    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    DHM = "DHM"
    NDVI = "NDVI"
    RESOLUTION = "RESOLUTION"
    RESAMPLING = "RESAMPLING"
    EXTENT = "EXTENT"
    SIEVE = "SIEVE"
    THRESHOLD_SIEVE = "THRESHOLD_SIEVE"
    CONNECTEDNESS = "CONNECTEDNESS"
    POLYGONIZE = "POLYGONIZE"
    CLOSE = "CLOSE"
    OVERWRITE = "OVERWRITE"
    OUTPUT_RASTER = "OUTPUT_RASTER"
    OUTPUT_SIEVE = "OUTPUT_SIEVE"
    OUTPUT_VECTOR = "OUTPUT_VECTOR"

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.DHM,
                self.tr("DHM mask")
            )
        )

        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.NDVI,
                self.tr("NDVI mask")
            )
        )

        self.addParameter(
            QgsProcessingParameterNumber(
                name=self.RESOLUTION,
                description=self.tr("Output resolution (meters)"),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=None,
                minValue=0.0,
                optional=True
            )
        )

        resampling_opt_param = QgsProcessingParameterEnum(
            name=self.RESAMPLING,
            description=self.tr("Resampling method"),
            options=["Nearest neighbour ", "Bilinear",
                     "Cubic", "Cubic spline ",
                     "Lanczos windowed sinc", "Average", "Mode",
                     "Maximum", "Minimum", "Median",
                     "First quartile", "Third quartile"],
            optional=True,
            allowMultiple=False)
        resampling_opt_param.setHelp(
            self.tr('This option determines how to resample the output DHM'))
        self.addParameter(resampling_opt_param)

        if GdalUtils.version() >= 3030000:
            extent_opt_param = QgsProcessingParameterEnum(
                self.EXTENT,
                self.tr('Handling of extent differences'),
                options=['Ignore', 'Fail', 'Union', 'Intersect'],
                defaultValue=0)
            extent_opt_param.setHelp(
                self.tr('This option determines how to handle \
                        rasters with different extents'))
            self.addParameter(extent_opt_param)

        self.addParameter(
            QgsProcessingParameterBoolean(
                self.SIEVE,
                self.tr("Sieve tree cover"),
                defaultValue=True
            )
        )

        self.addParameter(
            QgsProcessingParameterNumber(
                name=self.THRESHOLD_SIEVE,
                description=self.tr("Threshold sieve value (pixel number)"),
                type=QgsProcessingParameterNumber.Integer,
                defaultValue=50,
                optional=True,
                minValue=0
            )
        )

        self.addParameter(
            QgsProcessingParameterEnum(
                name=self.CONNECTEDNESS,
                description=self.tr("Connectedness parameter for sieved output"),
                options=[
                    "4",
                    "8"],
                defaultValue=1,
                optional=True,
                allowMultiple=False))

        self.addParameter(
            QgsProcessingParameterBoolean(
                self.POLYGONIZE,
                self.tr("Polygonize tree cover raster"),
                defaultValue=True
            )
        )

        self.addParameter(
            QgsProcessingParameterBoolean(
                self.CLOSE,
                self.tr("Morphological closing of the vector layer"),
                defaultValue=True
            )
        )

        self.addParameter(
            QgsProcessingParameterBoolean(
                self.OVERWRITE,
                self.tr("Overwrite"),
                defaultValue=True
            )
        )

        self.addParameter(
            QgsProcessingParameterRasterDestination(
                self.OUTPUT_RASTER,
                self.tr("Raster tree cover")
            )
        )

        self.addParameter(
            QgsProcessingParameterRasterDestination(
                self.OUTPUT_SIEVE,
                self.tr("Sieve")
            )
        )

        self.addParameter(
            QgsProcessingParameterFeatureSink(
                self.OUTPUT_VECTOR,
                self.tr("Vector tree cover")
            )
        )

    def __init__(self):
        super().__init__()

        self.steps = 2
        self.step_per_alg = 100 // self.steps
        self.value = 0

    def progress_bar(self, feedback):
        """
        Incrementation of progress bar and cancellation
        """
        # Set progress
        feedback.setProgress(self.value)
        self.value += self.step_per_alg
        # Check for cancellation
        if feedback.isCanceled():
            return {}

    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """
        dhm = self.parameterAsRasterLayer(parameters, self.DHM, context)
        ndvi = self.parameterAsRasterLayer(parameters, self.NDVI, context)
        resolution = self.parameterAsDouble(parameters, self.RESOLUTION, 
                                            context)
        resampling = self.parameterAsEnum(parameters, self.RESAMPLING, context)
        extent = self.parameterAsEnum(parameters, self.EXTENT, context)
        compute_sieve = self.parameterAsBoolean(parameters, self.SIEVE, 
                                                context)
        threshold_sieve = self.parameterAsDouble(parameters, self.THRESHOLD_SIEVE, 
                                                 context)
        connectedness = self.parameterAsInt(parameters, self.CONNECTEDNESS, 
                                            context)
        polygonize = self.parameterAsBoolean(parameters, self.POLYGONIZE, 
                                             context)
        close = self.parameterAsBoolean(parameters, self.CLOSE, context)
        overwrite = self.parameterAsBoolean(parameters, 
                                            self.OVERWRITE, context)
        output_rst = self.parameterAsOutputLayer(parameters, self.OUTPUT_RASTER, 
                                                 context)
        if compute_sieve:
            output_sieve = self.parameterAsOutputLayer(parameters, 
                                                       self.OUTPUT_SIEVE, 
                                                       context)
        else:
            output_sieve = None

        if polygonize:
            # Avoid pushInfo "Incorrect generation"
            output_vct = "TEMPORARY_OUTPUT"
        else:
            output_vct = None

        outputs = {}

        TC_builder = gTC.GenerateTreeCover((dhm, 1), (ndvi, 1),
                                           feedback, context,
                                           self.displayName())
        TC_builder.process_algorithm(extent, resolution,
                                     resampling, compute_sieve,
                                     threshold_sieve, connectedness,
                                     polygonize, close,
                                     overwrite, output_rst,
                                     output_sieve, output_vct)

        # Polygonize tree cover raster or sieved raster.
        # Close to remove artifacts created by resampling if needed
        if TC_builder.out_resample is not None:
            outputs[self.OUTPUT_RASTER] = TC_builder.out_resample.dataProvider(
            ).dataSourceUri()
        else:
            outputs[self.OUTPUT_RASTER] = TC_builder.out_index.dataProvider(
            ).dataSourceUri()

        if compute_sieve:
            outputs[self.OUTPUT_SIEVE] = TC_builder.out_mask.dataProvider(
            ).dataSourceUri()

        if polygonize:
            (sink, id) = self.parameterAsSink(
                parameters,
                self.OUTPUT_VECTOR,
                context,
                TC_builder.out_vector.fields(),
                TC_builder.out_vector.wkbType(),
                TC_builder.out_vector.sourceCrs())

            for feature in TC_builder.out_vector.getFeatures():
                sink.addFeature(feature, QgsFeatureSink.FastInsert)

            outputs[self.OUTPUT_VECTOR] = id

        return outputs

    def icon(self):
        """
        Should return a QIcon which is used for your provider inside
        the Processing toolbox.
        """
        return QIcon(":/plugins/hedge_tools/images/hedge_tools.png")

    def shortHelpString(self):
        """
        Returns a localised short helper string for the algorithm. This string
        should provide a basic description about what the algorithm does and
        the parameters and outputs associated with it.
        """
        return self.tr(
            "Generate a tree cover raster from any intersecting \
             NDVI and DHM rasters that have the same resolution. \
             The algorithm allows you to choose a target resolution. \n\
             You can also select the resampling method to use \
             when adjusting the resolution. Some methods yield better results \
             for downsampling while others are better for upsampling. \
             Be aware that some methods (like nearest neighbor) \
             might create gaps between tiles. \n\
             It is also possible and recommended to use the sieve option to \
             remove small objects (connected pixels) in the masks \
             for further processing within HedgeTools. \
             The sieve value is measured in number of pixels. \n\
             The connectedness parameter specifies if 8 neighboring pixels \
             (diagonals included) should be tested for connection or only 4. \n\
             For further processing within the HedgeTools toolbox, \
             the tree cover should be polygonized. \
             The tool offers this option with the polygonize \
             tree cover raster option. A morphological opening \
             of the tree cover using the resolution value is possible \
             to remove artifacts on the tile borders created by some \
             resampling algorithms.")

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "generatetreecover"

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr("3 - Generate tree cover")

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr("0 - Extraction [optional]")

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "extraction"

    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate("Processing", string)

    def checkParameterValues(self, parameters, context):

        if (parameters["RESOLUTION"] is not None
            and parameters["RESOLUTION"] != QgsRasterLayer(self.DHM).extent()) \
                and parameters["RESAMPLING"] is None:
            return (False, "Please select a resampling algorithm.")

        if (parameters["RESOLUTION"] is None
            or parameters["RESOLUTION"] == QgsRasterLayer(self.DHM).extent()) \
                and parameters["RESAMPLING"] is not None:
            return (False, "Please select a target resolution.")

        if parameters["POLYGONIZE"] is False and parameters["CLOSE"] is True:
            return (False, "Please select the polygonize parameter if \
                    you want to perform a morphological closing.")

        return (True, '')

    def createInstance(self):
        return GenerateTreeCoverProcessingAlgorithm()
