
from qgis.core import (QgsProcessingException,
                       QgsGeometry,
                       QgsRectangle,
                       QgsRasterLayer)
from typing import Union

def polygonize_input_validation(input_idx:int, args:list):
    """
    Will raise an error if input is not between 0 and 3 
    or if selected input is None.

    Parameters
    ---
    input_idx: int
        Index of input to polygonize
        -0: out_index
        -1: out_resample
        -2: out_mask
        -3: out_sieve

    args: list
        List of inputs to check
    """
    if input_idx is not None and input_idx not in range(4):
        raise QgsProcessingException(
            f"Polygonize input must be between 0 and 3. Got {input_idx}.")
    try:
        if args[input_idx] is None:
            raise QgsProcessingException(
                f"Input {input_idx} is None.")
    except:
        raise QgsProcessingException(
            f"Input {input_idx} is out of range.")

def polygonize_option_validation(polygonize:bool, close:bool):
    """
    Will raise an error if close is True but polygonize is False.

    Parameters
    ---
    polygonize: boolean
    close: boolean
    """
    if close and not polygonize:
        raise QgsProcessingException(
            "Polygonize must be toggle if you want to use the close option.")
    
def sieve_option_validation(compute_mask:bool, 
                            compute_sieve:bool):
    """
    Will raise an error if sieve output is wanted but mask output is not.
    This check should be made at the process_algorithm start.

    Parameters
    ---
    compute_mask: bool
        If True, will compute a masked output
    compute_sieve: bool
        If True, will coompute a sieved mask.
    """
    # Before launch condition (process_alg call)
    if not compute_mask and compute_sieve:
        raise QgsProcessingException(
            "Compute mask must be toggle if you want to use the sieve option.")

def sieve_call_validation(mask:Union[QgsRasterLayer, None]):
    """
    Will raise an error if compute_sieve method is called 
    but a mask isn't created yet.

    Parameters
    ---
    mask: Union[QgsRasterLayer, None]
    """
    # After launch condition (process_sieve call)
    if mask is None:
        raise QgsProcessingException(
            "A mask must be provided if you want to use the sieve option.")
    
def resampling_validation(resampling:int, target_res:float):
    """
    Will raise an error either if :
        - The selected resampling algorithm does not exist.
        - A valid resampling algorithm is selected 
        but the target resolution is 0.0.
        - A valid target resolution is selected 
        but a resampling algorithm is not selected.

    Parameters
    ---
    resampling: int
        Selected resampling algorithm.
    target_res: float
        Target resolution.
    """
    if resampling is not None and resampling not in range(12):
        raise QgsProcessingException(
            f"Resampling method must be between 0 and 11. Got {resampling}.")
    elif resampling is not None and target_res == 0.0:
        raise QgsProcessingException(
            f"A target resolution is needed for resampling.")
    elif resampling is None and target_res != 0.0:
        raise QgsProcessingException(
            f"A resampling algorithm is needed for resampling.")
    
def extent_validation(extent:Union[QgsGeometry, QgsRectangle]):
    """
    Will raise an error if the provided extent is empty.

    Parameters
    ---
    extent: Union[QgsGeometry, QgsRectangle]
        Processing extent.
    """
    if extent.isEmpty():
        raise QgsProcessingException(
            "Selected extent is either empty or not overlapping input extent")
    
def extent_option_validation(extent_opt):
    """
    Will raise an error if the extent option is not valid.

    Parameters
    ---
    extent_opt: int
        Extent opt is between 0 and 3.

        -0: Ignore
            Will ignore CRS and only check both inputs dimensions.
            Raise an error if both inputs have different dimensions.
        -1: Fail
            Will check if both inputs are in the same CRS 
            and have the same dimensions (Equality predicate). 
            Raise an error otherwise.
        -2: Union
            Execute algorithm on the union of inputs.
        -3: Intersect
            Execute algorithm on the intersection of inputs.
            Raise an error if there is no intersection.
    """
    if extent_opt not in range(4):
        raise QgsProcessingException(
            f"Extent option must be between 0 and 3. Got {extent_opt}.")
    
def projection_validation(input_A, input_B):
    """
    Will raise an error if both inputs are not in the same CRS.
    
    Parameters
    ---
    input_A: QgsRasterLayer
    input_B: QgsRasterLayer
    """
    if input_A.crs().authid() != input_B.crs().authid():
        raise QgsProcessingException(
            f"{input_A.name()} and {input_B.name()} must have the same CRS.")

def resolution_validation(input_A, input_B):
    """
    Will raise an error if both inputs have different resolutions.

    Parameters
    ---
    input_A: QgsRasterLayer
    input_B: QgsRasterLayer
    """
    # Assuming squared pixels
    if input_A.rasterUnitsPerPixelX() != input_B.rasterUnitsPerPixelX(): 
        raise QgsProcessingException(
            f"{input_A.name()} and {input_B.name()}\
                  must have the same resolution.")
    
def dimension_validation(input_A, input_B):
    """
    Will raise an error if both inputs 
    have different dimensions (width and height).
    Projection is dismissed.
    
    Parameters
    ---
    input_A: QgsRasterLayer
    input_B: QgsRasterLayer
    """
    if input_A.width() != input_B.width() \
            or input_A.height() != input_B.height():
        raise QgsProcessingException(
            f"{input_A.name()} and {input_B.name()}\
                must have the same dimensions.")
    
def alignment_validation(input_A, input_B):
    """
    Perform an equality check and raise an error 
    if both inputs are not aligned.
    Projection and dimensions are checked.

    Parameters
    ---
    input_A: QgsRasterLayer
    input_B: QgsRasterLayer
    """
    geom_A = QgsGeometry().fromRect(input_A.extent())
    geom_B = QgsGeometry().fromRect(input_B.extent())
    if not geom_A.equals(geom_B):
        raise QgsProcessingException(
            f"{input_A.name()} and {input_B.name()} are not aligned")
    
def intersection_validation(input_A, input_B):
    """
    Perform an intersection check and raise an error
    if both inputs are not overlapping.
    
    Parameters
    ---
    input_A: QgsRasterLayer
    input_B: QgsRasterLayer
    """
    if input_A.extent().intersects(input_B.extent()) is False:
        raise QgsProcessingException(
            f"{input_A.name()} and {input_B.name()} are not overlapping")

