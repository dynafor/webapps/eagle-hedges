from qgis.core import (QgsRasterLayer, 
                       QgsProcessingFeedback,
                       QgsProcessingContext)
from .GenerateRaster import GenerateRaster
from ..raster import wrapper as wr
from ..raster import utils as rutils

from typing import Union


class GenerateDHM(GenerateRaster):
    def __init__(self, DEM:tuple[Union[QgsRasterLayer, str], int], 
                 DSM:tuple[Union[QgsRasterLayer, str], int],
                 feedback:QgsProcessingFeedback=None,
                 context:QgsProcessingContext=None,
                 alg_name:str="Generate DHM"):
        """
        Compute DHM from a DEM and DSM raster/band.
        Both inputs can be from the same raster.

        Parameters
        ---
        DEM: tuple[Union[QgsRasterLayer, str], int]
            Digital Elevation Model raster and band
        DSM: tuple[Union[QgsRasterLayer, str], int]
            Digital Surface Model raster and band
        feedback: QgsProcessingFeedback
            Advised if executed within QgsProcessingAlgorithm
        context: QgsProcessingContext
            Advised if executed within QgsProcessingAlgorithm
        alg_name: str
            Name of the algorithm. 
            Advised to use self.displayName() if executed within QGIS.
        """
        super().__init__(DEM, DSM, feedback, context, alg_name)

    def process_index(self, out_path:str="TEMPORARY_OUTPUT", 
                      overwrite:bool=True, extent_opt:int=3):
        """
        Compute DHM from a DEM and DSM raster/band.
        Store the result in self.out_index.

        Parameters
        ---
        out_path: str
            Output path. Default is in tmp
        overwrite: bool
            Overwrite output if already exists. Default is True.
        extent_opt: int
            Extent option for gdal_calc.

            -0: Ignore
                Will ignore CRS and only check both inputs dimensions.
                Raise an error if both inputs have different dimensions.
            -1: Fail
                Will check if both inputs are in the same CRS 
                and have the same dimensions (Equality predicate). 
                Raise an error otherwise.
            -2: Union
                Execute algorithm on the union of inputs.
            -3: Intersect
                Execute algorithm on the intersection of inputs.
                Raise an error if there is no intersection.
        """
        formula = "(B.astype(float32) - A.astype(float32))"
        
        self.out_index = self._load(
            wr.gdal_calc([self.input_A, self.input_B], 
                         formula, output=out_path, 
                         overwrite=overwrite, 
                         extent_opt=extent_opt))

    def process_algorithm(self, extent_opt:Union[int, None]=None,
                          target_res:float=0.0, 
                          resampling:Union[int, None]=None, 
                          compute_mask:bool=True,
                          threshold:float=2.5,
                          overwrite:bool=True,
                          dhm_path="TEMPORARY_OUTPUT", 
                          mask_path="TEMPORARY_OUTPUT"):
        """
        Method that store algorithm execution 
        to compute DHM and DHM mask.

        Parameters
        ---
        extent_opt: Union[int, None]
            Extent option for gdal_calc.

            -0: Ignore
                Will ignore CRS and only check both inputs dimensions.
                Raise an error if both inputs have different dimensions.
            -1: Fail
                Will check if both inputs are in the same CRS 
                and have the same dimensions (Equality predicate). 
                Raise an error otherwise.
            -2: Union
                Execute algorithm on the union of inputs.
            -3: Intersect
                Execute algorithm on the intersection of inputs.
                Raise an error if there is no intersection.
        target_res: float
            Target resolution for resampling the NDVI raster.
        resampling: Union[int, None]
            Resampling method

            -0: Nearest Neighbour
            -1: Bilinear (2x2 Kernel)
            -2: Cubic (4x4 Kernel)
            -3: Cubic B-Spline (4x4 Kernel)
            -4: Lanczos (6x6 Kernel)
            -5: Average
            -6: Mode
            -7: Maximum
            -8: Minimum
            -9: Median
            -10: First Quartile (Q1)
            -11: Third Quartile (Q3)
        compute_mask: bool
            Compute DHM mask. Default is True
        threshold: bool
            Threshold to compute DHM mask. Default is 2.5.
        overwrite: bool
            Overwrite output if already exists. Default is True.
        dhm_path: str
            Output path for DHM raster. Default is in tmp
        mask_path: str
            Output path for DHM mask. Default is in tmp
        """
        self._input_validation(extent_opt)

        nb_alg = self._define_nb_alg(target_res, compute_mask)
        self._init_progress_bar(nb_alg)

        if target_res != 0.0 and \
                self.input_A[0].rasterUnitsPerPixelX() != target_res:
            self._push_info("Computing DHM...")
            self.process_index(extent_opt=extent_opt)
            self._progress_bar("Resampling DHM...")
            self.resample(target_res, resampling, dhm_path)
            self._progress_bar()
        else:
            self._push_info("Computing DHM...")
            self.process_index(dhm_path, overwrite, extent_opt)
            self._progress_bar()

        if compute_mask:
            self._push_info("Thresholding DHM...")
            self.process_mask(threshold, mask_path, overwrite, extent_opt)
            self._progress_bar()