from abc import abstractmethod

from qgis.core import (QgsRasterLayer,
                       QgsRectangle,
                       QgsGeometry,
                       QgsVectorLayer,
                       QgsProcessingFeedback,
                       QgsProcessingContext)
from ..raster import wrapper as wr
from ..raster import utils as rutils
from ..vector import qgis_wrapper as qw
from .. import checkers as ch

from typing import Union

class GenerateRaster():
    def __init__(self, 
                 input_A:tuple[Union[QgsRasterLayer, str], int], 
                 input_B:tuple[Union[QgsRasterLayer, str], int],
                 feedback:QgsProcessingFeedback=None,
                 context:QgsProcessingContext=None,
                 alg_name:str=None):
        """
        Mother class to generate a raster with gdal_calc 
        from two rasters or two bands.
        Both inputs can be from the same raster.

        Parameters
        ---
        input_A: tuple[Union[QgsRasterLayer, str], int]
            Raster and band to use
        input_B: tuple[Union[QgsRasterLayer, str], int]
            Raster and band to use
        feedback: QgsProcessingFeedback
            Advised if executed within QgsProcessingAlgorithm
        context: QgsProcessingContext
            Advised if executed within QgsProcessingAlgorithm
        alg_name: str
            Name of the algorithm. 
            Advised to use self.displayName() if executed within QGIS.
        """
        self.input_A = (self._load(input_A[0]), input_A[1])
        self.input_B = (self._load(input_B[0]), input_B[1])
        self.feedback = feedback
        self.context = context
        self._name = alg_name.split(" - ")[-1] \
            if alg_name is not None else None
        self._value = None
        self._step = None
        self.out_index = None
        self.out_resample = None
        self.out_mask = None
        self.out_sieve = None
        self.out_vector = None

    @abstractmethod
    def process_index(self):
        """
        Base method to compute index with gdal_calc (NDVI, MNH, TreeCover, ...)
        """
        pass
    
    @abstractmethod
    def process_algorithm(self):
        """
        Base method to store algorithm execution.
        """
        pass

    def _load(self, raster:Union[QgsRasterLayer, str])->QgsRasterLayer:
        """
        Check if a raster is already loaded and load it if needed

        Parameters
        ---
        raster: QgsRasterLayer
            Raster to check and load if needed
        """
        return rutils.load_raster(raster)
    
    def _define_nb_alg(self, target_res:float=0.0, 
                        compute_mask:bool=False, 
                        compute_sieve:bool=False,
                        extent:Union[QgsRectangle, QgsGeometry]=None,
                        polygonize:bool=False):
        """
        Define step of the progress bar given 
        the booleans toggled by the user

        Parameters
        ---
        target_res: float
            Target resolution, if != 0 will add an alg number
        compute_mask: bool
            Compute mask, if True will add an alg number
        compute_sieve: boolan alg number
        extent: QgsRectangle, QgsGeometry
            Extent of the raster, if not None or not Empty will add an alg number
        polygonize: bool
            Polygonize raster, if True will add an alg number

        Returns
        ---
        nb_alg: int
        """
        nb_alg = 1
        if target_res != 0.0:
            nb_alg += 1
        if compute_mask:
            nb_alg += 1
        if compute_sieve:
            nb_alg += 1
        try:
            if extent is not None and extent.isEmpty() == False:
                nb_alg += 1
        except AttributeError:
            pass

        if polygonize:
            nb_alg += 1
        
        return nb_alg

    def _init_progress_bar(self, nb_alg:int, message:str=None):
        """
        Initialize progress bar if the algorithm is executed within QGIS.

        Parameters
        ---
        nb_alg: int
            Number of alg to execute.
            Define the step of the progress bar
            if a feedback object is provided.
        message: str
            Message to display in the tool GUI

        """
        if self.feedback is not None:
            self.value = 1
            self.step = 100/nb_alg
            self.feedback.setProgress(self.value)

            if message is not None:
                self._push_info(message)
            
            if self.value == 1 and message is None:
                self._push_info(f"Algorithm {self._name} started")


    def _progress_bar(self, message:str=None):
        """
        Update the progress bar if the algorithm is executed within QGIS.

        Parameters
        ---
        message: str
            Message to display in the tool GUI
        """
        if self.feedback is not None:

            if message is not None:
                self._push_info(message)

            self.value += self.step
            self.feedback.setProgress(self.value)
            # Check for cancellation
            if self.feedback.isCanceled():
                return {}
            
            if self.value >= 100 and message is None:
                self._push_info(
                    f"Algorithm {self._name} terminated without error \n")

    def _push_info(self, message:str):
        """
        Push info in the tool GUI.

        Parameters
        ---
        message: str
            Message to display in the tool GUI
        """
        if self.feedback is not None:
            self.feedback.pushInfo(message)

    def _input_validation(self, extent_opt:int=None, 
                          compute_mask:bool=None, 
                          compute_sieve:bool=None,
                          polygonize:bool=None,
                          open:bool=None):
        """
        CRS, resolution, extent_opt validation between input A and B.
        Validation of options for sieving and polygonizing outputs.

        Parameters
        ---
        extent_opt: int
            Extent option for gdal_calc.
            Used to check the condition to respect for inputs.
        compute_mask: bool
            If True, the mask will be computed.
        compute_sieve: bool
            If True, the sieve will be computed.
            Throw an error if compute_sieve is True but compute_mask is False.
        polygonize: bool
            If True, will convert raster to vector
        open: bool
            If True, morphological opening will be computed on the vector.
            Will throw an error if True and polygonize is False.
        
        """
        if extent_opt != 0:
            ch.projection_validation(self.input_A[0], self.input_B[0])
        ch.resolution_validation(self.input_A[0], self.input_B[0])

        if compute_mask is not None and compute_sieve is not None:
            ch.sieve_option_validation(compute_mask, compute_sieve)

        if polygonize is not None and open is not None:
            ch.polygonize_option_validation(polygonize, open)

        if extent_opt is not None:
            ch.extent_option_validation(extent_opt)

            if extent_opt == 0:   
                ch.dimension_validation(self.input_A[0], self.input_B[0])
            elif extent_opt == 1:
                ch.alignment_validation(self.input_A[0], self.input_B[0])
            elif extent_opt == 3:
                ch.intersection_validation(self.input_A[0], self.input_B[0])

    def resample(self, target_res:float=0.0, resampling:Union[int, None]=None, 
                 out_path:str="TEMPORARY_OUTPUT")->QgsRasterLayer:
        """
        Resample out_index to a target resolution.
        Store the result in self.out_resample.

        Parameters
        ---
        target_res: float
            Target resolution
        resampling: Union[int, None]
            Resampling method

            -0: Nearest Neighbour
            -1: Bilinear (2x2 Kernel)
            -2: Cubic (4x4 Kernel)
            -3: Cubic B-Spline (4x4 Kernel)
            -4: Lanczos (6x6 Kernel)
            -5: Average
            -6: Mode
            -7: Maximum
            -8: Minimum
            -9: Median
            -10: First Quartile (Q1)
            -11: Third Quartile (Q3)
        out_path: str
            Output path. Default is in tmp folder
        overwrite: bool
            Overwrite output if already exists. Default is True
        """
        ch.resampling_validation(resampling, target_res)
  
        self.out_resample = self._load(
            wr.gdal_warp(self.out_index, target_res, 
                         self.out_index.extent(), 
                         resampling, output=out_path))
    
    def clip_by_extent(self, extent:Union[QgsRasterLayer, str, 
                                          QgsRectangle, QgsGeometry]
                                          )->QgsRasterLayer:
        """
        Clip inputs by an extent and update self.input_A and self.input_B

        Parameters
        ---
        extent: QgsRasterLayer, str, QgsRectangle, QgsGeometry
            Extent of the wanted raster to process.
            If no common extent an error will be raised.
        """
        if extent.area() != float("inf"): # User inputed an extent
            common_ext = rutils.get_common_extent(self.input_A[0], extent)
            ch.extent_validation(common_ext)

            if self.input_A[0].dataProvider().dataSourceUri() \
                != self.input_B[0].dataProvider().dataSourceUri():
                
                input_A = self._load(
                    wr.gdal_clip_by_extent(self.input_A[0], common_ext)) 
                input_B = self._load(
                    wr.gdal_clip_by_extent(self.input_B[0], common_ext))
                self.input_A = (input_A, self.input_A[1])
                self.input_B = (input_B, self.input_B[1])
            else:
                input_A = self._load(
                    wr.gdal_clip_by_extent(self.input_A[0], common_ext)) 
                self.input_A = (input_A, self.input_A[1])
                self.input_B = (input_A, self.input_B[1])

    def process_mask(self, threshold:float=0.0,
                     out_path:str="TEMPORARY_OUTPUT",
                     overwrite:bool=True, extent_opt:int=3
                     )->QgsRasterLayer:
        """
        Compute a mask from the index raster 
        or the resampled raster if available.
        Store the result in self.output_mask.

        Parameters
        ---
        threshold: float
            Threshold to apply to the raster.
        out_path: str
            Output path. Default is in tmp
        overwrite: bool
            Overwrite output if already exists. Default is True
        extent_opt: int
            Extent option for gdal_calc.

            -0: Ignore
                Will ignore CRS and only check both inputs dimensions.
                Raise an error if both inputs have different dimensions.
            -1: Fail
                Will check if both inputs are in the same CRS 
                and have the same dimensions (Equality predicate). 
                Raise an error otherwise.
            -2: Union
                Execute algorithm on the union of inputs.
            -3: Intersect
                Execute algorithm on the intersection of inputs.
                Raise an error if there is no intersection.
        """

        if self.out_resample is not None:
            raster = self.out_resample
        else:
            raster = self.out_index

        self.out_mask = self._load(
            wr.gdal_calc([(raster, 1)], 
                         f"where(A >= {threshold}, 1, 0)", 
                         0, output=out_path, overwrite=overwrite, 
                         extent_opt=extent_opt))    
        
    def process_sieve(self, threshold:int=100, 
                      connectedness:bool=True , 
                      out_path:str="TEMPORARY_OUTPUT"
                      )->QgsRasterLayer:
        """
        Sieve masked output for the object 
        of size below the threshold number.
        Store the result in self.output_sieve.

        For some resolution sieve ouput can have 
        incorrect maximum value displayed.
        Users have to update symbology to 1 manually for now.

        Parameters
        ---
        threshold: int
            Number of connected pixels. Default is 100
            Object of size below this value will be erased
        connectedness: bool
            If True, 8D will be used for the object construction
            Otherwise 4D will be used.
            Default is True.
        out_path: str
            Output path. Default is in tmp
        """
        ch.sieve_call_validation(mask=self.out_mask)

        self.out_sieve = self._load(
            wr.gdal_sieve(self.out_mask, threshold, 
                          connectedness, out_path))
            

    def convert_to_vector(self, input_idx:int=0, open:bool=True, 
                          out_path:str="TEMPORARY_OUTPUT"):
        """
        Polygonyze a binary raster.
        An option is available to perform a morphological closing 
        of one pixel resolution value. Avoiding gap between tiles 
        with some resample algorithm.


        Parameters
        ---
        input_idx: int
            Input raster to polygonyze.
            Avialable value :

            -0: out_index
            -1: out_resample
            -2: out_mask
            -3: out_sieve

        open: bool
            If True, a morphological closing will be performed.
            Default is True.
        out_path: str
            Output path for polygonized output. Default is in tmp.
        """
        inputs = [self.out_index, self.out_resample, self.out_mask, self.out_sieve]
        ch.polygonize_input_validation(input_idx, inputs)
        raster = inputs[input_idx]
        if open:
            resolution = raster.rasterUnitsPerPixelX()
            vector = wr.gdal_polygonize(raster)
            vector = qw.closing(vector, resolution, True, 1, 1)
            self.out_vector = qw.multipart_to_singleparts(vector, output=out_path)
            if "fid" in self.out_vector.fields().names():
                idx = self.out_vector.fields().indexFromName("fid")
                attr_map = {f.id():{idx:f.id()} for f in self.out_vector.getFeatures()}
                self.out_vector.dataProvider().changeAttributeValues(attr_map)
        else:   
            self.out_vector = wr.gdal_polygonize(raster, output=out_path)

        if isinstance(self.out_vector, QgsVectorLayer):
                self.out_vector.setName("Tree Cover")
        else:
            self.out_vector = QgsVectorLayer(self.out_vector, "Tree_Cover", "ogr")
            
