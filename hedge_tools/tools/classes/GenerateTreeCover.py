from qgis.core import (QgsRasterLayer, 
                       QgsProcessingFeedback,
                       QgsProcessingContext)
from .GenerateRaster import GenerateRaster
from ..raster import wrapper as wr
from .. import checkers as ch

from typing import Union


class GenerateTreeCover(GenerateRaster):
    def __init__(self, DHM:tuple[Union[QgsRasterLayer, str], int], 
                 NDVI:tuple[Union[QgsRasterLayer, str], int],
                 feedback:QgsProcessingFeedback=None,
                 context:QgsProcessingContext=None,
                 alg_name:str="Generate tree cover"):
        """
        Compute a tree cover raster from a DHM and NDVI raster/band.
        Both inputs can be from the same raster.

        Parameters
        ---
        DHM: tuple[Union[QgsRasterLayer, str], int]
            Digital Height Model raster and band
        NDVI: tuple[Union[QgsRasterLayer, str], int]
            Normalized Difference Vegetation Index raster and band
        feedback: QgsProcessingFeedback
            Advised if executed within QgsProcessingAlgorithm
        context: QgsProcessingContext
            Advised if executed within QgsProcessingAlgorithm
        alg_name: str
            Name of the algorithm. 
            Advised to use self.displayName() if executed within QGIS.
        """
        super().__init__(DHM, NDVI, feedback, context, alg_name)

    def process_index(self, out_path:str="TEMPORARY_OUTPUT", 
                      overwrite:bool=True, extent_opt:int=3):
        """
        Compute tree cover from a DHM and NDVI raster.
        Store the result in self.out_index.

        Parameters
        ---
        out_path: str
            Output path. Default is in tmp
        overwrite: bool
            Overwrite output if already exists. Default is True.
        extent_opt: int
            Extent option for gdal_calc.

            -0: Ignore
                Will ignore CRS and only check both inputs dimensions.
                Raise an error if both inputs have different dimensions.
            -1: Fail
                Will check if both inputs are in the same CRS 
                and have the same dimensions (Equality predicate). 
                Raise an error otherwise.
            -2: Union
                Execute algorithm on the union of inputs.
            -3: Intersect
                Execute algorithm on the intersection of inputs.
                Raise an error if there is no intersection.
        """
        formula = "A.astype(uint8) * B.astype(uint8)"
        
        self.out_index = self._load(
            wr.gdal_calc([self.input_A, self.input_B], 
                         formula, output=out_path, 
                         overwrite=overwrite,
                         extent_opt=extent_opt))
    
    def process_sieve(self, threshold:int=100, 
                      connectedness:bool=True, 
                      out_path:str="TEMPORARY_OUTPUT"
                      )->QgsRasterLayer:
        """
        Overriding of parent method to avoid sieve_call_validation
        for tree cover as it is already a mask.
        Sieve masked output for the object 
        of size below the threshold number.
        Store the result in self.output_sieve.

        For some resolution sieve ouput can have 
        incorrect maximum value displayed.
        Users have to update symbology to 1 manually for now.


        Parameters
        ---
        threshold: int
            Number of connected pixels. Default is 100
            Object of size below this value will be erased
        connectedness: bool
            If True, 8D will be used for the object construction
            Otherwise 4D will be used.
            Default is True.
        out_path: str
            Output path. Default is in tmp
        """
        self.out_mask = self.out_index
        return super().process_sieve(threshold, connectedness, out_path)
    
    def process_algorithm(self, extent_opt:Union[int, None]=None,
                          target_res:float=0.0, 
                          resampling:Union[int, None]=None, 
                          compute_sieve:bool=True,
                          threshold_sieve:float=100,
                          connectedness:bool=True,
                          polygonize:bool=True,
                          open:bool=True,
                          overwrite:bool=True,
                          tree_cover_path="TEMPORARY_OUTPUT", 
                          sieve_path="TEMPORARY_OUTPUT",
                          vector_path="TEMPORARY_OUTPUT"):
        """
        Method that store algorithm execution 
        to compute tree cover, sieved tree cover 
        and vectorized tree cover.

        Parameters
        ---
        extent_opt: Union[int, None]
            Extent option for gdal_calc.

            -0: Ignore
                Will ignore CRS and only check both inputs dimensions.
                Raise an error if both inputs have different dimensions.
            -1: Fail
                Will check if both inputs are in the same CRS 
                and have the same dimensions (Equality predicate). 
                Raise an error otherwise.
            -2: Union
                Execute algorithm on the union of inputs.
            -3: Intersect
                Execute algorithm on the intersection of inputs.
                Raise an error if there is no intersection.
        target_res: float
            Target resolution for resampling the NDVI raster.
        resampling: Union[int, None]
            Resampling method

            -0: Nearest Neighbour
            -1: Bilinear (2x2 Kernel)
            -2: Cubic (4x4 Kernel)
            -3: Cubic B-Spline (4x4 Kernel)
            -4: Lanczos (6x6 Kernel)
            -5: Average
            -6: Mode
            -7: Maximum
            -8: Minimum
            -9: Median
            -10: First Quartile (Q1)
            -11: Third Quartile (Q3)
        compute_sieve: bool
            Compute sieved mask. Default is True
        threshold_sieve: float
            Threshold to compute sieved mask. efault is 100.
            Number of connected pixels.
            Object of size below this value will be erased.
        connectedness: bool
            Connectedness parameter for sieve option.
            If True, 8D will be used for the object construction
            Otherwise 4D will be used.
            Default is True.
        polygonize: bool
            Polygonize the tree cover or the sieved tree cover. 
            Default is True.
            There is no option to polygonize with 8D as it create 
            too much problem when creating median axis (narrow gap).
        open: bool [Might not be needed anymore]
            Morphological opening of the polygonized output. Default is True.
            Value of either input resolution or resampled resolution will be used to avoid
            artifact in raster border.
        overwrite: bool
            Overwrite output if already exists. Default is True.
        tree_cover_path: str
            Output path for NDVI raster. Default is in tmp
        sieve_path: str
            Output path for sieved mask. Default is in tmp
        vector_path: str
            Output path for polygonized output. Default is in tmp
        """
        self._input_validation(extent_opt, True,
                              compute_sieve, polygonize, open)
        nb_alg = self._define_nb_alg(target_res, False, 
                                     compute_sieve, None, polygonize)
        self._init_progress_bar(nb_alg)

        if target_res != 0.0 and \
                self.input_A[0].rasterUnitsPerPixelX() != target_res:
            self._push_info("Computing tree cover...")
            self.process_index(extent_opt=extent_opt)
            self._progress_bar("Resampling tree cover...")
            self.resample(target_res, resampling, tree_cover_path)
            self._progress_bar()
            input_idx = 1
        else:
            self._push_info("Computing tree cover...")
            self.process_index(tree_cover_path, overwrite, extent_opt)
            self._progress_bar()
            input_idx = 0

        if compute_sieve:
            self._push_info("Sieving tree cover...")
            self.process_sieve(threshold_sieve, connectedness, sieve_path)
            self._progress_bar()
            input_idx = 3

        if polygonize:
            self._push_info("Converting tree cover raster to vector...")
            self.convert_to_vector(input_idx, open, vector_path)
            self._progress_bar()