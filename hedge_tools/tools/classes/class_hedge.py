import collections
import os
import statistics as stats
import tempfile
from itertools import combinations

import numpy as np
import processing
from osgeo import gdal, ogr, osr
from qgis.PyQt.QtCore import QVariant
from qgis.core import (QgsProcessingUtils,
                       QgsVectorLayer,
                       QgsGeometry,
                       QgsPoint,
                       QgsPointXY,
                       QgsFeatureRequest,
                       QgsFeature,
                       QgsRasterLayer,
                       NULL)
from qgis.core.additions.edit import edit

from hedge_tools.tools.raster import utils as rutils
from hedge_tools.tools.vector import attribute_table as attr
from hedge_tools.tools.vector import geometry as g
from hedge_tools.tools.vector import utils as vutils
from hedge_tools.tools.vector import attribute_table as at
from hedge_tools.tools.raster import process_array as pa

class Hedges():
    def __init__(self, in_poly_layer, in_arc_layer, in_node_layer):
        """
         A class used to facilitate switching between geometry of a hedge.

         Parameters
         ---
         in_poly_layer (QgsVectorLayer) : Polygon layer representing the hedges
         in_arc_layer (QgsVectorLayer) : Polyline layer representing the hedges
         in_node_layer (QgsVectorLayer) : Node layer representing the hedges
         """

        self.poly_layer = in_poly_layer
        self.arc_layer = in_arc_layer
        self.node_layer = in_node_layer
        self._relation_dict = collections.defaultdict(dict)
        self._make_relation()

    def _make_relation(self):
        """
         Build a nested dict storing the relation of the different geometries in an hedges.
         Ex : {key : {pid: int, eid: int, vids: [int, int]}
         """

        for idx, arc in enumerate(self.arc_layer.getFeatures()):
            # arc.setFields(self.arc_layer.fields(), initAttributes=False)
            self._relation_dict[idx]["pid"] = arc.attribute("pid")
            self._relation_dict[idx]["eid"] = arc.attribute("eid")
            self._relation_dict[idx]["vids"] = \
                [arc.attribute("vid_1"), arc.attribute("vid_2")]

    @property
    def relation(self):
        """
         Getter of the hedge relation nested dict

         Return
         ---
         self._relation_dict (nested dict) : Dict storing geometries relation of hedges
         """

        return self._relation_dict

    def get_fid_from_relation(self, in_id_type, in_id):
        """
         Getter of a specific element from the hedge relation nested dict.

         Parameters
         ---
         id_type (string) : Secondary key (id_type) of the nested dict
         id (int) : Id value of respective id_type

         Return
         ---
         pid, eid, vids : id value of related features corresponding to the id in input.

         TODO : Return both topology for nodes incase not O
         """

        if in_id_type == "pid" or in_id_type == "eid":
            row_dict = {k: v for k, v in self.relation.items() if v[in_id_type] == in_id}
        if in_id_type == "vids":
            row_dict = {k: v for k, v in self.relation.items() if in_id in v[in_id_type]}

        pid, eid, vids = [v for k, v in row_dict[list(row_dict)[0]].items()]

        poly = next(self.poly_layer.getFeatures(QgsFeatureRequest().
                                                setFilterExpression("pid = %d" %pid)))
        arc = next(self.arc_layer.getFeatures(QgsFeatureRequest().
                                                setFilterExpression("eid = %d" %eid)))
        node_1 = next(self.node_layer.getFeatures(QgsFeatureRequest().
                                                  setFilterExpression("vid = %d" %vids[0])))
        node_2 = next(self.node_layer.getFeatures(QgsFeatureRequest().
                                                  setFilterExpression("vid = %d" % vids[1])))

        return poly.id(), arc.id(), [node_1.id(), node_2.id()]

    def split_by_orientation(self, context, feedback, simpl=5, intern_angle=150,
                             ignore_dist=0, node_type_field="Node_type"):
        """
        Allow to split the hedges by orientation.
        Intra feature orientation must be computed.
        Therefore, an over simplification of the median axis is done to avoid noise.
        An option to ignore L node close to extremities( 25 by default) is given

        Parameters
        ----------
        self.poly_layer: QgisObject : QgsVectorLayer : Polygon
            Polygons layer
        self.arc_layer: QgisObject : QgsVectorLayer : LineString
            Skeleton of the polygons layer
        self.node_layer: QgisObject : QgsVectorLayer : Nodes
            Node layer representing hedge extrimities
        context : QgsContext : Context in which the tools are called
        feedback : QgsFeedback : Allow communication with the users (message, progress bar, ...)
        simpl : float : Default 5 : Simplification value for the arc.
            Arcs must reflect orientation that you need.
        intern_angle : int : Default 150 : Intern angle below a node will be classified as L (vertex)
        ignore_dist : int : Default 0 : If different of 0.
            erase one of the two cutline too close to each other
        node_type_field : string : Default Node_type :
            Field used to store node type (O,L,T, ...)

        Returns
        -------
        outpoly_layer : QgisObject : QgsVectorLayer : Polygon
            New polygon layer split with orientation
        outarc_layer : QgisObject : QgsVectorLayer : LineString
            New arc layer split with orientation
        node_layer : QgisObject : QgsVectorLayer : Node
            New node layer split with orientation
        """
        # Init progress bar steps
        alg_number = 9
        step_per_alg = int(100/alg_number)

        # Create copy of the layers as we don't want to modify our base class
        poly_layer = self.poly_layer
        arc_layer = self.arc_layer
        node_layer = self.node_layer

        # Duplicate pk/fk
        poly_fields = [poly_layer.fields().field("pid")]
        poly_layer = at.duplicate_fields(poly_layer, poly_fields)

        arc_fields = [field for field in arc_layer.fields() if 
                      field.name() in ["vid_1", "vid_2", "eid", "pid"]]
        arc_layer = at.duplicate_fields(arc_layer, arc_fields)

        node_fields = [node_layer.fields().field("vid")]
        node_layer = at.duplicate_fields(node_layer, node_fields)

        # Get pr
        pr_node = node_layer.dataProvider()

        # Get field index
        idx_node_id = node_layer.fields().indexFromName("vid")
        idx_node_fid = node_layer.fields().indexFromName("fid")

        idx_fk_node_1 = arc_layer.fields().indexFromName("vid_1")
        idx_fk_node_2 = arc_layer.fields().indexFromName("vid_2")

        feedback.pushInfo("Removing small orientation changes")

        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Simplify it to retain only big orientation change
        alg_name = "hedgetools:simplifymedianaxis"
        params = {"INPUT_POLY": poly_layer,
                  "INPUT_ARC": arc_layer,
                  "INPUT_NODE": node_layer,
                  "INPUT_DEV": simpl,
                  "OUTPUT_ARC": "TEMPORARY_OUTPUT",
                  "OUTPUT_NODE": "TEMPORARY_OUTPUT"}
        arc_simp = processing.run(alg_name, params)["OUTPUT_ARC"]

        # Set progress
        feedback.setProgress(step_per_alg)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Get node
        alg_name = "native:extractvertices"
        params = {"INPUT": arc_simp,
                  "OUTPUT": "TEMPORARY_OUTPUT"}
        node_dupe = processing.run(alg_name, params, context=context)["OUTPUT"]

        # Set progress
        feedback.setProgress(step_per_alg * 2)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Delete dupe node
        alg_name = "native:deleteduplicategeometries"
        params = {"INPUT": node_dupe,
                  "OUTPUT": "TEMPORARY_OUTPUT"}
        vertex_layer = processing.run(alg_name, params, context=context)["OUTPUT"]
        del(node_dupe)

        # Set progress
        feedback.setProgress(step_per_alg * 3)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Computing changes in orientation and reporting it to original geometry")

        # Get field max value
        max_id = node_layer.maximumValue(idx_node_id)
        max_fid = node_layer.maximumValue(idx_node_fid)
        # Create nodes that respect angle condition
        feature_dict = {}
        for vertex in vertex_layer.getFeatures():
            # Get parent arc
            _, fid_arc, _ = self.get_fid_from_relation("eid", vertex["eid"])
            arc = arc_simp.getFeature(fid_arc)
            if ignore_dist != 0:
                pnt = vertex.geometry().asPoint()
                dist_start, dist_end = g.distance_from_extremities(arc, pnt)

                # If too close of an extremities, ignore it
                if dist_start < ignore_dist or dist_end < ignore_dist:
                    continue

            # Compute angle
            _, _, before, after, _ = arc.geometry().closestVertex(vertex.geometry().asPoint())
            if before != -1 and after != -1:
                prev_node = arc.geometry().vertexAt(before)
                next_node = arc.geometry().vertexAt(after)
                angle = g.get_angle(vertex.geometry().asPoint(), QgsGeometry(prev_node), QgsGeometry(next_node))
                if angle < intern_angle:
                    # Get real node on real median axis and add it to a new node_layer
                    real_arc = arc_layer.getFeature(fid_arc)
                    real_vertex = real_arc.geometry().nearestPoint(vertex.geometry())

                    # Add new node
                    new_node = QgsFeature()
                    new_node.setGeometry(real_vertex)
                    new_node.setFields(node_layer.fields())
                    new_node["fid"] = max_fid + 1
                    new_node["vid"] = max_id + 1
                    new_node["Degree"] = 2
                    new_node[node_type_field] = "L"
                    if vertex["eid"] in feature_dict:
                        feature_dict[vertex["eid"]].append(new_node)
                    else:
                        feature_dict[vertex["eid"]] = [new_node]
                    max_fid += 1
                    max_id += 1
        del(vertex_layer)

        # Remove vertex in feature dict too close to each other
        expression = f"eid in {tuple(feature_dict.keys())}"
        request = QgsFeatureRequest().setFilterExpression(expression)
        for arc in arc_simp.getFeatures(request):
            arc_geom = arc.geometry()
            if len(feature_dict[arc["eid"]]) > 1:
                for n_1, n_2 in combinations(feature_dict[arc["eid"]], 2):
                    pnt_1 = n_1.geometry().asPoint()
                    pnt_2 = n_2.geometry().asPoint()

                    arc_geom = g.vertex_add(arc_geom, pnt_1.x(), pnt_1.y())
                    arc_geom = g.vertex_add(arc_geom, pnt_2.x(), pnt_2.y())
                    arc_feat = QgsFeature()
                    arc_feat.setGeometry(arc_geom)
                    # Check for distance between them
                    dist_start_1, _ = g.distance_from_extremities(arc_feat,
                                                                pnt_1)
                    dist_start_2, _ = g.distance_from_extremities(arc_feat,
                                                                pnt_2)
                    if abs(dist_start_2 - dist_start_1) < ignore_dist:
                        try:
                            feature_dict[arc["eid"]].remove(n_2)
                        except ValueError:
                            pass

        # Transform feature_dict to a list
        feature_list = sum(feature_dict.values(), [])
        pr_node.addFeatures(feature_list)

        # Set progress
        feedback.setProgress(step_per_alg * 4)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Building new arcs and updating topology")

        # Run the topological network algorithm to create arcs between nodes
        upd_arc_layer = g.update_arc_and_identifiers(node_layer, arc_layer)
        success = at.update_old_id(upd_arc_layer, arc_layer)
        
        if success == False:
            feedback.pushWarning("Could not save old id in arc layer")
            
        # Set progress
        feedback.setProgress(step_per_alg * 5)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # As fk_vid has been recomputed by topological network
        # we delete old ones who were truncated by v.net
        # with edit(arc_layer):
        #     arc_layer.deleteAttributes([idx_fk_node_1, idx_fk_node_2])

        # Cut polygon
        feedback.pushInfo("Cut lines creation")
        request = QgsFeatureRequest().setFilterExpression("%s = 'L'" %node_type_field)
        cutlines = vutils.create_layer(upd_arc_layer)
        cutline_layer = g.make_cutlines(cutlines, poly_layer, upd_arc_layer, node_layer,
                                        request)
        # Set progress
        feedback.setProgress(step_per_alg * 6)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Cut lines validation")
        cutline_valid = g.validate_cutlines(cutline_layer, poly_layer, upd_arc_layer)
        del(cutline_layer)

        # Set progress
        feedback.setProgress(step_per_alg * 7)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Cutting polygons with cut lines")
        outpoly_layer, outarc_layer = g.use_cutlines(cutline_valid, poly_layer,
                                                     upd_arc_layer, node_layer)
        del(cutline_valid)

        # Set progress
        feedback.setProgress(step_per_alg * 8)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        return outpoly_layer, outarc_layer, node_layer

    def split_by_distance(self, context, feedback, distance, ignore_dist=0, node_type_field="Node_type"):
        """
        Split regularly the hedges by a length criteria given by the users.
        Nodes created this way are labelled "L"

        Parameters
        ----------
        self.poly_layer: QgisObject : QgsVectorLayer : Polygon
            Polygons layer
        self.arc_layer: QgisObject : QgsVectorLayer : LineString
            Skeleton of the polygons layer
        self.node_layer: QgisObject : QgsVectorLayer : Nodes
            Node layer representing hedge extrimities
        context : QgsContext : Context in which the tools are called
        feedback : QgsFeedback : Allow communication with the users (message, progress bar, ...)
        distance : int : Distance criteria to split the hedges
        ignore_dist : int : Default 0 : If different of 0.
            erase one of the two cutline too close to each other
        node_type_field : string : Default Node_type :
            Field used to store node type (O,L,T, ...)

        Returns
        -------
        outpoly_layer : QgisObject : QgsVectorLayer : Polygon
            New polygon layer split by length
        outarc_layer : QgisObject : QgsVectorLayer : LineString
            New arc layer split by length
        node_layer : QgisObject : QgsVectorLayer : Node
            New node layer split by length
        """
        # Init progress bar steps
        alg_number = 8
        step_per_alg = int(100/alg_number)

        # Create copy of the layers as we don't want to modify our base class
        poly_layer = self.poly_layer
        arc_layer = self.arc_layer
        node_layer = self.node_layer

        # Duplicate pk/fk
        poly_fields = [poly_layer.fields().field("pid")]
        poly_layer = at.duplicate_fields(poly_layer, poly_fields)

        arc_fields = [field for field in arc_layer.fields() if 
                      field.name() in ["vid_1", "vid_2", "eid", "pid"]]
        arc_layer = at.duplicate_fields(arc_layer, arc_fields)

        node_fields = [node_layer.fields().field("vid")]
        node_layer = at.duplicate_fields(node_layer, node_fields)

        pr_node = node_layer.dataProvider()

        # Get field idx
        idx_node_id = node_layer.fields().indexFromName("vid")
        idx_node_fid = node_layer.fields().indexFromName("fid")
        idx_node_type = node_layer.fields().indexFromName("%s" % node_type_field)
        idx_fk_node_1 = arc_layer.fields().indexFromName("vid_1")
        idx_fk_node_2 = arc_layer.fields().indexFromName("vid_2")

        feedback.pushInfo("Creating nodes at given interval")

        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Create point along line given distance criteria
        alg_name = "native:pointsalonglines"
        params = {"INPUT": arc_layer,
                  "DISTANCE": distance,
                  "START_OFFSET": distance,
                  "END_OFFSET": distance,
                  "OUTPUT": "TEMPORARY_OUTPUT"}
        new_node_layer = processing.run(alg_name, params, context=context)["OUTPUT"]

        # Set progress
        feedback.setProgress(step_per_alg)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Integrating nodes into arcs")

        # Ignore_dist selection and adding new nodes in node_layer
        max_id = node_layer.maximumValue(idx_node_id)
        max_fid = node_layer.maximumValue(idx_node_fid)
        features = []
        for node in new_node_layer.getFeatures():
            # Get parent arc
            _, fid_arc, _ = self.get_fid_from_relation("eid", node["eid"])
            arc = arc_layer.getFeature(fid_arc)
            if ignore_dist != 0:
                # Get vertex id of the node
                arc_length = arc.geometry().length()
                upper = arc_length - ignore_dist
                lower = ignore_dist
                if node["distance"] < lower or node["distance"] > upper:
                    continue

            # max_id = node_layer.maximumValue(idx_node_id)
            # max_fid = node_layer.maximumValue(idx_node_fid)

            # Add new node
            max_fid += 1
            max_id += 1

            new_node = QgsFeature()
            new_node.setGeometry(node.geometry())
            new_node.setFields(node_layer.fields())
            new_node["fid"] = max_fid
            new_node["vid"] = max_id
            new_node["Degree"] = 2
            new_node[node_type_field] = "temp"
            features.append(new_node)
            
            # pr_node.addFeature(new_node)
            # node_layer.updateExtents()
        pr_node.addFeatures(features)
        node_layer.updateExtents()
        
        # Set progress
        feedback.setProgress(step_per_alg * 2)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        alg_name = "native:snapgeometries"
        params = {"INPUT": arc_layer,
                  "REFERENCE_LAYER": node_layer,
                  "TOLERANCE": 0.1,
                  "BEHAVIOR": 0,
                  "OUTPUT": "TEMPORARY_OUTPUT"}
        arc_layer = processing.run(alg_name, params, context=context)["OUTPUT"]

        # Set progress
        feedback.setProgress(step_per_alg * 3)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Building new arcs and updating topology")

        # Run the topological network algorithm to create arcs between nodes
        upd_arc_layer = g.update_arc_and_identifiers(node_layer, arc_layer)
        success = at.update_old_id(upd_arc_layer, arc_layer)
        if success == False:
            feedback.pushWarning("Could not save old id in arc layer")

        # Set progress
        feedback.setProgress(step_per_alg * 4)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # As fk_vid has been recomputed by topological network
        # we delete old ones who were truncated by v.net
        # with edit(arc_layer):
        #     arc_layer.deleteAttributes([idx_fk_node_1, idx_fk_node_2])

        # Cut polygon
        feedback.pushInfo("Cut lines creation")
        request = QgsFeatureRequest().setFilterExpression("%s = 'temp'" % node_type_field)
        cutlines = vutils.create_layer(upd_arc_layer)
        cutline_layer = g.make_cutlines(cutlines, poly_layer, upd_arc_layer, node_layer,
                                        request)

        # Set progress
        feedback.setProgress(step_per_alg * 5)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Cut lines validation")
        cutline_valid = g.validate_cutlines(cutline_layer, poly_layer, upd_arc_layer)
        del(cutline_layer)

        # Set progress
        feedback.setProgress(step_per_alg * 6)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Cutting polygons with cut lines")
        outpoly_layer, outarc_layer = g.use_cutlines(cutline_valid, poly_layer,
                                                     upd_arc_layer, node_layer)
        del(cutline_valid)

        # Set progress
        feedback.setProgress(step_per_alg * 7)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Rename temp node to L
        # Init attr map
        node_attr_map = {}
        for node in node_layer.getFeatures(request):
            node_attr_map[node.id()] = {idx_node_type: "L"}
        # Edit node layer with map
        node_layer.dataProvider().changeAttributeValues(node_attr_map)

        return outpoly_layer, outarc_layer, node_layer

    def split_by_interface(self, context, feedback, itf_layer, ori_bound=15,
                           ignore_dist=25,
                           node_type_field="Node_type"):
        """
        Split regularly the hedges by an adjacent interface changes
        Nodes created this way are labelled "L"
        The accuracy of the algorithm is directly linked to numerisation quality of interface layer.
        A little correction (snap) will be done in option but it is strongly advised
        that you correct the layer if you use one with overlap or unsnapped geometries

        Parameters
        ----------
        self.poly_layer: QgisObject : QgsVectorLayer : Polygon
            Polygons layer
        self.arc_layer: QgisObject : QgsVectorLayer : LineString
            Skeleton of the polygons layer
        self.node_layer: QgisObject : QgsVectorLayer : Node
            Nodes layer representing hedge extremities
        self.itf_layer : QgisObject : QgsVectorLayer : Polygons
            Polygon layer representing the interface to cut hedges with.
        context : QgsContext : Context in which the tools are called
        feedback : QgsFeedback : Allow communication with the users (message, progress bar, ...)
        itf_layer : QgisObject : QgsVectorLayer : Polygon
            Interface layer
        ori_bound : int : Junction orientation bounds to allow cutting the hedges with it.
            ori junction - 45 > ori hedge or ori junction + 45 < ori_hedge
        ignore_dist : int : Default 0 : If different from 0,
            erase one of the two cutline too close to each other
        node_type_field : string : Default Node_type :
            Field used to store node type (O,L,T, ...)

        Returns
        -------
        outpoly_layer : QgisObject : QgsVectorLayer : Polygon
            New polygon layer split by length
        outarc_layer : QgisObject : QgsVectorLayer : LineString
            New arc layer split by length
        node_layer : QgisObject : QgsVectorLayer : Node
            New node layer split by length
        """
        # Init progress bar steps
        alg_number = 7
        step_per_alg = int(100 / alg_number)

        # Create copy of the layers as we don't want to modify our base class
        poly_layer = self.poly_layer
        arc_layer = self.arc_layer
        node_layer = self.node_layer

        # Duplicate pk/fk
        poly_fields = [poly_layer.fields().field("pid")]
        poly_layer = at.duplicate_fields(poly_layer, poly_fields)

        arc_fields = [field for field in arc_layer.fields() if 
                      field.name() in ["vid_1", "vid_2", "eid", "pid"]]
        arc_layer = at.duplicate_fields(arc_layer, arc_fields)

        node_fields = [node_layer.fields().field("vid")]
        node_layer = at.duplicate_fields(node_layer, node_fields)

        # Set progress
        feedback.setProgress(step_per_alg * 2)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Cut lines creation")
        cutline_layer = g.make_cutlines_from_itf(poly_layer,
                                                 arc_layer,
                                                 itf_layer,
                                                 ori_bound=ori_bound)

        # Set progress
        feedback.setProgress(step_per_alg * 3)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Cut lines validation and nodes creation")
        cutline_layer, outnode_layer = g.validate_cutlines_from_itf(
                                                    arc_layer, node_layer,
                                                    cutline_layer,
                                                    ignore_dist=ignore_dist,
                                                    node_type_field=node_type_field)

        # Set progress
        feedback.setProgress(step_per_alg * 3)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Updating topology
        feedback.pushInfo("Building new arcs and updating topology")

        upd_arc_layer = g.update_arc_and_identifiers(node_layer, arc_layer)
        success = at.update_old_id(upd_arc_layer, arc_layer)
        if success == False:
            feedback.pushWarning("Could not save old id in arc layer")

        # Set progress
        feedback.setProgress(step_per_alg * 4)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Validation
        feedback.pushInfo("Cut lines validation")

        #cutline_valid = g.validate_cutlines(cutline_layer, poly_layer,
                                            #upd_arc_layer)
        #del(cutline_layer)

        # Set progress
        feedback.setProgress(step_per_alg * 5)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Cut polygons with cutline
        feedback.pushInfo("Cutting polygons with cut lines")

        outpoly_layer, outarc_layer = g.use_cutlines(cutline_layer, poly_layer,
                                                     upd_arc_layer, node_layer)

        # Set progress
        feedback.setProgress(step_per_alg * 6)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        return outpoly_layer, outarc_layer, outnode_layer

    def compute_length(self, context, feedback):
        """
        Compute the length of the median axis as a new field on median axis layer.

        Parameters
        ----------
        self.poly_layer: QgisObject : QgsVectorLayer : Polygon
            Layer containing the polygons to measure.
        self.arc_layer: QgisObject : QgsVectorLayer : LineString
            Skeleton of the polygons layer
        context : QgsContext : Context in which the tools are called
        feedback : QgsFeedback : Allow communication with the users (message, progress bar, ...)
        Returns
        -------
        ioself.arc_layer : Arc layer with a length attribute
        """
        # Init progress bar steps
        count = self.arc_layer.featureCount()

        feedback.pushInfo("Computing and storing length field")

        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Prepare fields_list for creation
        fields_list = [("length", QVariant.Double)]

        # Create new attributes if they already exist they will be overwriten
        idx = attr.create_fields(self.arc_layer, fields_list)[0]

        # Init attr_map
        arc_attr_map = {}
        # Populate length field
        for current, arc in enumerate(self.arc_layer.getFeatures()):
            length = round(arc.geometry().length(), 2)
            arc_attr_map[arc.id()] = {idx: length}

            # Set progress
            feedback.setProgress(int((current/count) * 100))

        # Edit with map
        self.arc_layer.dataProvider().changeAttributeValues(arc_attr_map)

        # Check for cancellation
        if feedback.isCanceled():
            return {}

    def compute_orientation(self, context, feedback):
        """
        Compute the orientation (clokcwise from North in degree)
        in a new attributes of the polygon layer. The arc are used to compute orientation
        as it is more accurate than polygon

        Parameters
        ----------
        self.arc_layer: QgisObject : QgsVectorLayer : LineString
            Skeleton of the polygons layer
        context : QgsContext : Context in which the tools are called
        feedback : QgsFeedback : Allow communication with the users (message, progress bar, ...)
        Returns
        -------
        ioself.arc_layer : Arc layer with an orientation attributes
         """
        # Init progress bar steps
        count = self.arc_layer.featureCount()

        feedback.pushInfo("Computing and storing orientation field")

        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Prepare fields_list for creation
        fields_list = [("direction", QVariant.Double)]

        # Create new attributes if they already exist they will be overwriten
        idx_ori = attr.create_fields(self.arc_layer, fields_list)[0]

        # Init attr map
        arc_attr_map = {}
        # Get main orientation
        for current, arc in enumerate(self.arc_layer.getFeatures()):
            # This method does 0 to 360 degree. Worth ?
            #start_pnt = arc.geometry().vertexAt(0)
            #end_pnt = arc.geometry.vertexAt(-1)
            #angle = start_pnt.azimuth(end_pnt)
            _, _, angle, _, _ = arc.geometry().orientedMinimumBoundingBox()
            arc_attr_map[arc.id()] = {idx_ori: round(angle, 2)}
            # Set progress
            feedback.setProgress(int((current / count) * 100))

        # Edit layer with attr map
        self.arc_layer.dataProvider().changeAttributeValues(arc_attr_map)

        # Check for cancellation
        if feedback.isCanceled():
            return {}

    def compute_shape_metrics(self, context, feedback):
        """
        Compute elongation, compacity and convexity of the polygon in three new attributes.

        Parameters
        ----------
        self.poly_layer: QgisObject : QgsVectorLayer : LineString
            Polygons layer
        context : QgsContext : Context in which the tools are called
        feedback : QgsFeedback : Allow communication with the users (message, progress bar, ...)
        Returns
        -------
        ioself.poly_layer : Polygon layer with three shape attributes
        """
        # Init progress bar steps
        count = self.poly_layer.featureCount()

        feedback.pushInfo("Computing and storing shape metrics fields")

        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Prepare fields_list for creation
        fields_list = [("elongation", QVariant.Double),
                       ("compacity", QVariant.Double),
                       ("convexity", QVariant.Double)]

        # Create new attributes if they already exist they will be overwritten
        idx_elong, idx_compac, idx_convex = attr.create_fields(self.poly_layer, fields_list)

        # Init attr map
        poly_attr_map = {}
        # Compute metrics
        for current, poly in enumerate(self.poly_layer.getFeatures()):
            _, _, _, width, height = poly.geometry().orientedMinimumBoundingBox()
            area = poly.geometry().convexHull().area()

            elongation = round(width / height, 2)
            compactness = round(4 * np.pi * (poly.geometry().area() / poly.geometry().length() ** 2), 2)
            convexity = round(poly.geometry().area() / area, 2)

            poly_attr_map[poly.id()] = {idx_elong: elongation,
                                        idx_compac: compactness,
                                        idx_convex: convexity}
            # Set progress
            feedback.setProgress(int((current / count) * 100))

        # Edit layer with attr map
        self.poly_layer.dataProvider().changeAttributeValues(poly_attr_map)

        # Check for cancellation
        if feedback.isCanceled():
            return {}

    def compute_height_metrics_from_DHM(self, context, feedback, mnh):
        """
        Compute height metrics as new fields : mean, median, min, max, var

        Parameters
        ----------
        self.poly_layer: QgisObject : QgsVectorLayer : Polygon
            Polygons layer
        context : QgsContext : Context in which the tools are called
        feedback : QgsFeedback : Allow communication with the users (message, progress bar, ...)
        mnh: QgisObject : QgsRasterLayer : Raster

        Returns
        -------
        outpoly_layer : QgisObject : QgsVectorLayer : Polygon
            Polygons layer with height metrics field.
        """
        # Init progress bar steps
        count = self.arc_layer.featureCount()

        feedback.pushInfo("Computing zonal statistics")

        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Prepare fields_list for deletion and creation
        fields_list = [("h_mean", QVariant.Double),
                       ("h_med", QVariant.Double),
                       ("h_min", QVariant.Double),
                       ("h_max", QVariant.Double),
                       ("h_std", QVariant.Double)]

        # Delete fields if it already exists as it'll cause an error in zonal_stat
        attr.delete_fields(self.poly_layer, [field[0] for field in fields_list])

        # Call zonal stat tool
        alg_name = "native:zonalstatisticsfb"
        params = {"INPUT": self.poly_layer,
                  "INPUT_RASTER": mnh,
                  "RASTER_BAND": 1,
                  "COLUMN_PREFIX": "h_",
                  "STATISTICS": "2, 3, 5, 6, 4",
                  "OUTPUT": "TEMPORARY_OUTPUT"}
        poly_layer_output = processing.run(alg_name, params)["OUTPUT"]

        # Set progress
        feedback.setProgress(50)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Creating height fields")
        # Create fields in self.poly_layer
        indexes = attr.create_fields(self.poly_layer, fields_list)

        # Get field index from self.poly_layer
        idx_mean = indexes[0]
        idx_med = indexes[1]
        idx_min = indexes[2]
        idx_max = indexes[3]
        idx_std = indexes[4]

        # Init attr map
        poly_attr_map = {}
        # Fill map of self.poly_layer with poly_layer_output fields
        for current, (poly1, poly2) in enumerate(zip(self.poly_layer.getFeatures(), poly_layer_output.getFeatures())):
            poly_attr_map[poly1.id()] = {idx_mean: round(
                                            poly2["h_mean"], 2),
                                        idx_med: round(
                                            poly2["h_median"], 2),
                                        idx_min: round(
                                            poly2["h_min"], 2),
                                        idx_max: round(
                                            poly2["h_max"], 2),
                                        idx_std: round(
                                            poly2["h_stdev"], 2)}

            # Set progress
            feedback.setProgress(50 + int((current / count) * 50))
            # Check for cancellation
            if feedback.isCanceled():
                return {}

        # Edit layer with map
        self.poly_layer.dataProvider().changeAttributeValues(poly_attr_map)

        # Check for cancellation
        if feedback.isCanceled():
            return {}

    def strata_overlap_from_DHM(self, context, feedback, mnh, bins, idx_list,
                                spatial_output=False):
        """
        Compute the percentage of height bins for each hedge.

        Parameters
        ----------
        self.poly_layer : QgsVectorLayer : Polygon : Hedges polygons
        context : QgsContext : Context in which the tools are called
        feedback : QgsFeedback : Allow communication with the users (message, progress bar, ...)
        mnh : QgsRasterLayer : MNH/MNC. Height raster.
        bins : list : List of values representing the height class to create.
        idx_list : list : Field index of indicators field.
        spatial_output : Boolean : If True output a new layer representing each catgeroy as a feature

        Returns
        -------
        outpoly_layer : QgisObject : QgsVectorLayer : Polygon
            Polygons layer with a field for each bin
            representing the percentage of this bin in the hedge.
        """
        total = self.poly_layer.featureCount()

        feedback.pushInfo(
            "Processing, given raster resolution it could be slow")

        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Open layer
        r_ds = rutils.qgis_raster_to_gdal(mnh)
        p_ds, layer_idx = vutils.qgis_vector_to_ogr(self.poly_layer)

        # Output arg
        mem_driver = ogr.GetDriverByName("Memory")
        mem_driver_gdal = gdal.GetDriverByName("MEM")

        # Raster info
        geot, projection, gdal_dtype, no_data = rutils.get_raster_metadata(r_ds)

        # Vector info
        fields = self.poly_layer.fields()

        # Load vector layer
        lyr = p_ds.GetLayer(layer_idx)
        p_feat = lyr.GetNextFeature()
        current = 1

        # If spatial output
        if spatial_output:
            # Init output QgsVectorLayer
            qgs_layer = vutils.create_layer(self.poly_layer, multi=True)
            fields_list = [("id_strata", QVariant.Int),
                           ("pid", QVariant.Int),
                           ("strata", QVariant.String),
                           ("overlap", QVariant.Double)]
            _ = attr.create_fields(qgs_layer, fields_list)

        # Init attr map
        poly_attr_map = {}
        while p_feat:
            if p_feat.GetGeometryRef() is not None:
                id = p_feat.GetFID()

                offsets = rutils.bounding_box_to_offsets(
                    p_feat.GetGeometryRef().GetEnvelope(), geot)
                new_geot = rutils.geot_from_offsets(offsets[0], offsets[2], geot)

                tr_array = g.rasterize_feature(mem_driver, mem_driver_gdal, 
                                               p_feat, offsets, new_geot)
                # Open only the region of the feature
                r_array = r_ds.GetRasterBand(1).ReadAsArray(offsets[2],
                                                            offsets[0],
                                                            offsets[3] -
                                                            offsets[2],
                                                            offsets[1] -
                                                            offsets[0])

                if r_array is not None:
                    # Create catgeory and unique id for each of them with D8
                    mask_ar = pa.create_bins(r_array, tr_array, bins, no_data)

                    if mask_ar is not None:
                        unique, _, overlap = pa.overlap_by_category(mask_ar)
                        poly_attr_map = pa.populate_attr_map(poly_attr_map, overlap, unique, 
                                                             fields, idx_list, id)
                        # Check for cancellation
                        if feedback.isCanceled():
                            return {}

                        if spatial_output:
                            # Init multipoly output:
                            multipoly_d = {}
                            # Burn mask and expand to 3 dim to fit in gdal
                            out_ar = np.where(mask_ar.mask, -1,
                                              mask_ar.data + 1)  # Need to add +1 because the 0 class is not write as id when polygonizing
                            exp_ar = np.expand_dims(out_ar, axis=2)
                            out_ds = rutils.write_image("", array=exp_ar, transform=new_geot, 
                                                        projection=projection, gdal_dtype=gdal_dtype, 
                                                        no_data=no_data, memory=True, output=True)

                            # Get feature raster ds
                            src_band = out_ds.GetRasterBand(1)

                            dst_layer, dst_ds = rutils.gdal_polygonize(data_set=None, 
                                                               band=src_band, 
                                                               mask=src_band, 
                                                               proj=projection)

                            # Check for cancellation
                            if feedback.isCanceled():
                                return {}

                            # Get geom
                            ogr_feat = dst_layer.GetNextFeature()

                            while ogr_feat:
                                qgs_feat = QgsFeature()
                                qgs_geom = g.ogr_geom_to_qgis_geom(ogr_feat)
 
                                qgs_layer, multipoly_d = at.update_field_from_DHM(
                                                            ogr_feat, qgs_feat, qgs_geom, qgs_layer, 
                                                            multipoly_d, unique, overlap, bins, id)
                                # Check for cancellation
                                if feedback.isCanceled():
                                    return {}

                                ogr_feat = dst_layer.GetNextFeature()

                        if spatial_output:
                            out_ds = None
                            dst_layer = None
                            dst_ds = None
                            qgs_layer = g.build_multipolygon(qgs_layer, multipoly_d, id)
 

            p_feat = lyr.GetNextFeature()
            # Set progress
            feedback.setProgress(int((current / total) * 100))
            current += 1

        # Edit poly_layer with attr map
        self.poly_layer.dataProvider().changeAttributeValues(poly_attr_map)

        # Check for cancellation
        if feedback.isCanceled():
            return {}

        if spatial_output:
            return qgs_layer

    def interface_type(self, context, feedback, layer_dict):
        """
        Retrieve intersecting interface (crop field, river, road, railway, building)
        and store their id in fields inside the polygon layer

        Parameters
        ----------
        self.poly_layer : QgisObject : QgsVectorLayer : Polygon.
            Polygon layer
        context : QgsContext : Context in which the tools are called
        feedback : QgsFeedback : Allow communication with the users (message, progress bar, ...)
        layer_dict : Dict : Contains interface type as key
                            and corresponding layer aswell as his unique id in dict values

        Returns
        -------
        self.poly_layer : QgisObject : QgsVectorLayer : Polygon.
            Polygon layer with new fields corresponding to intersecting features
        """
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Init progress bar steps
        count = self.poly_layer.featureCount() * len(layer_dict)

        # Deleting existing Adj_* fields if corresponding layers is informed by user
        del_fields_list = [f for f in self.poly_layer.fields().names()
                           if f[:-2] in ["Adj_%s"%k for k in layer_dict.keys()]]
        attr.delete_fields(self.poly_layer, del_fields_list)

        # Init fields related variable and result dictionnary
        max_idx = max(self.poly_layer.attributeList())
        fields_list = []
        poly_attr_map = {}

        feedback.pushInfo("Retrieving number of attributes to create")

        # Retrieve maximum intersection for each layer
        current = 0
        for key, (layer, _) in layer_dict.items():
            max_inter = 0
            for poly in self.poly_layer.getFeatures():
                i_count, _ = g.get_clementini(layer, poly.geometry())
                max_inter = i_count if max_inter < i_count else max_inter

                # Set progress
                current += 1
                feedback.setProgress(int((current / count) * 50))
                # Check for cancellation
                if feedback.isCanceled():
                    return {}

            layer_dict[key].append(max_inter)

        feedback.pushInfo("Retrieving unique id of intersecting interface")
        # Retrieve interface for each hedges
        current = 0
        for key, (layer, id_field, max_inter) in layer_dict.items():
            for poly in self.poly_layer.getFeatures():
                idx = max_idx + 1
                i_count, i_list = g.get_clementini(layer, poly.geometry())
                while i_count < max_inter:
                    i_list.append(NULL)
                    i_count += 1
                for feat in i_list:
                    if poly.id() not in poly_attr_map:
                        poly_attr_map[poly.id()] = {idx: feat[id_field] # Id_field
                                                    if feat is not NULL else feat}
                    else:
                        poly_attr_map[poly.id()] = {**poly_attr_map[poly.id()],
                                                    **{idx: feat[id_field]
                                                    if feat is not NULL else feat}}
                    idx += 1

                current += 1
                # Set progress
                feedback.setProgress(50 + int((current / count) * 45))
                # Check for cancellation
                if feedback.isCanceled():
                    return {}

            max_idx += max_inter

            # Format output fields
            fields_list += [("Adj_%s_%d" %(key, n), QVariant.String) for n in
                            range(1, max_inter + 1)]

        feedback.pushInfo("Formating output")

        # Create fields and fill them
        _ = attr.create_fields(self.poly_layer, fields_list)
        self.poly_layer.dataProvider().changeAttributeValues(poly_attr_map)

        # Set progress
        feedback.setProgress(100)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

    def shortest_distance(self, context, feedback, forest_layer, forest_id_field):
        """
        Compute the shortest distance on the fly between a hedge and a forest
        The distance is then stored in node layer attribute table with the forest id.

        Parameters
        ----------
        self.arc_layer: QgisObejct : QgsVectorLayer : LineString
            Arc layer
        context : QgsContext : Context in which the tools are called
        feedback : QgsFeedback : Allow communication with the users (message, progress bar, ...)
        forest_layer :  QgisObejct : QgsVectorLayer : Polygon or Point
        forest_id_field : Integer : Idx of the field to use as unique identifier

        Returns
        -------
        self.node_layer: QgisObejct : QgsVectorLayer : Point
            Updated node layer with the shortest distance to forest field.
        """
        # Init progress bar variable
        alg_number = 4
        step_per_alg = int(100 / alg_number)
        count = self.node_layer.featureCount()

        feedback.pushInfo("Computing nearest neghbor analysis for hedge extremities")

        # Compute nearest distance between hedge nodes and forest point boundary
        alg_name = "qgis:distancetonearesthubpoints"
        params = {"INPUT": self.node_layer,
                  "HUBS": forest_layer,
                  "FIELD": forest_id_field,
                  "UNIT": 0, # Meters
                  "OUTPUT": "TEMPORARY_OUTPUT"}
        dist_layer = processing.run(alg_name, params)["OUTPUT"]

        # Set progress
        feedback.setProgress(step_per_alg * 2)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Init output dict and output fields
        node_attr_map = {}

        # Retrieve field type of forest_id
        field_type = forest_layer.fields().at(
            forest_layer.fields().indexFromName(forest_id_field)).typeName()
        if field_type == "String":
            variant = QVariant.String
        elif field_type == "Real":
            variant = QVariant.Double
        else:
            variant = QVariant.Int

        idx_hub, idx_dist = attr.create_fields(self.node_layer, 
                                              [("frt_id", variant),
                                               ("frt_dist", QVariant.Double)])

        feedback.pushInfo("Writing results")

        for current, feat in enumerate(self.node_layer.getFeatures()):
            req = QgsFeatureRequest().setFilterExpression(
                                        "vid = %d" %feat["vid"])\
                                       .setFlags(QgsFeatureRequest.NoGeometry)\
                                       .setSubsetOfAttributes(
                                        ["HubName", "HubDist"],
                                        dist_layer.fields())

            node = next(dist_layer.getFeatures(req))

            distance = g.update_distance_to_nearest_hub(node, forest_layer, forest_id_field)

            # Fill node_attr_map
            node_attr_map[feat.id()] = {idx_hub: node["HubName"],
                                        idx_dist: round(distance, 2)}

            # Set progress
            feedback.setProgress((step_per_alg * 2) + int((current / count)))
            # Check for cancellation
            if feedback.isCanceled():
                return {}

        # Update arc_layer
        self.node_layer.dataProvider().changeAttributeValues(node_attr_map)

    def topographic_position(self, context, feedback, mnt, search=100, skip=50, kernel=7):
        """
        Compute terrain morphology and determine hedges topographic position based on grass geomorphon
        Default values are determined for a 5m resolution dem

        Parameters
        ----------
        self.poly_layer : QgsVectorLayer : Polygon : Hedges polygons
        context : QgsContext : Context in which the tools are called
        feedback : QgsFeedback : Allow communication with the users (message, progress bar, ...)
        mnt : QgsRasterLayer : MNT/MNE. Should be bigger than poly_layer extent
            in case their is a skip value
        search : int : Radius in meters to search for terrain form.
            Higher value will result to larger terrain form
        skip : int : Radius in meters to skip when searching for terrain form.
            Allow to ignore small form
        kernel : int : Radius of the majority filter kernel (must be odd)

        Returns
        -------
        outpoly_layer : QgisObject : QgsVectorLayer : Polygon
            Polygons layer with topographic position field.
        """
        alg_number = 5
        step_per_alg = int(100 / alg_number)

        # Get extent of polygon layer
        extent = self.poly_layer.sourceExtent()

        # Clip MNE to poly_layer extent + skip
        if extent.area() < mnt.extent().area():
            alg_name = "gdal:cliprasterbyextent"
            params = {"INPUT": mnt,
                      "PROJWIN": extent.buffered(skip + mnt.rasterUnitsPerPixelX() * 2),
                      # Skip as it'll reduce the raster size in geomorphon output if skip != 0.
                      # rasterunit * 2 as geomorphon output is reduced by that even with skip = 0.
                      "OUTPUT": "TEMPORARY_OUTPUT"}
            output = processing.run(alg_name, params)["OUTPUT"]
            mnt = QgsRasterLayer(output)

        # Set progress
        feedback.setProgress(step_per_alg)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Applying majority filter")

        # Apply median filter
        alg_name = "grass7:r.neighbors"
        params = {"input": mnt,
                  "method": 1,
                  "size": kernel,
                  "output": "TEMPORARY_OUTPUT"}
        output = processing.run(alg_name, params)["output"]
        mnt = QgsRasterLayer(output)

        # Set progress
        feedback.setProgress(step_per_alg * 2)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Processing geomorphology, given raster resolution it could be slow")
    
        # Geomorphon
        alg_name = "grass7:r.geomorphon"
        params = {"elevation": mnt,
                  "search": search,
                  "skip": skip,
                  "-m": True, # meters instead of cells for unit
                  "forms": "TEMPORARY_OUTPUT"}
        output = processing.run(alg_name, params)["forms"]
        geomorphon = QgsRasterLayer(output)



        # Set progress
        feedback.setProgress(step_per_alg * 3)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Computing zonal statistics")

        # Extract to poly_layer
        alg_name = "native:zonalstatisticsfb"
        params = {"INPUT": self.poly_layer,
                  "INPUT_RASTER": geomorphon,
                  "RASTER_BAND": 1,
                  "COLUMN_PREFIX": "_",
                  "STATISTICS": "9",
                  "OUTPUT": "TEMPORARY_OUTPUT"}
        poly_layer_output = processing.run(alg_name, params)["OUTPUT"]

        # Set progress
        feedback.setProgress(step_per_alg * 4)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Creating topographic position field")

        # Prepare fields_list for deletion
        fields_list = [("topo_pos", QVariant.String)]

        # Delete topo_pos if it exists
        idx_topo = attr.create_fields(self.poly_layer, fields_list)[0]

        # Init attr map
        poly_attr_map = {}

        # Fill map with result
        for poly1, poly2 in zip(self.poly_layer.getFeatures(), poly_layer_output.getFeatures()):
            poly_attr_map[poly1.id()] = {idx_topo: rutils.map_morph(
                                                  int(poly2["_majority"]))}

        # Edit layer with map
        self.poly_layer.dataProvider().changeAttributeValues(poly_attr_map)

        # Set progress
        feedback.setProgress(step_per_alg * 5)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

    def embankment_ditch(self, context, feedback, dem, band,
                        search_distance, overlap_thresh):
        """
        Compute embankment and ditch in a dem and check if they are overlapping with an hedge.
        If this is the case return True in two new boolean fields in the polygon layer data provider.

        Parameters
        ----------
        context : QgsContext : Context in which the tools are called
        feedback : QgsFeedback : Allow communication with the users (message, progress bar, ...)
        dem: QgsRasterLayer : MNT/DEM. Elevation raster.
        band : int : gdal band number of the DEM.
        search_distance: int : search distance in meters
        overlap_thresh: int: Threshold to determine if an hedge have an embankment or a ditch

        Returns
        -------
        outpoly_layer : QgisObject ; QgsVectorlayer ; Polygon
            Polygons layer with a relative orientation field.
        """
        alg_number = 5
        step_per_alg = int(100 / alg_number)

        idx_embank, idx_ditch = attr.create_fields(self.poly_layer, 
                                                  [("embankment", QVariant.Bool),
                                                   ("ditch", QVariant.Bool)])

        feedback.pushInfo("Computing terrain forms")

        alg_name = "grass7:r.geomorphon"
        params = {"elevation": dem,
                  "search": search_distance,
                  "skip": 0,
                  "-m": True,  # meters instead of cells for unit
                  "forms": "TEMPORARY_OUTPUT"}
        output = processing.run(alg_name, params)["forms"]
        geomorphon = QgsRasterLayer(output)

        # Set progress
        feedback.setProgress(step_per_alg)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Retrieving embankment and ditch")

        alg_name = "gdal:rastercalculator"
        params = {"INPUT_A": geomorphon,
                  "BAND_A": 1,
                  "FORMULA": "where(logical_and(A !=3, A != 9), 0, A)",
                  "NO_DATA": 0,
                  "RTYPE": 0,
                  "OUTPUT": "TEMPORARY_OUTPUT"}
        output = processing.run(alg_name, params)["OUTPUT"]
        geo_binary = QgsRasterLayer(output)

        # Set progress
        feedback.setProgress(step_per_alg * 2)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Retrieving number of cells of embankment and ditch inside the hedge")

        alg_name = "native:zonalhistogram"
        params = {"INPUT_RASTER": geo_binary,
                  "RASTER_BAND": 1,
                  "INPUT_VECTOR": self.poly_layer,
                  "COLUMN_PREFIX": "",
                  "OUTPUT": "TEMPORARY_OUTPUT"}
        histo_layer = processing.run(alg_name, params)["OUTPUT"]

        # Set progress
        feedback.setProgress(step_per_alg * 3)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Computing overlap percentage")

        request = QgsFeatureRequest().setFlags(
            QgsFeatureRequest.NoGeometry).setSubsetOfAttributes(
            ["fid", "NODATA", "3", "9"], histo_layer.fields())

        feature_dict = {poly["fid"]:
                        [poly["3"], poly["9"],
                         poly["3"] + poly["9"] + poly["NODATA"]]
                        for poly in histo_layer.getFeatures(request)}

        # Set progress
        feedback.setProgress(step_per_alg * 4)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        poly_attr_map = {k: {idx_embank: (ht_3 / ht_sum) * 100 > overlap_thresh,
                             idx_ditch: (9 / ht_sum) * 100 > overlap_thresh} for
                         k, (ht_3, ht_9, ht_sum) in feature_dict.items()}

        feedback.pushInfo("Writing results")

        self.poly_layer.dataProvider().changeAttributeValues(poly_attr_map)

        # Set progress
        feedback.setProgress(step_per_alg * 5)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

    def forest_connection(self, context, feedback, forest_layer, forest_id,
                         distance):
        """
        Return if a hedge is connected to a forest or not.

        Parameters
        ----------
        self.arc_layer: QgisObejct : QgsVectorLayer : LineString
            Arc layer
        context : QgsContext : Context in which the tools are called
        feedback : QgsFeedback : Allow communication with the users (message, progress bar, ...)
        forest_layer :  QgisObejct : QgsVectorLayer : Polygon or Point
        forest_id : QgsField : Field name of the forest unique identifier
        distance : Integer : Distance to connect forest to network.

        Returns
        -------
        self.arc_layer: QgisObejct : QgsVectorLayer : LineString
            Updated arc layer with a boolean field.
                True if connected to a forest
                False if not connected to a forest
        """
        # Init progress bar steps
        alg_number = 5
        step_per_alg = int(100 / alg_number)

        feedback.pushInfo("Creating network")

        forest_connection_point = g.create_forest_connection(self.poly_layer,
                                                             self.arc_layer,
                                                             self.node_layer,
                                                             forest_layer,
                                                             forest_id,
                                                             distance)
        
        # Set progress
        feedback.setProgress(step_per_alg * 3)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Network analysis")

        # v.net.distance
        network_path = QgsProcessingUtils.generateTempFilename("network_temp.gpkg")

        alg_name = "grass7:v.net.distance"
        params = {"input": self.arc_layer,
                  "flayer": self.node_layer,
                  "tlayer": forest_connection_point,
                  "threshold": distance,
                  "output": network_path}
        network_path = processing.run(alg_name, params)["output"]
        network_layer = QgsVectorLayer(network_path, "network_temp")

        # Set progress
        feedback.setProgress(step_per_alg * 4)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Writing results")

        # Retrieve field type of forest_id
        field_type = forest_layer.fields().at(
            forest_layer.fields().indexFromName(forest_id)).typeName()
        if field_type == "String":
            variant = QVariant.String
        elif field_type == "Real":
            variant = QVariant.Double
        else:
            variant = QVariant.Int

        # Create output fields and get their index
        idx_forest_co = attr.create_fields(self.poly_layer, [("N_frt_co", QVariant.Bool)])[0]
        indexes = attr.create_fields(self.node_layer, [("N_frt_dist", QVariant.Double),
                                                       ("N_frt_id", variant)])
        idx_forest_dist = indexes[0]
        idx_forest_id = indexes[1]

        # Init results buffer
        poly_attr_map = {}
        node_attr_map = {}

        # Add poly that have node connected to a forest to results
        for feat in network_layer.getFeatures():
            _, iF = g.get_clementini(self.poly_layer,
                                      feat.geometry().buffer(0.1, 5))
            # Get forest_id
            req = QgsFeatureRequest().setFilterFid(feat["tcat"])
            curr_id = next(forest_connection_point.getFeatures(req))[forest_id]

            for f in iF:
                if f.id() not in poly_attr_map:
                    poly_attr_map[f.id()] = {idx_forest_co: True}
                if feat["cat"] not in node_attr_map:
                    node_attr_map[feat["cat"]] = {
                        idx_forest_dist: round(feat["dist"], 2),
                        idx_forest_id: curr_id}

        # Add poly that have a direct forest connexion to results
        # We can enhance speed by excluding a list of curr_id in expression
        for node in forest_connection_point.getFeatures():
            _, iF = g.get_clementini(self.poly_layer,
                                      node.geometry().buffer(0.1, 5))
            exp = "vid = %d" % node["vid"]
            req = QgsFeatureRequest().setFilterExpression(exp)
            fid_node = next(self.node_layer.getFeatures(req)).id()
            for f in iF:
                poly_attr_map[f.id()] = {idx_forest_co: True}
                node_attr_map[fid_node] = {idx_forest_dist: round(0, 2),
                                           idx_forest_id: node[forest_id]}

        # Add results in layers
        self.poly_layer.dataProvider().changeAttributeValues(poly_attr_map)
        self.node_layer.dataProvider().changeAttributeValues(node_attr_map)

        # Remove v.net.distance layer
        del (network_layer)
        if os.path.exists(network_path):
            os.remove(network_path)

        # Set progress
        feedback.setProgress(step_per_alg * 5)
        # Check for cancellation
        if feedback.isCanceled():
            return {}
