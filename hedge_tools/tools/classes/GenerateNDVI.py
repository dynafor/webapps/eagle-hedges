from qgis.core import (QgsRasterLayer,
                       QgsRectangle,
                       QgsGeometry,
                       QgsProcessingFeedback,
                       QgsProcessingContext)
from .GenerateRaster import GenerateRaster
from ..raster import wrapper as wr
from ..raster import utils as rutils
from .. import checkers as ch

from typing import Union


class GenerateNDVI(GenerateRaster):
    def __init__(self, red:tuple[Union[QgsRasterLayer, str], int], 
                 infrared:tuple[Union[QgsRasterLayer, str], int],
                 feedback:QgsProcessingFeedback=None,
                 context:QgsProcessingContext=None,
                 alg_name:str="Generate NDVI"):
        """
        Compute NDVI from a red and infrared raster/band.
        Both inputs can be from the same raster.

        Parameters
        ---
        red: tuple[Union[QgsRasterLayer, str], int]
            Red raster and band
        infrared: tuple[Union[QgsRasterLayer, str], int]
            Infrared raster and band
        feedback: QgsProcessingFeedback
            Advised if executed within QgsProcessingAlgorithm
        context: QgsProcessingContext
            Advised if executed within QgsProcessingAlgorithm
        alg_name: str
            Name of the algorithm. 
            Advised to use self.displayName() if executed within QGIS.
        """
        super().__init__(red, infrared, feedback, context, alg_name)

    def process_index(self, out_path:str="TEMPORARY_OUTPUT", 
                      overwrite:bool=True, extent_opt:int=3):
        """
        Compute NDVI from a red and infrared band
        Store the result in self.out_index.

        Parameters
        ---
        out_path: str
            Output path. Default is in tmp
        overwrite: bool
            Overwrite output if already exists. Default is True.
        extent_opt: int
            Extent option for gdal_calc.

            -0: Ignore
                Will ignore CRS and only check both inputs dimensions.
                Raise an error if both inputs have different dimensions.
            -1: Fail
                Will check if both inputs are in the same CRS 
                and have the same dimensions (Equality predicate). 
                Raise an error otherwise.
            -2: Union
                Execute algorithm on the union of inputs.
            -3: Intersect
                Execute algorithm on the intersection of inputs.
                Raise an error if there is no intersection.
        """
        formula = "(B.astype(float32) - A.astype(float32))/\
                   (B.astype(float32) + A.astype(float32))"
        
        self.out_index = self._load(
            wr.gdal_calc([self.input_A, self.input_B], 
                         formula, output=out_path, 
                         overwrite=overwrite,
                         extent_opt=extent_opt))
    
    def process_algorithm(self, extent:Union[QgsRasterLayer,
                                             str, QgsRectangle, 
                                             QgsGeometry]=None,
                          extent_opt:Union[int, None]=None,
                          target_res:float=0.0, 
                          resampling:Union[int, None]=None, 
                          compute_mask:bool=True,
                          threshold_mask:float=0.2,
                          compute_sieve:bool=True,
                          threshold_sieve:float=100,
                          connectedness:bool=True,
                          overwrite:bool=True,
                          ndvi_path="TEMPORARY_OUTPUT", 
                          mask_path="TEMPORARY_OUTPUT",
                          sieve_path="TEMPORARY_OUTPUT"):
        """
        Method that store algorithm execution 
        to compute NDVI, NDVI mask and sieved mask.

        Parameters
        ---
        extent: Union[QgsRasterLayer, str, QgsRectangle, QgsGeometry]
            Extent to clip the raster.
            If None full raster extent will be used.
        extent_opt: Union[int, None]
            Extent option for gdal_calc.

            -0: Ignore
                Will ignore CRS and only check both inputs dimensions.
                Raise an error if both inputs have different dimensions.
            -1: Fail
                Will check if both inputs are in the same CRS 
                and have the same dimensions (Equality predicate). 
                Raise an error otherwise.
            -2: Union
                Execute algorithm on the union of inputs.
            -3: Intersect
                Execute algorithm on the intersection of inputs.
                Raise an error if there is no intersection.
        target_res: float
            Target resolution for resampling the NDVI raster.
        resampling: Union[int, None]
            Resampling method

            -0: Nearest Neighbour
            -1: Bilinear (2x2 Kernel)
            -2: Cubic (4x4 Kernel)
            -3: Cubic B-Spline (4x4 Kernel)
            -4: Lanczos (6x6 Kernel)
            -5: Average
            -6: Mode
            -7: Maximum
            -8: Minimum
            -9: Median
            -10: First Quartile (Q1)
            -11: Third Quartile (Q3)
        compute_mask: bool
            Compute NDVI mask. Default is True
        threshold_mask: bool
            Threshold to compute NDVI mask. Default is 0.2
        compute_sieve: bool
            Compute sieved mask. Default is True
        threshold_sieve: float
            Threshold to compute sieved mask. efault is 100.
            Number of connected pixels.
            Object of size below this value will be erased.
        connectedness: bool
            If True, 8D will be used for the object construction
            Otherwise 4D will be used.
            Default is True.
        overwrite: bool
            Overwrite output if already exists. Default is True.
        ndvi_path: str
            Output path for NDVI raster. Default is in tmp
        mask_path: str
            Output path for NDVI mask. Default is in tmp
        sieve_path: str
            Output path for sieved mask. Default is in tmp
        """
        self._input_validation(extent_opt, compute_mask, compute_sieve)
        
        if isinstance(extent, QgsRasterLayer):
            extent = extent.extent()
        elif isinstance(extent, str):
            extent = self._load(extent).extent()
            
        nb_alg = self._define_nb_alg(target_res, compute_mask, 
                                     compute_sieve, extent)
        self._init_progress_bar(nb_alg)

        if extent is not None and extent.isEmpty() == False:
            self.clip_by_extent(extent)
            self._progress_bar()

        if target_res != 0.0 and \
                self.input_A[0].rasterUnitsPerPixelX() != target_res:
            self._push_info("Computing NDVI...")
            self.process_index(extent_opt=extent_opt)
            self._progress_bar("Resampling NDVI...")
            self.resample(target_res, resampling, ndvi_path)
            self._progress_bar()
        else:
            self._push_info("Computing NDVI...")
            self.process_index(ndvi_path, overwrite, extent_opt)
            self._progress_bar()

        if compute_mask:
            self._push_info("Thresholding NDVI...")
            self.process_mask(threshold_mask, mask_path, overwrite)
            self._progress_bar()
        
        if compute_sieve:
            self._push_info("Sieving NDVI...")
            self.process_sieve(threshold_sieve, connectedness, sieve_path)
            self._progress_bar()