"""
Wrappers functions that use QGIS algorithms.

TODO: Add is_child_algorithm to handle output inside a QgsProcessingAlgorithm
"""
import processing
from qgis.core import (QgsGeometry,
                       QgsFeature,
                       QgsVectorLayer,
                       QgsProcessingFeatureSource,
                       QgsRectangle,
                       QgsProcessingFeedback,
                       QgsProcessingContext)

from hedge_tools.tools.vector import utils

from typing import Union # until python 3.10 we need to use that instead of | 

def create_spatial_index(layer:QgsVectorLayer)->QgsVectorLayer:
    """
    Create a spatial index on a layer if not already present

    Parameters
    ----------
    layer:
    """
    if layer.hasSpatialIndex() != 2:
        alg_name = "native:createspatialindex"
        params = {"INPUT": layer}
        processing.run(alg_name, params)

def buffer(layer:QgsVectorLayer, distance:float, dissolve:bool=False, 
           end_style:int=0, join_style:int=0, miter_limit:float=2, 
           segments:int=100, output:str="TEMPORARY_OUTPUT")->QgsVectorLayer:
    """
    Perform a buffer with the given distance on a layer.

    Parameters
    ----------
    layer:
    distance:
    dissolve:
    end_style:
        Available values:
		- 0: Circle
		- 1: Flat
		- 2: Squared
    join_style:
         Available values:
		- 0: Circle
		- 1: Squared angle
		- 2: Diagonal
    miter_limit:
    segments:
    output: 
        Default : "TEMPORARY_OUTPUT"
        Path of the output

    Returns
    -------
    result:

    TODO: As of QGIS 3.32 add the SEPARATE_DISJOINT parameters 
    to avoid using multipart to singlepart
    """    
    alg_name = "native:buffer"
    params = {"INPUT": layer,
              "DISTANCE": distance,
              "SEGMENTS": segments,
              "END_CAP_STYLE": end_style,  # Round
              "JOIN_STYLE": join_style,  # Round
              "MITER_LIMIT": miter_limit,
              "DISSOLVE": dissolve,
              "OUTPUT": output}
    result = processing.run(alg_name, params)["OUTPUT"]

    return result

def single_sided_buffer(layer:QgsVectorLayer, distance:float, side:int=0, 
                        join_style:int=0, miter_limit:float=2, 
                        segments:int=100, output:str="TEMPORARY_OUTPUT"
                        )->QgsVectorLayer:
    """
    Perform a single sided buffer on a linestring layer

    Parameters
    ---
    layer: QgsVectorLayer
        Only linear features
    distance: float
        Width of the buffer
    side: int
        Default is 0.
        - 0 : left
        - 1 : right
    join_style: 
     Available values:
		- 0: Circle
		- 1: Squared
		- 2: Diagonal
    miter_limit: float
    segments: int
    output: 
        Default : "TEMPORARY_OUTPUT"
        Path of the output

    Return
    ---
    result: QgsVectorLayer
    """
    alg_name = "native:singlesidedbuffer"
    params = {"INPUT": layer,
              "DISTANCE":distance,
              "SIDE": side,
              "SEGMENTS": segments,
              "JOIN_STYLE": join_style,
              "MITER_LIMIT": miter_limit,
              "OUTPUT": output}
    result = processing.run(alg_name, params)["OUTPUT"]

    return result

def closing(layer:QgsVectorLayer, distance:float,
            dissolve:bool=False, end_style:int=0, 
            join_style:int=0, miter_limit:float=2, 
            segments:int=100, output:str="TEMPORARY_OUTPUT"
            )->QgsVectorLayer:
    """
    Perform a closing operation.

    From a layer and a distance value.

    Parameters
    ----------
    layer:
    distance:
    dissolve:
    end_style:
        Available values:
		- 0: Circle
		- 1: Flat
		- 2: Squared
    join_style:
         Available values:
		- 0: Circle
		- 1: Squared angle
		- 2: Diagonal
    miter_limit:
    segments:
    output: 
        Default : "TEMPORARY_OUTPUT"
        Path of the output

    Returns
    -------
    result: 

    TODO: As of QGIS 3.32 add the SEPARATE_DISJOINT parameters 
    to avoid using multipart to singlepart
    """ 
    # Erosion
    layer = buffer(layer, distance, dissolve, end_style, 
                   join_style, miter_limit, segments)
    
    # Dilation
    result = buffer(layer, -distance, dissolve, end_style, 
                    join_style, miter_limit, segments, output)

    return result


def opening(layer:QgsVectorLayer, distance:float, 
            dissolve:bool=False, end_style:int=0, 
            join_style:int=0, miter_limit:float=2, 
            segments:int=100, output:str="TEMPORARY_OUTPUT"
            )->QgsVectorLayer:
    """
    Perform an opening.

    From a layer and a distance value.

    Parameters
    ----------
    layer:
    distance:
    dissolve:
    end_style:
        Available values:
		- 0: Circle
		- 1: Flat
		- 2: Squared
    join_style:
         Available values:
		- 0: Circle
		- 1: Squared angle
		- 2: Diagonal
    miter_limit:
    segments:
    output: 
        Default : "TEMPORARY_OUTPUT"
        Path of the output

    Returns
    -------
    result:

    TODO: As of QGIS 3.32 add the SEPARATE_DISJOINT parameters 
    to avoid using multipart to singlepart
    """ 
    # Erosion
    layer = buffer(layer, -distance, dissolve, end_style, 
                   join_style, miter_limit, segments)
    
    # Dilation
    result = buffer(layer, distance, dissolve, end_style, 
                    join_style, miter_limit, segments, output)

    return result


def dissolve(layer:QgsVectorLayer, dissolve_field:Union[str,None]=None, 
             separate_disjoint:bool=False, output:str="TEMPORARY_OUTPUT"
             )->QgsVectorLayer:
    """
    Dissolve a layer.
    
    Parameters
    ----------
    layer:
    dissolve_field:
    separate_disjoint:
    output: 
        Default : "TEMPORARY_OUTPUT"
        Path of the output

    Returns
    -------
    result:
    """
    dissolve_field = [dissolve_field]
    alg_name = "native:dissolve"
    params = {"INPUT": layer,
              "FIELD": dissolve_field,
              "SEPARATE_DISJOINT": separate_disjoint,
              "OUTPUT": output}
    result = processing.run(alg_name, params)["OUTPUT"]

    return result


def multipart_to_singleparts(layer:Union[QgsVectorLayer, QgsProcessingFeatureSource],
                             output:str="TEMPORARY_OUTPUT",  
                             feedback:Union[QgsProcessingFeedback, None]=None, 
                             context:Union[QgsProcessingContext, None]=None, 
                             is_child_algorithm:bool=False)->QgsVectorLayer:
    """
    Convert multiparts layer to single parts layer.

    Parameters
    ----------
    layer:
    output: 
        Default : "TEMPORARY_OUTPUT"
        Path of the output
    feedback:
        Communication with user
    context:
        Context of execution and how to handle layers access
    is_child_algorithm:
        If the algorithm is executed within another algorithm

    Returns
    -------
    result:
    """
    # Multiple features to single features
    alg_name = "native:multiparttosingleparts"
    params = {"INPUT": layer,
              "OUTPUT": output}
    
    if context is not None and feedback is not None:
        result = processing.run(alg_name, params, feedback=feedback, context=context, 
                                is_child_algorithm=is_child_algorithm)["OUTPUT"]#, progress=None)["OUTPUT"]
    elif feedback is not None:
        result = processing.run(alg_name, params, feedback=feedback, 
                                is_child_algorithm=is_child_algorithm)["OUTPUT"]#, progress=None)["OUTPUT"]
    elif context is not None:
        result = processing.run(alg_name, params, context=context,
                                is_child_algorithm=is_child_algorithm)["OUTPUT"]
    else:
        result = processing.run(alg_name, params, 
                                is_child_algorithm=is_child_algorithm)["OUTPUT"]

    return result


def snap(layer_1:QgsVectorLayer, layer_2:QgsVectorLayer, 
         tolerance:float, behavior:int=0, 
         output:str="TEMPORARY_OUTPUT")->QgsVectorLayer:
    """
    Perform a snap between two layers.

    Parameters
    ----------
    layer_1:
        Any geometry.
    layer_2:
        Any geometry.
    tolerance:
        Tolerance for snapping.
    behavior:
        - 0: Prefer aligning nodes, insert extra vertices where required
                Prefer to snap to nodes, even when a segment may be closer than a node. 
                New nodes will be inserted to make geometries follow each other 
                exactly when inside allowable tolerance.

        - 1: Prefer closest point, insert extra vertices where required
                Snap to closest point, regardless of it is a node or a segment. 
                New nodes will be inserted to make geometries follow each other 
                exactly when inside allowable tolerance.

        - 2: Prefer aligning nodes, don’t insert new vertices
                Prefer to snap to nodes, even when a segment may be closer than a node. 
                No new nodes will be inserted.

        - 3: Prefer closest point, don’t insert new vertices
                Snap to closest point, regardless of it is a node or a segment. 
                No new nodes will be inserted.

        - 4: Move end points only, prefer aligning nodes
                Only snap start/end points of lines (point features will also be snapped,
                polygon features will not be modified), prefer to snap to nodes.

        - 5: Move end points only, prefer closest point
                Only snap start/end points of lines (point features will also be snapped, 
                polygon features will not be modified), snap to closest point

        - 6: Snap end points to end points only
                Only snap the start/end points of lines
                to other start/end points of lines
        
        - 7: Snap to anchor nodes (single layer only)
    output: 
        Default : "TEMPORARY_OUTPUT"
        Path of the output

    See native:snapgeometries description
    Can be accessed with processing.algorithmHelp("native:snapgeometries")
    in qgis python console. (Default value = 0)
    """
    alg_name = "native:snapgeometries"
    params = {"INPUT": layer_1,
              "REFERENCE_LAYER": layer_2,
              "TOLERANCE": tolerance,
              "BEHAVIOR": behavior,
              "OUTPUT": output}
    snapped = processing.run(alg_name, params)["OUTPUT"]

    return snapped


def extract_by_location(layer:QgsVectorLayer, overlay:QgsVectorLayer, 
                        predicate:Union[int,list[int],str], 
                        output:str="TEMPORARY_OUTPUT")->QgsVectorLayer:
    """
    From an input_layer extract the features
    that respect the predicate form the intersect layer.

    Parameters
    ----------
    layer:
    overlay:
    predicate:
        - 0: intersects
		- 1: contains
		- 2: disjoints
		- 3: equals
		- 4: touches
		- 5: overlaps
		- 6: withins
		- 7: crosses
    See native:extractbylocation description.
    Can be accessed with processing.algorithmHelp("native:extractbylocation")
    in qgis python console.
    output: 
        Default : "TEMPORARY_OUTPUT"
        Path of the output
        
    Returns
    -------
    result:
    """
    # Create spatial index
    create_spatial_index(layer)
    create_spatial_index(overlay)
    
    alg_name = "native:extractbylocation"
    params = {"INPUT": layer,
              "PREDICATE": predicate,
              "INTERSECT": overlay,
              "OUTPUT": output}
    result = processing.run(alg_name, params)["OUTPUT"]

    return result


def clip(layer:QgsVectorLayer, overlay:QgsVectorLayer, 
         output:str="TEMPORARY_OUTPUT")->QgsVectorLayer:
    """
    Clip an input layer with an overlay layer.

    Parameters
    ----------
    layer:
    overlay:
    output: 
        Default : "TEMPORARY_OUTPUT"
        Path of the output

    Returns
    -------
    result:
    """
    # Spatial index
    create_spatial_index(layer)
    create_spatial_index(overlay)

    # Difference
    alg_name = "native:clip"
    params = {"INPUT": layer,
              "OVERLAY": overlay,
              "OUTPUT": output}
    result = processing.run(alg_name, params)["OUTPUT"]

    return result


def difference(layer:QgsVectorLayer, overlay:QgsVectorLayer, 
               output:str="TEMPORARY_OUTPUT")->QgsVectorLayer:
    """
    Perform the difference between an input layer and an overlay layer.

    Parameters
    ----------
    layer:
    overlay:
    output: 
        Default : "TEMPORARY_OUTPUT"
        Path of the output

    Returns
    -------
    result:
    """
    # Spatial index
    create_spatial_index(layer)
    create_spatial_index(overlay)
    
    # Difference
    alg_name = "native:difference"
    params = {"INPUT": layer,
              "OVERLAY": overlay,
              "OUTPUT": output,
              'GRID_SIZE':None}
    result = processing.run(alg_name, params)["OUTPUT"]

    return result

def extract_by_expression(layer:QgsVectorLayer, expression:str,
                          output:str="TEMPORARY_OUTPUT")->QgsVectorLayer:
    """
    Extract features from a layer by an expression.

    Expression should be in qgis syntax.

    Parameters
    ----------
    layer:
    expression:
    output: 
        Default : "TEMPORARY_OUTPUT"
        Path of the output

    Returns
    -------
    result:
    """
    alg_name = "native:extractbyexpression"
    params = {"INPUT": layer,
              "EXPRESSION": expression,
              "OUTPUT": output}
    result = processing.run(alg_name, params)["OUTPUT"]

    return result

def extract_by_extent(extent:Union[QgsVectorLayer,QgsRectangle,tuple[str]], 
                      layer:QgsVectorLayer, clip_by:bool=False, 
                      output:str="TEMPORARY_OUTPUT")->QgsVectorLayer:
    """
    Extract features from a layer by the extent of the padded grid. 
    Geometries are not clipped by the givne extent.

    Parameters
    ---
    extent : QgsVectorLayer or a QgsRectangle
    or tuple of string of coordinate x_min, x_max, y_min, y_max
    layer:
    clip_by:
    output: 
        Default : "TEMPORARY_OUTPUT"
        Path of the output

    Return
    ---
    clipped
    """
    create_spatial_index(layer)

    # If extractbyextent with clip True is creating wrong geometry 
    # then we do not clip. It create a redundancy in the data 
    # but this case should be rare enough to be an acceptable bias
    # To improve it we could create an itnersectin function that check the wkbtypes 
    # of the results and discard non matching goemetry type with input geometry type
    try:
        alg_name = "native:extractbyextent"
        params = {"INPUT": layer,
                "EXTENT": extent,
                "CLIP": clip_by,
                "OUTPUT": output}
        clipped = processing.run(alg_name, params)["OUTPUT"]
    except:
        clip_by = False
        try:
            overlay = utils.create_layer(layer, geom_type="Polygon")
            geometry = QgsGeometry().fromRect(extent)
            feature = QgsFeature()
            feature.setGeometry(geometry)
            overlay.dataProvider().addFeature(feature)
            overlay.updateExtents()
            clipped = clip(layer, overlay)
        except:
            alg_name = "native:extractbyextent"
            params = {"INPUT": layer,
                    "EXTENT": extent,
                    "CLIP": False,
                    "OUTPUT": output}
            clipped = processing.run(alg_name, params)["OUTPUT"]

    if clip_by is True:
        clipped = multipart_to_singleparts(clipped)
        
    return clipped
    
def merge_layers(layers:list[QgsVectorLayer], 
                 output:str="TEMPORARY_OUTPUT")->QgsVectorLayer:
    """
    Merge vector layers together.

    Input layers should be of same geometry.

    Parameters
    ----------
    layers:
    output: 
        Default : "TEMPORARY_OUTPUT"
        Path of the output

    Returns
    -------
    result:
    """

    alg_name = "native:mergevectorlayers"
    params = {"LAYERS": layers,
              "OUTPUT": output}
    result = processing.run(alg_name, params)["OUTPUT"]

    return result


def field_calculator(layer:QgsVectorLayer, field_name:str, field_type:int, 
                     formula:str, field_length:int=0, field_precision:int=0,
                     output:str="TEMPORARY_OUTPUT")->QgsVectorLayer:
    """
    Create a field in a QgsVectorLayer

    See qgis:fieldcalculator description for further information.
    Can be accessed with processing.algorithmHelp("qgis:fieldcalculator")
    in qgis python console.

    Parameters
    ----------
    layer:
    field_name:
    field_type:
        - 0: Décimal (double)
		- 1: Entier (32bit)
		- 2: Texte (chaîne de caractères)
		- 3: Date
		- 4: Temps
		- 5: Date et heure
		- 6: Booléen
		- 7: Objet binaire (BLOB)
		- 8: Liste de chaîne de caractères
		- 9: Liste d'entier
		- 10: Liste de décimal (double)
    formula:
    field_length:
    field_precision:
    output: 
        Default : "TEMPORARY_OUTPUT"
        Path of the output

    Returns
    -------
    result
    """
    alg_name = "qgis:fieldcalculator"
    params = {"INPUT": layer,
              "FIELD_NAME": field_name,
              "FIELD_TYPE": field_type,
              "FIELD_LENGTH": field_length,
              "FIELD_PRECISION": field_precision,
              "FORMULA": formula,
              "OUTPUT": output}
    result = processing.run(alg_name, params)["OUTPUT"]

    return result


def remove_holes(layer:QgsVectorLayer, min_area:float=0, 
                 output:str="TEMPORARY_OUTPUT")->QgsVectorLayer:
    """
    Remove holes from a layer given an area threshold.

    Parameters
    ----------
    layer:
    min_area:
        Area below wich hole will be filled.
        With 0 remove all holes of any size. (Default value = 0)
    output: 
        Default : "TEMPORARY_OUTPUT"
        Path of the output

    Returns
    -------
    result:
    """
    alg_name = "native:deleteholes"
    params = {"INPUT": layer,
              "MIN_AREA": min_area,
              "OUTPUT": output}
    
    result = processing.run(alg_name, params)["OUTPUT"]

    return result


def create_ombb(layer:QgsVectorLayer, 
                output:str="TEMPORARY_OUTPUT")->QgsVectorLayer:
    """
    Create the oriented minimum bounding box for features in a layer.

    Parameters
    ----------
    layer:
    output: 
        Default : "TEMPORARY_OUTPUT"
        Path of the output

    Returns
    -------
    result:
    """
    alg_name = "native:orientedminimumboundingbox"
    params = {"INPUT": layer,
              "OUTPUT": output}
    result = processing.run(alg_name, params)["OUTPUT"]
    
    return result


def create_convex_hull(layer:QgsVectorLayer, 
                       output:str="TEMPORARY_OUTPUT")->QgsVectorLayer:
    """
    Create the convex hull for features in a layer.

    Parameters
    ----------
    layer:
    output: 
        Default : "TEMPORARY_OUTPUT"
        Path of the output

    Returns
    -------
    result:
    """
    alg_name = "native:convexhull"
    params = {"INPUT": layer,
              "OUTPUT": output}
    result = processing.run(alg_name, params)["OUTPUT"]

    # In some case area field is not populated, do it manually.
    feature = result.getFeature(1)
    if feature["area"] is None:
        idx_area = result.fields().indexFromName("area")
        attr_map = {f.id(): {idx_area: f.geometry().area()} for f in
                    result.getFeatures()}
        result.dataProvider().changeAttributeValues(attr_map)

    return result

def remove_null_geometries(layer:QgsVectorLayer, remove_empty:bool=True, 
                           output:str="TEMPORARY_OUTPUT")->QgsVectorLayer:
    """
    Delete null and empty geometries

    Parameters
    ----------
    layer:
    remove_empty:
    output: 
        Default : "TEMPORARY_OUTPUT"
        Path of the output

    Returns
    -------
    result:
    """
    alg_name = "native:removenullgeometries"
    params = {"INPUT": layer,
              "REMOVE_EMPTY": remove_empty,
              "OUTPUT": output}
    result = processing.run(alg_name, params)["OUTPUT"]

    return result

def fix_geometries(layer:QgsVectorLayer, 
                   output:str="TEMPORARY_OUTPUT")->QgsVectorLayer:
    """
    Fix geometries to respect GEOS.

    Parameters
    ----------
    layer:
    output: 
        Default : "TEMPORARY_OUTPUT"
        Path of the output

    Returns
    -------
    result:
    """
    alg_name = "native:fixgeometries"
    params = {"INPUT": layer,
              "OUTPUT": output}
    result = processing.run(alg_name, params)["OUTPUT"]

    return result


def delete_column(layer:QgsVectorLayer, fields_list:list[str], 
                  output:str="TEMPORARY_OUTPUT"):
    """
    Delete specified columns in a layer.

    Parameters
    ----------
    layer:
        Any geometry.
    fields_list:
        Name of the fields.
    output: 
        Default : "TEMPORARY_OUTPUT"
        Path of the output

    Returns
    -------
    result : QgsVectorLayer :
        Any geometry.
    """

    if bool(fields_list):
        alg_name = "native:deletecolumn"
        params = {"INPUT": layer,
                "COLUMN": fields_list,
                "OUTPUT": output}
        result = processing.run(alg_name, params)["OUTPUT"]
    else:
        result = layer
        
    return result


def join_attributes_by_location(layer:QgsVectorLayer, join_layer:QgsVectorLayer, 
                                predicate:list[int]=[0], join_fields:list[str]=[], 
                                method:int=0, discard_nonmatching:bool=False,
                                prefix:str="", output:str="TEMPORARY_OUTPUT"
                                )->QgsVectorLayer:
    """
    Perform a spatial join between layer and join_layer

    Parameters
    ----------
    layer:
    join_layer:
        Layer that'll be join
    predicate:
        Spatial predicate (clementini) 
        - 0: intersect
		- 1: contain
		- 2: equal
		- 3: touch
		- 4: overlap
		- 5: within
		- 6: cross
    join_fields:
        Empty list for all fields
    method:
        - 0 : 1 to 1
        - 1 : 1 to many
    discard_nonmatching:
    prefix:
    output: 
        Default : "TEMPORARY_OUTPUT"
        Path of the output

    Return
    ------
    result : QgsVectorLayer : Any
    """
    
    alg_name = "native:joinattributesbylocation"
    params = {"INPUT": layer,
              "PREDICATE": predicate,
              "JOIN": join_layer,
              "JOIN_FIELDS": join_fields,
              "METHOD": method,
              "DISCARD_NONMATCHING": discard_nonmatching,
              "PREFIX": prefix,
              "OUTPUT": output}
    result = processing.run(alg_name, params)["OUTPUT"]

    return result


def densify_by_interval(layer:QgsVectorLayer, interval:float=3.0,
                        output:str="TEMPORARY_OUTPUT")->QgsVectorLayer:
    """
    Densify geometries in a layer given an interval. 
    If two vertex are further apart than interval value it'll add a vertex.

    Parameters
    ----------
    layer:
    interval:
    output: 
        Default : "TEMPORARY_OUTPUT"
        Path of the output

    Returns
    -------
    result:
        Densified layer
    """
    alg_name = "native:densifygeometriesgivenaninterval"
    params = {"INPUT": layer,
              "INTERVAL": interval,
              "OUTPUT": output}
    result = processing.run(alg_name, params)["OUTPUT"]

    return result

def transect(layer:QgsVectorLayer, length:float, angle:float, 
             side:int, output:str="TEMPORARY_OUTPUT")->QgsVectorLayer:
    """
    Create a transect at every vertex of a linestring

    Parameters
    ----------
    layer : LineString
    length: 
        Length of the transect
    angle:
        Angle of the transect with the line
    side:
        - 0: left
        - 1: right
        - 2: both side
    output: 
        Default : "TEMPORARY_OUTPUT"
        Path of the output

    Returns
    -------
    result:
        Transect layer
    """
    alg_name = "native:transect"
    params = {"INPUT": layer,
              "LENGTH": length,
              "ANGLE": angle,
              "SIDE": side,  # Both side
              "OUTPUT": output}
    result = processing.run(alg_name, params)["OUTPUT"]

    return result

def simplification(layer:QgsVectorLayer, method:int=0, tolerance:float=0.25,
                   output:str="TEMPORARY_OUTPUT")->QgsVectorLayer:
    """
    Perform vector simplification using native:simplifygeometries processing algorithm

    Parameters
    ----------
    layer: 
        Vector layer to be simplified
    method: 
        - 0: Distance (Douglas-Peucker)
		- 1: Accrochage à la grille
		- 2: Aire (Visvalingam)
    tolerance: 
        Shift tolerance in meters
    output: 
        Default : "TEMPORARY_OUTPUT"
        Path of the output

    Returns
    -------
    result : 
        Simplified vector layer
    """
    alg_name = "native:simplifygeometries"
    params = {"INPUT": layer,
              "METHOD": method,
              "TOLERANCE": tolerance,
              "OUTPUT": output}
    result = processing.run(alg_name, params)["OUTPUT"]

    return result

def tessellate(layer:QgsVectorLayer, output:str="TEMPORARY_OUTPUT")->QgsVectorLayer:
    """
    Tesselate polygons in a vector layer.
    Tesselated geometries will be aggregated in a MultiPolygon.

    Parameters
    ---
    layer: QgsVectorLayer
        Vector layer to tesselate
    output: 
        Default : "TEMPORARY_OUTPUT"
        Path of the output

    Returns
    ---
    result:
        Teeselated vector layer
    """
    alg_name = "3d:tessellate"
    params = {"INPUT": layer,
              "OUTPUT": output}
    result = processing.run(alg_name, params)["OUTPUT"]

    return result