"""
#TODO add docstring.
"""
from copy import deepcopy

import processing
from qgis.PyQt.QtCore import (QVariant)
from qgis.core import (QgsVectorLayer,
                       QgsFields,
                       QgsField,
                       QgsVectorLayerJoinInfo,
                       QgsFeature,
                       QgsGeometry)
from hedge_tools.tools.vector import qgis_wrapper as qw
from hedge_tools.tools.vector import geometry as g

from typing import Union # until python 3.10 we need to use that instead of | 

def create_fields(layer:QgsVectorLayer, 
                  fields_list:list[Union[tuple[str, QVariant],tuple[str, QVariant, int, int]]]
                  )->list[int]:
    """
    Create field after checking if it already exists.
    If this is the case fields value will be overwritten.
    In option a len and a precision cna be added for each tuple 
    at index 2 and 3 respectively
    If len and prec are not used, the field created will be 
    of max length and precision
    Parameters
    ----------
    layer : QgisObject : QgsVectorLayer :
        Any geometry.
    fields_list : list :
        List of fields with [("NAME", QVariant, ...)].
    
    Returns
    -------
    indexes : ite[str]
        Index of the field in the same order

    """
    fields = []
    names = []
    for attr in fields_list:
        if attr[0] not in [f.name() for f in layer.fields()]: 
            if len(attr) == 4:
                field = QgsField(attr[0], attr[1], len=attr[2], prec=attr[3])
            elif len(attr) == 2:
                field = QgsField(attr[0], attr[1])
            fields.append(field)
        names.append(attr[0])

    if len(fields) != 0:
        layer.dataProvider().addAttributes(fields)
        layer.updateFields()
    indexes = [layer.fields().indexFromName(name) for name in names]

    return indexes

def delete_fields(layer, fields_list):
    """
    Delete fields in a layer.

    It allows to prevent error when QgsProcessing tools create them.

    Parameters
    ----------
    layer : QgisObject : QgsVectorLayer :
        Any geometry
    fields_list : list :
        List of fields with ["NAME"].

    Returns
    -------
    Updated layer.

    """
    fields = [layer.fields().indexFromName(i) for i in fields_list if
              i in [field.name() for field in layer.fields()]]
    if len(fields) != 0:
        layer.dataProvider().deleteAttributes(fields)
        layer.updateFields()

def add_fields(feature, fields_name, fields_type):
    """
    Add a field map to a feature.

    Parameters
    ----------
    feature : QgsFeature
        Input feature.
    fields_name : ite[str]
    fields_type : ite[QVariant]

    Returns
    -------
    feature :
        Modified feature.
    """
    fields = QgsFields()
    for name, variant in zip(fields_name, fields_type):
        fields.append(QgsField(name, variant))
    feature.setFields(fields)

    return feature

def set_fields_value(feature, fields_name, values):
    """
    Set a given value to a field in a feature.

    Parameters
    ----------
    feature : QgsFeature
    fields_name : ite[str]
    values : ite[any] :
        Value must be of the correct type.

    Returns
    -------
    feature :
        Modified feature.
    """
    for name, value in zip(fields_name, values):
        feature[name] = value

    return feature

def add_field_at_given_idx(layer, field_name, field_type, field_idx, values):
    """
    Add a field to a given index with the given value.

    It is suggested to do a copy of the layer first to avoid data deletion
    in case of errors. All the fields at the right of the given index will
    be moved.

    Parameters
    ----------
    layer : QgsVectorLayer:
        Any Geometry
    field_name : str :
        name of the new field.
    field_type : QVariant
    field_idx :
        Position of the new field.
    values : ite[Matching QVariant] :
        Values in order of features to be added.

    Returns
    -------
    layer : QgsVectorLayer: Any Geometry:
        Shallow copy of input layer.
    """

    # Create new field map. It is assumed the layer have a fid field.
    fields = layer.fields()
    idxs = [fields.indexOf(name) for name in fields.names() if name != "fid"]

    new_field = QgsField(field_name, field_type)
    fields_list = fields.toList()
    fields_list.insert(field_idx, new_field)    
    new_fields = QgsFields()
    for field in fields_list:
        new_fields.append(field)

    # Create new attributes map
    feats = [f for f in layer.getFeatures()]
    max_attr = len(fields.names())
    attr_map = {f.id(): {i: attribute for (i, attribute)
                         in enumerate(f.attributes())} for f in feats}
    attr_map_copy = deepcopy(attr_map)
    for i, (k, v) in enumerate(attr_map_copy.items()):
        idx = field_idx
        # While loop goal is to move all the fields to the right from given idx.
        while idx < max_attr:
            v[idx+1] = attr_map[k][idx]
            idx += 1
        # Once all the fields are moved, "insert" new field to given idx.
        v[field_idx] = values[i]

    # Delete current fields, add new ones and update values
    layer.dataProvider().deleteAttributes(idxs)
    layer.dataProvider().addAttributes(new_fields)
    layer.updateFields()
    layer.dataProvider().changeAttributeValues(attr_map_copy)

    return layer

def update_field_from_DHM(ogr_feat, qgs_feat, qgs_geom, qgs_layer, 
                          multipoly_d, unique, overlap, bins, id):
    """
    This function will only work for the strata proportion from DHM tools
    It'll update QgsFields from stored DHM values

    Parameters
    ----------
    ogr_feat : OGRShadowFeature
        Origin feature
    qgs_feat : QgsFeature
        Feature to add
    qgs_geom : QgsGeometry
        Geometry to add
    qgs_layer : QgsVectorLayer
        Layer to populate
    multipoly_d : dict
        Store geom parts from multigeometry if parts already added in qgs_layer
        Multipolygon are created afterwards
    unique : nnumpy.array
        Unique strata
    overlap : numpy.array
        Overlap % of unique strata
    bins : list
        Bounds of unique strata
    id : Int
        Unique Id of the original polygon

    Return
    ------
    qgs_layer : QgsVectorLayer  -> Not necessarry to return it i think
    multipoly_d : dict
        Updated multipolygon dictionnary
    """
    # Write if first occurence or store if already
    # a poly with same id_strata in q_layer
    if ogr_feat["id"] in multipoly_d:
        multipoly_d[ogr_feat["id"]].append(qgs_geom)
    else:
        multipoly_d[ogr_feat["id"]] = []
        qgs_feat.setGeometry(qgs_geom)
        # Get alt_class
        sup = unique[unique == ogr_feat["id"] - 1][0]  # - 1 to retrieve real class because we added +1 to id
        str_class = \
            "[%(1)s/%(2)s["%{"1": bins[sup - 1] if bins[sup] != bins[0] 
                                else "0.0",
                             "2": bins[sup] if bins[sup] != bins[-1] 
                                else "+"}
        overlap_value = round(float(overlap[unique == ogr_feat["id"] - 1]), 2)
        qgs_feat.setAttributes([ogr_feat["id"], id, str_class, overlap_value])  # - 1 to retrieve real class because we added +1 to id
        qgs_layer.dataProvider().addFeature(qgs_feat)
        qgs_layer.updateExtents()
        
    return qgs_layer, multipoly_d

def table_join(target_layer, pk_field, join_layer, fk_field, join_field=None,
               join_prefix=""):
    """
    Performs table join from one table to another table.

    Multiple layers can be used at once if list are used in arguments.

    Parameters
    ----------
    target_layer: (QgisObject) QgsVectorLayer :
        Any Geometry.
    pk_field: (String) :
        Name of the primary key field in the source layer.
    join_layer: (QgisObject) : QgsVectorLayer :
        Any Geometry.
    fk_field: (String or list) :
        Name of the foreign key field in the target layer. If multiple target
        layer the fk_field must be a list of the same length unless the same
        field is used for all layers.
    join_field: (String or list) :
        Name of fields to join. Should be a list of list for multiple layers of
        same len as join layer number.
    join_prefix: ite[str] or str if one layer : default : "".
        en of iterable should be equal to the number of layer.
    """
    # Check arguments format
    if isinstance(join_layer, list):
        pass
    else:
        join_layer = [join_layer]

    if isinstance(fk_field, list):
        pass
    else:
        fk_field = [fk_field]

    if isinstance(join_field, list):
        pass
    else:
        join_field = [[join_field]]

    if isinstance(join_prefix, list):
        pass
    else:
        join_prefix = [join_prefix] * len(join_layer)

    # if all elements in fk_field are equal
    if all(element == fk_field[0] for element in fk_field)\
            and len(fk_field) < len(join_layer):

        while len(fk_field) < len(join_layer):
            fk_field.append(fk_field[0])

    # if all elements in target_field are equal
    if all(element == join_field[0] for element in join_field) \
            and len(join_field) < len(join_layer):

        while len(join_field) < len(join_layer):
            join_field.append(join_field[0])

    # Make table join
    info = QgsVectorLayerJoinInfo()
    for layer, fk, prefix, field in \
            zip(join_layer, fk_field, join_prefix, join_field):
        info.setJoinLayerId(layer.id())
        info.setJoinFieldName(fk)
        info.setTargetFieldName(pk_field)
        info.setUsingMemoryCache(True)
        info.setPrefix(prefix)
        info.setJoinFieldNamesSubset(field)
        info.setJoinLayer(layer)
        target_layer.addJoin(info)

def refactor_fields(layer, change_map, output_type="TEMPORARY_OUTPUT"):
    """
    Refactor fields of a QgsVectorLayer given a change_map.

    Parameters
    ----------

    layer : QgsVectorLayer : Any geometry
    change_map : list :
        List of dict of form :
        [{{"expression": current name,
           "name": new or current name,
           "type": new or current type,
           "length": new or current length,
           "precision": new or current precision}]
        It is important to use current value if you do not want it to be changed
    output_type : string :
        Either a path to write the layer or TEMP.
        If default value result will be stored in cache.

    Returns
    -------
    output : QgsVectorLayer :
        Any geometry.
    """
    fields = layer.fields().toList()
    fields_map = [{"expression": field.name(),
                   "name": field.name(),
                   "type": field.type(),
                   "length": field.length(),
                   "precision": field.precision()} for field in fields]

    field_name_2b_changed = [item["expression"] for item in change_map]
    idx_2b_changed = [fields_map.index(item) for item in fields_map
                      if item["expression"] in field_name_2b_changed]

    # Update field map
    for i, idx in enumerate(idx_2b_changed):
        fields_map.pop(idx)
        fields_map.insert(idx, change_map[i])

    alg_name = "native:refactorfields"
    params = {
        "INPUT": layer,
        "FIELDS_MAPPING": fields_map,
        "OUTPUT": output_type
    }
    output = processing.run(alg_name, params)["OUTPUT"]
    if output_type != "TEMPORARY_OUTPUT":
        output = QgsVectorLayer(output, "refactorised_layer", "ogr")

    return output

def QVariant_to_field_calc_map(type):
    """
    From a QVariant object or his index 
    return the field type in QgsFieldCalc
    
    Parameters
    ----------
    type : QVariant or integer
    
    Return
    ------
    int : field type in QgsFieldCalc
    """
    field_type_map = {6 : 0, # Double
                      2: 1, # Integer
                      7: 2, # Str
                      14: 3, # Date
                      15: 4, # Time
                      16: 5, # DateTime
                      1: 6} # Boolean
                      # String type not included
   
    return field_type_map[type]

def duplicate_fields(layer, fields):
    """
    From an hedge layer (polygon, line or point) 
    will duplicate unique layer id.
    Use case : when changing hedge definition allow to keep old topological relation

    Parameters
    ----------
    layer : QgsVectorLayer
    fields : ite[QgsField]
        Name of field to be duplicate
    
    Return
    ------
    layer : QgsVectorLayer
    """
    for field in fields:
        name = field.name()
        new_name = "o_" + name
        qgs_type = QVariant_to_field_calc_map(field.type())
        layer = qw.field_calculator(layer, new_name, qgs_type, name)

    return layer

def update_old_id(layer, overlay, prefix="o_"):
    """
    From a layer fetch all the field starting with a 
    specific prefix and populate it with 
    the corresponding field without prefix in the overlay

    Parameters
    ----------
    layer : QgsVectorLayer
        Layer whose fields to update
    overlay : QgsVectorLayer
        Layer whose fields to get value from
    prefix:  str
        Prefix of the field to update
        new_name = prefix_old_name

    Return
    ------
    success : boolean
    """
    fields_map = [(fld.name().strip(prefix), layer.fields().indexFromName(fld.name())) 
                  for fld in layer.fields() if prefix in fld.name()]
    attr_map = {}
    for feature in overlay.getFeatures():
        geom = feature.geometry()
        count, features = g.get_clementini(layer, geom.buffer(0.1, 5), predicate="contains")
        results = {idx: feature[name] for (name, idx) in fields_map}
        attr_map.update({f.id(): results for f in features})

    success = layer.dataProvider().changeAttributeValues(attr_map)
    
    return success

def create_feature(fields:Union[QgsFields, None], geom:QgsGeometry, 
                    fid:Union[int, None])->QgsFeature:
    """
    Will create a new feature and populate it with at least a geometry.
    Fields can be provided too and a max_fid if needed
    """ 
    feat = QgsFeature()
    feat.setGeometry(geom)
    
    if fields is not None:
        feat.setFields(fields, True)
    if fid is not None:
        feat["fid"] = fid

    return feat