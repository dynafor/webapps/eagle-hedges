"""
Wrappers functions that use raster based algorithms.
"""
from qgis.core import (QgsRasterLayer,
                       QgsVectorLayer,
                       QgsGeometry,
                       QgsRectangle,
                       QgsProcessingFeedback,
                       QgsProcessingContext)

import processing
from typing import Union # until python 3.10 we need to use that instead of | 

def raster_neighbors(raster:QgsRasterLayer, method:int=1, 
                     kernel_size:int=7, circular:bool=False)-> QgsRasterLayer:
    """
    Perform a kernel filtering using the specified method and the specified kernel size

    Parameters
    ----------
    raster : QgsRasterLayer
        Memory raster does not work in 3.28
    method : int
		- 0: average
		- 1: median
		- 2: mode
		- 3: minimum
		- 4: maximum
		- 5: range
		- 6: stddev
		- 7: sum
		- 8: count
		- 9: variance
		- 10: diversity
		- 11: interspersion
		- 12: quart1
		- 13: quart3
		- 14: perc90
		- 15: quantile
    kernel_size : int
        Must be odd
    circular : boolean
        Use of a ciruclar kernel if True

    Return
    ------
    out_raster : QgsRasterLayer
        Filtered raster
    """
    alg_name = "grass7:r.neighbors"
    params = {"input": raster,
              "method": method,
              "size": kernel_size, # odd
              "-c": circular,
              "output": "TEMPORARY_OUTPUT"}
    uri = processing.run(alg_name, params)["output"]
    out_raster = QgsRasterLayer(uri, "mnt_med")

    return out_raster


def compute_slope(dem:QgsRasterLayer, band:int=1, 
                 as_percent:bool=True) -> QgsRasterLayer:
    """
    From a DEM compute slope raster

    Parameters
    ----------
    dem : QgsRasterLayer
    band : int
    as_percent : bool
        Return slope as percent if true, degree otherwise
    
    Return
    ------
    slope_raster : QgsRasterLayer
    """
    alg_name = "gdal:slope"
    params = {"INPUT": dem,
              "BAND": band,
              "AS_PERCENT": as_percent,
              "OUTPUT": "TEMPORARY_OUTPUT"}
    slope_raster = processing.run(alg_name, params)["OUTPUT"]

    return slope_raster


def compute_aspect(dem:QgsRasterLayer, z_factor:int=1) -> QgsRasterLayer:
    """
    From a DEM compute aspect raster

    Parameters
    ----------
    dem : QgsRasterLayer
    z_factor : int
        Height coefficient
    
    Return
    ------
    aspect_raster : QgsRasterLayer
    """
    alg_name = "native:aspect"
    params = {"INPUT": dem,
              "Z_FACTOR": z_factor,  
              "OUTPUT": "TEMPORARY_OUTPUT"}
    aspect_raster = processing.run(alg_name, params)["OUTPUT"]

    return aspect_raster


def compute_geomorphon(dem:QgsRasterLayer, search:int=100, skip:int=0, 
                       flat_thresh:int=1, flat_dist:int=0,
                       as_meters:bool=True)-> QgsRasterLayer:
    """
    Compute ground shape using geomorphon
    More at : https://grass.osgeo.org/grass82/manuals/r.geomorphon.html

    Parameters
    ----------
    dem : QgsRasterLayer
    search : int
        Search distance for shape analysis
    skip : int
        Distance where the pixel will be ignored for shape analysis
        Allow to reduce noise impact (microtopography)
    flat_thresh : int
        From grass help :
        The difference (in degrees) between zenith 
        and nadir line-of-sight which indicate flat direction. 
    flat_dist : int
        From grass hlep :
        This is additional parameter defining the distance above which 
        the threshold starts to decrease to avoid problems 
        with pseudo-flat line-of-sights 
    as_meters : bool
        If true use meters instead of pixel number for search and skip distance

    Return
    ------
    geomorphon : QgsRasterLayer
    """
    alg_name = "grass7:r.geomorphon"
    params = {"elevation": dem,
              "search": search,
              "skip": skip,
              "flat": flat_thresh,
              "dist": flat_dist,
              "-m": as_meters, # meters instead of cells for unit
              "forms": "TEMPORARY_OUTPUT"}
    uri = processing.run(alg_name, params)["forms"]
    geomorphon = QgsRasterLayer(uri)

    return geomorphon


def zonal_statistics(vector:QgsVectorLayer, raster:QgsRasterLayer, 
                     stats:list[int]=[0,1,2], band:int=1, column_prefix="_") -> QgsVectorLayer:
    """
    Perform zonal statistic on a raster using polygon features of a vector layer.

    Parameters
    ----------
    vector : QgsVectorLayer : polygons
    raster : QgsRasterLayer
    stats : list[int]
        - 0: Compte
		- 1: Somme
		- 2: Moyenne
		- 3: Médiane
		- 4: Ecart-type
		- 5: Minimum
		- 6: Maximum
		- 7: Plage
		- 8: Minorité
		- 9: Majorité
		- 10: Variété
		- 11: Variance
    band : int
    column_prefix : str

    Return
    ------
    out_vector : QgsVectorLayer : polygons
    """
    alg_name = "native:zonalstatisticsfb"
    params = {"INPUT": vector,
              "INPUT_RASTER": raster,
              "RASTER_BAND": band,
              "COLUMN_PREFIX": column_prefix,
              "STATISTICS": stats,
              "OUTPUT": "TEMPORARY_OUTPUT"}
    out_vector = processing.run(alg_name, params)["OUTPUT"]

    return out_vector


def assign_projection(raster:Union[QgsRasterLayer, str])-> None:
    """
    Assign the lambert projection (EPSG:2154) to a raster.
    This is note a reprojection and it is assumed 
    that the input raster is already in that CRS
    
    Use case : Most IGN raster have a Lambert projection 
    that is not explicitly recognized by QGIS
    
    Parameters:
    ---
    raster: QgsRasterLayer | Str
        Raster to assign the projection to. It can be an already open layer or an uri
    
    Return
    ---
    None
    """
    alg_name = "gdal:assignprojection"
    params = {"INPUT": raster,
              "CRS": "EPSG:2154"}
    processing.run(alg_name, params)


def gdal_clip_by_extent(raster:Union[QgsRasterLayer, str],
                        extent: Union[QgsGeometry, QgsRectangle], 
                        output="TEMPORARY_OUTPUT")->str:
    """
    Clip a raster by an extent
    Beware, output will automatically be overwrite.

    Parameters
    ---
    raster: Union[QgsRasterLayer, str]
        Raster can either be a QgsRasterLayer or a path
    extent: Union[QgsGeometry, QgsRectangle]
        Extent the raster should be clipped by
        It can either be a QgsGeometry, in this case 
        the bounding box will be used. Or a QgsRectangle
    output: str.
        Path of the output raster. default is a temporary file

    Return
    ---
    result: str
        Path of the processed raster
    """
    alg_name = "gdal:cliprasterbyextent"
    params = {"INPUT": raster,
              "PROJWIN": extent,
              "OUTPUT": output}

    result = processing.run(alg_name, params)["OUTPUT"]
    
    return result
    

def gdal_warp(raster:Union[QgsRasterLayer, str], 
              resolution:float, 
              common_ext: Union[QgsGeometry, QgsRectangle],
              resampling:int=0,
              output="TEMPORARY_OUTPUT")->str:
    """
    Warp a raster given a resolution and an extent
    
    Parameters
    ---
    raster: Union[QgsRasterLayer, str]
        Raster can either be a QgsRasterLayer or a path
    resolution: float
        Resolution of the pixels. It is assumed pixels are squared
    common_ext: Union[QgsGeometry, QgsRectangle]
        Extent the raster should be clipped by
    resampling: int
        Gdal sampling algorithm
            -0: Nearest Neighbour
            -1: Bilinear (2x2 Kernel)
            -2: Cubic (4x4 Kernel)
            -3: Cubic B-Spline (4x4 Kernel)
            -4: Lanczos (6x6 Kernel)
            -5: Average
            -6: Mode
            -7: Maximum
            -8: Minimum
            -9: Median
            -10: First Quartile (Q1)
            -11: Third Quartile (Q3)
    output: str.
        Path of the output raster. default is a temporary file

    Return
    ---
    result: str
        Path of the warped raster
    """
    alg_name = "gdal:warpreproject"
    params = {"INPUT":raster,
              "TARGET_RESOLUTION": resolution,
              "TARGET_EXTENT": common_ext,
              "RESAMPLING": resampling,
              "OUTPUT": output}

    result = processing.run(alg_name, params)["OUTPUT"]
    
    return result


def gdal_calc(rasters: list[tuple[Union[QgsRasterLayer, str], int]], 
              formula:str, rtype:int=None, no_data:Union[float, int]=None,
              output="TEMPORARY_OUTPUT", overwrite:bool=True, extent_opt:int=0)->str:
    """
    Performs calculation between rasters using gdal calculator.
    Gdal calc can take 6 differents raster.
    
    Parameters
    ---
    rasters:  list[tuple[Union[QgsRasterLayer, str]
        List of tuple of the input rasters and the band to use.
        If different bands of the same raster are used 
        the raster should be used twice.
        Band number start at 1.
    formula: str
        Formula used to performs calculation.
        Raster are named from A to E. 
        A is the first tuple of the input rasters and E the last.
    rtype: int
        Type of the raster in output
            - 0: Byte
            - 1: Int16
            - 2: UInt16
            - 3: UInt32
            - 4: Int32
            - 5: Float32
            - 6: Float64
    no_data : Union[float, int]
        No data value to use
    output: str.
        Path of the output raster. default is a temporary file
    overwrite: Bool
        If True will delete the output file if it exists.
    extent_opt: int
        Default to 0 -> Ignore extent difference
        - 1 -> Fail and return an error
        - 2 -> Union of rasters only
        - 3 -> Intersection of rasters
    
    Return
    ---
    result: str
        Path of the result of the calculation
    """
    alias = ["A", "B", "C", "D", "E", "F"]
    params = {}
    for i, (raster, band) in enumerate(rasters):
        param = {f"INPUT_{alias[i]}": raster, 
                 f"BAND_{alias[i]}": band,
                 "EXTENT_OPT": extent_opt}
        params.update(param)
    extra = {"FORMULA": formula,
             "OUTPUT": output}
    if bool(rtype):
        extra.update({"RTYPE": str(rtype)})
    if bool(no_data):
        extra.update({"NO_DATA": no_data})
    if overwrite:
        extra.update({"EXTRA": "--overwrite"})
    params.update(extra)
    
    alg_name = "gdal:rastercalculator"
    result = processing.run(alg_name, params)["OUTPUT"]
    
    return result

def gdal_sieve(raster:Union[QgsRasterLayer, str], threshold:int=10,
               connectedness:bool=True, output:str="TEMPORARY_OUTPUT")->str:
    """
    Sieve a raster given a sieve value. 
    8D or 4D can be considered for the object to sieve.
    Beware, output file will be overwrite if it already exists.

    Parameters
    ---
    raster: QgsRasterLayer | str
        Raster to be sieved
    threshold : int
        Max pixel size for the object to avoid being erased
    connectedness: bool
        If True, 8D will be used for the object construction
        Otherwise 4D is used.
    output: str
        Path of the output raster. default is a temporary file

    Return
    ---
    result: str
        Path of the result of the calculation
    """
    alg_name = "gdal:sieve"
    params = {"INPUT": raster,
              "THRESHOLD": threshold,
              "EIGHT_CONNECTEDNESS": connectedness,
              "OUTPUT": output}
    result = processing.run(alg_name, params)["OUTPUT"]

    return result

def gdal_merge(rasters:list[Union[QgsRasterLayer, str]], rtype:int=0, 
               no_data_input:Union[int, float]=None, 
               no_data_output:Union[int, float]=None, 
               output:str="TEMPORARY_OUTPUT")->str:
    """
    Fuse together a list of rasters.

    Parameters
    ---
    rasters: list[QgsRasterLayer | str]
        Rasters to be fused together
    rtype: int : default 0
        Data type for the output raster. Default is byte.
        Available values:
		- 0: Byte
		- 1: Int16
		- 2: UInt16
		- 3: UInt32
		- 4: Int32
		- 5: Float32
		- 6: Float64
    no_data_input: Union[int, float]
        No data to consider in input
    no_data_output: Union[int, float]
        No data value to burn in output
    output: st
        Path of the output raster. Default is a temporary file

    Return
    ---
    result: str
        Path of the merged raster
    """
    alg_name = "gdal:merge"
    params = {"INPUT": rasters,
              "RTYPE": rtype,
              "OUTPUT": output}
    if no_data_input is not None:
        params.update({"NODATA_INPUT": no_data_input})
    if no_data_output is not None:
        params.update({"NODATA_OUTPUT": no_data_output})
    result = processing.run(alg_name, params)["OUTPUT"]

    return result

def gdal_polygonize(raster:Union[QgsRasterLayer, str], band:int=1, 
                    field:str="id", connectedness:bool=False,
                    mask:bool=True, output:str="TEMPORARY_OUTPUT", 
                    feedback:Union[QgsProcessingFeedback, None]=None, 
                    context:Union[QgsProcessingContext, None]=None,
                    is_child_algorithm:bool=False)->str:
    """
    Polygonize features in a raster.

    Parameters
    ---
    raster: QgsRasterLayer | str
        Raster contianing features that'll be polygonized.
    band: int : default 1
        Band of the raster to use
    field: str : default id
        Field for the unique identifier in the vector layer.
        "Fid" is forbidden for a geopackage layer as 
        unique constraint will throw an error.
    connectedness: bool : default False
        If True will use 8D, otherwise will use 4D.
    mask: bool : default True
        If True it'll mask the pixels with a value of 0.
        If false all the pixels will be converted.
    output: str : default TEMPORARY_OUTPUT
        Path of the output raster.
    feedback:
        Communication with user
    context:
        Context of executiona and how to handle layers access
    is_child_algorithm:
        If the algorithm is executed within another algorithm

    Return
    ---
    result: str
        Path of the vector layer
    """
    alg_name = "gdal:polygonize"
    params = {"INPUT": raster,
              "BAND": band,
              "FIELD": field,
              "EIGHT_CONNECTEDNESS": connectedness,
              "OUTPUT": output}
    if mask:
        if isinstance(raster, QgsRasterLayer):
            uri = raster.dataProvider().dataSourceUri()
        else:
            uri = raster
            
        params.update({"EXTRA": f"-mask {uri}"})

    result = processing.run(alg_name, params, feedback=feedback, context=context,
                            is_child_algorithm=is_child_algorithm)["OUTPUT"]

    return result