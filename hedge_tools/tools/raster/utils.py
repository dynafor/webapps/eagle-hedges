"""
Utility functions to handle raster data.
"""
import os
import uuid
import glob
import re
import shutil
import subprocess
import sys

from typing import Tuple, Union

import numpy as np
import processing
from osgeo import gdal, ogr, osr
from qgis.core import (QgsProcessingUtils,
                       QgsRasterLayer,
                       QgsFeedback,
                       QgsRectangle,
                       QgsGeometry,
                       QgsRectangle)


def get_raster_name(path:str)->str:
    """
    From a path get the raster name
    
    Parameters
    ---
    path: str
        Path of the raster
    
    Return
    ---
        name:str
            Tail of the path will be used as raster name    
    """
    _, tail = os.path.split(path)
    name, _ = tail.split(".")
    
    return name
    

def load_raster(raster:Union[str, QgsRasterLayer])->QgsRasterLayer:
    """
    From a path load a raster inside Qgis.
    
    Parameters:
    ---
    path: str
        Raster path
        
    Return
    ---
    raster: QgsRasterLayer
        Loaded raster
    """
    if isinstance(raster, str):
        name = get_raster_name(raster)
        raster = QgsRasterLayer(raster, name, "gdal") 

    return raster
    



def fetch_intersecting_tiles(raster:Union[QgsRasterLayer, str], 
                             tiles:list[Union[QgsRasterLayer, str]])-> list[QgsRasterLayer]:
    """
    Fetch all the tile provided in rasters argument 
    that are intersecting tiles argument
    
    Parameters
    ---
    raster : QgsRasterLayer | str
        Raster to test the intersection of tiles with
    tiles: list of QgsRasterLayer | str
        Tiles that will be tested against raster
    
    Return
    ---
    intersect: list of QgsRasterLayer
    """
    intersect = []
    
    if isinstance(raster, str):
        raster = load_raster(raster)
    
    raster_extent = QgsGeometry().fromRect(raster.extent())
    
    for tile in tiles:
        if isinstance(tile, str):
            tile = load_raster(tile)
        tile_extent = QgsGeometry().fromRect(tile.extent())
        if raster_extent.intersects(tile_extent):
            intersect.append(tile)
    
    return intersect


def get_common_extent(raster_1:Union[QgsRasterLayer, str, 
                                     QgsRectangle, QgsGeometry], 
                      raster_2:Union[QgsRasterLayer, str, 
                                     QgsRectangle, QgsGeometry]
                      )->QgsGeometry:
    """
    Get the common extent between two raster like object and return it as a QgsGeometry
    
    Parameters
    ---
    raster_1: QgsRasterLayer | Str | QgsRectangle | QgsGeometry
    raster_2: QgsRasterLayer | Str | QgsRectangle | QgsGeometry
    
    Return
    ---
    common_ext: QgsGeometry
        Common extent betwwen both input raster
    """
    if isinstance(raster_1, str):
        raster_1 = load_raster(raster_1)
    if isinstance(raster_2, str):
        raster_2 = load_raster(raster_2)
    
    if isinstance(raster_1, QgsRasterLayer):
        raster_1 = raster_1.extent()
    if isinstance(raster_2, QgsRasterLayer):
        raster_2 = raster_2.extent()
    
    if isinstance(raster_1, QgsRectangle):
        raster_1 = QgsGeometry().fromRect(raster_1)
    if isinstance(raster_2, QgsRectangle):
        raster_2 = QgsGeometry().fromRect(raster_2)

    common_ext = raster_1.intersection(raster_2)
    
    return common_ext
        

def qgis_to_numpy_dtype(indtype):
    """
    Take a Qgis.DataType object and return corresponding numpy dtype.
    Note : Unknown dtype (0) and specific dtype (10 to 13) are not handled yet.

    Parameters
    ----------
    indtype : Qgs.DataType :
        Object describing data type.

    Returns
    -------
    outdtype : np.dtype :
        dtype in the numpy format.
    """
    # Init map
    map_dict = {1: np.int8,
                2: np.uint16,
                3: np.int16,
                4: np.uint32,
                5: np.int32,
                6: np.float32,
                7: np.float64,
                8: np.intc,
                9: np.intc}

    return map_dict[indtype]


def map_morph(n):
    """
    Take an int value and return a string indicating the terrain type

    Parameters
    ----------
    n: int or list :
        Class of r.geomorphon.

    Returns
    -------
    terrain_type : string :
        Topographic position in a drainage basin.
    """
    map_dict = {-1: "No_data",
                1: "Flat",
                2: "Peak",
                3: "Ridge",
                4: "Shoulder",
                5: "Spur",
                6: "Slope",
                7: "Hollow",
                8: "Footslope",
                9: "Valley",
                10: "Pit",
                255: "No_data"}

    return map_dict[n]


def bounding_box_to_offsets(bbox, geot):
    """
    Given a gdal geotransform object,
    compute the position in the input raster of the feature.

    Parameters
    ----------
    bbox : tuple :
        Coordinates tuple of a osgeo.ogr.Geometry.
    geot : tuple :
        Geotransform (resolution and top left corner coordinate)
        of a osgeo.gdal.Dataset.

    Returns
    -------
    row1, row2, col1, col2 : list(int) :
        Relative position of the vector feature in the raster.
    """
    col1 = int((bbox[0] - geot[0]) / geot[1])
    col2 = int((bbox[1] - geot[0]) / geot[1]) + 1
    row1 = int((bbox[3] - geot[3]) / geot[5])
    row2 = int((bbox[2] - geot[3]) / geot[5]) + 1
    return [row1, row2, col1, col2]


def geot_from_offsets(row_offset, col_offset, geot):
    """
    Compute a geotransform (resolution and top left corner coordinates)
    from a raster, a geotransform and an object relative position in a raster.

    Parameters
    ----------
    row_offset : int :
        Relative position of the top left of the bounding box object
        in the raster rows.
    col_offset : int :
        Relative position of the top left of the bounding box object
        in the raster cols.
    geot : tuple :
        Geotransform (resolution and top left corner coordinate)
        of a osgeo.gdal.Dataset (input raster).

    Returns
    -------
    new_geot : tuple :
        Geotransform representing the position (coordinates) of the object.
    """
    new_geot = [geot[0] + (col_offset * geot[1]),
                geot[1],
                0.0,
                geot[3] + (row_offset * geot[5]),
                0.0,
                geot[5]]
    return new_geot


def qgis_raster_to_gdal(raster):
    """
    From a raster layer inside qgis retrieve the uri and open it in gdal as a ds

    Parameters
    ----------
    raster : QgsRasterLayer

    Return
    ------
    ds : GDALDatasetShadow
    """
    raster_path = raster.dataProvider().dataSourceUri()
    ds = gdal.Open(raster_path)

    return ds


def get_raster_metadata(dataset):
    """
    From a GDALDatasetShadow get Datatype, projection, 
    no data value and geotransform

    Parameters
    ----------
    dataset : GDALDatasetShadow

    Return
    ------
    geot : tuple
    projection : string
    dtype : int
    no_data : int or float
    """
    geot = dataset.GetGeoTransform()
    projection = dataset.GetProjection()
    dtype = dataset.GetRasterBand(1).DataType
    no_data = dataset.GetRasterBand(1).GetNoDataValue()

    return geot, projection, dtype, no_data


def write_image(out_filename, array, data_set=None, transform=None, 
                projection=None, gdal_dtype=None, no_data=None, memory=False, 
                output=True):
    """
    Write numpy array as GeoTiff or in memory.

    Parameters
    ----------
    out_filename : str
        Path of the output raster.
        If in memory is True it will be ignored or you can use and empty string.
    array : numpy.ndarray
        Array to write.
    data_set : osgeo.gdal.Dataset
        Gdal object used to get dtype, driver, projection.
        (Default value = None)
    transform : tuple
        Pixel size and extent of the raster
    projection : string
        Crs of the raster
    gdal_dtype : int
        Gdal data type, if unregistered it will take the dataset data type.
        (Default value = None)
    no_data : int
        No data value. (Default value = None)
    memory ; boolean
        If true will write in a memory raster.
        (Default value = False)

    Returns
    -------
    if output is True: return the output_dataset
        

    """

    # Dimensions de l'image
    nb_col = array.shape[1]
    nb_ligne = array.shape[0]
    array = np.atleast_3d(array)
    nb_band = array.shape[2]

    # Informations à récupérer sur le data_set
    transform = transform if transform is not None else data_set.GetGeoTransform()
    projection =  projection if projection is not None else data_set.GetProjection()
    gdal_dtype = gdal_dtype if gdal_dtype is not None \
        else data_set.GetRasterBand(1).DataType
    
    try:
        driver_name = data_set.GetDriver().ShortName if memory is False else "MEM"
    except AttributeError:
        driver_name = "MEM" if memory else "GTiff"

    out_filename = out_filename if memory is False else ""
    # Initialisation de l'image en sortie
    driver = gdal.GetDriverByName(driver_name)
    output_data_set = driver.Create(out_filename, nb_col, nb_ligne, nb_band,
                                    gdal_dtype)
    output_data_set.SetGeoTransform(transform)
    output_data_set.SetProjection(projection)

    # Remplissage et écriture de l'image
    for idx_band in range(nb_band):
        output_band = output_data_set.GetRasterBand(idx_band + 1)
        output_band.WriteArray(array[:, :, idx_band])

        if no_data is not None:
            output_band.SetNoDataValue(no_data)
        output_band.FlushCache()

    # Fermeture des données en entrée
    del(output_band)
    if output:
        return output_data_set
    else:
        del(output_data_set)


def gdal_polygonize(data_set=None, band=None, mask=None, proj=None, 
                    dst_field="id", connectness=8):
    """
    Polygonize a raster with gdal
    If no band is indicated it is assumed the first band of the ds is to use

    Parameters
    ----------
    data_set : GDALDatasetShadow
        Used to get metatdata if not directly indicated
    band : GDALRasterBandShadow
        Band to polygonize
    mask : GDALRasterBandShadow
        Mask band, all pixels different than 0 will be polyginize
    proj : String
        Crs of the dst_layer
    dst_field : Int
        Field where pixel value is written
    connectness : Int
        4 or 8 connectness
    """
    if connectness == 8:
        connectness = ["8CONNECTED=8"]
    else:
        connectness = ["8CONNECTED=4"]

    proj = proj if proj is not None else data_set.GetProjection()
    band = band if band is not None else data_set.GetRasterBand(1)
    mask = mask if mask is not None else None

    # Create out layer and populate
    dst_layername = str(uuid.uuid4())[0:3]
    drv = ogr.GetDriverByName("MEMORY")
    dst_ds = drv.CreateDataSource("")

    sp_ref = osr.SpatialReference()
    sp_ref.ImportFromWkt(proj)

    dst_layer = dst_ds.CreateLayer(dst_layername, srs=sp_ref)

    fld = ogr.FieldDefn("id", ogr.OFTInteger)
    dst_layer.CreateField(fld)
    dst_field = dst_layer.GetLayerDefn().GetFieldIndex("id")

    # Polygonize
    gdal.Polygonize(band, mask, dst_layer,
                    dst_field, connectness, callback=None)
    # Return ds to avoid gdal and ogr gotcha
    return dst_layer, dst_ds


def raster_as_array(inraster):
    """
    Take a QgsRasterLayer and return a numpy ndarray containing the value.
    Note : QgsRasterCalculator have better performance for doing band operation.
    Array is for specific operation or visualisation.

    Parameters
    ----------
    inraster: QgisObject : QgsRasterLayer :
        Raster layer.

    Returns
    -------
    outarray : numpy.ndarray :
        Numpy array of the input raster.
    """
    # Get raster metadata as converting to array remove all shape information
    row = inraster.height()
    col = inraster.width()
    extent = inraster.extent()
    band_number = inraster.bandCount()
    qgis_dtype = inraster.dataProvider().block(1, extent, col, row).dataType()
    np_dtype = qgis_to_numpy_dtype(qgis_dtype)

    # Create empty output array
    outarray = np.empty(shape=(row, col, band_number), dtype=np_dtype)

    # We got raster shape. We want to get the data, but it's serialized.
    # First go get the bytes
    for band in range(0, band_number):
        block = inraster.dataProvider().block(band + 1, extent, col, row)
        # Ok so we have a serialized bytes array and is shape.
        # We want to unserialize it and put back the shape
        deserialized_bytes = np.frombuffer(block.data(), dtype=np_dtype)
        array = np.reshape(deserialized_bytes, newshape=(row, col))
        # Then we want to write the unserialized array in the output in the correct band
        outarray[:, :, band] = array

    return outarray


def get_count(array):
    """
    Take an array containing r.geomorphon output value for a feature
    and return the max occurence.

    Parameters
    ----------
    array: numpy.ndarray :
        Array containing geomorphon class for an hedge.

    Returns
    -------
    max_occ : list or int :
        Max occurence of geomorphon class.
    """
    val, count = np.unique(array, return_counts=True)
    max_occ = val[count == np.max(count)]
    if len(max_occ) > 1:
        return list(max_occ)
    else:
        return int(max_occ)


def rasterize_and_clip(vector, raster):
    """
    Clip one raster by the other to have the same extents in both.
    Note : Both raster should have the same resolution.

    Parameters
    ----------
    vector : QgsVectorLayer :
        Any geometry.
    raster : QgsRasterLayer :
        Raster image.

    Returns
    -------
    out_raster_vector : QgsRasterLayer :
        Rasterised vector layer.
    out_other_raster : QgsRasterLayer :
        Clipped raster image.
    """
    resolution_x = raster.rasterUnitsPerPixelX()
    resolution_y = raster.rasterUnitsPerPixelY()

    # Convert poly_layer to raster layer using gdal
    alg_name = "gdal:rasterize"
    params = {"INPUT": vector,
              "FIELD": "pid",
              "UNITS": 1,  # CRS units
              "WIDTH": resolution_x,
              "HEIGHT": resolution_y,
              "EXTENT": vector.extent().buffered(resolution_x + resolution_y),  # to Get geoemorphon to compute edges
              "NODATA": 0,
              "DATA_TYPE": 1,  # Int16
              "OUTPUT": "TEMPORARY_OUTPUT"}
    output = processing.run(alg_name, params)["OUTPUT"]
    raster_vector = QgsRasterLayer(output)

    if raster_vector.extent().area() < raster.extent().area():
        alg_name = "gdal:cliprasterbyextent"
        params = {"INPUT": raster,
                  "PROJWIN": raster_vector.extent(),
                  "OUTPUT": "TEMPORARY_OUTPUT"}
        output = processing.run(alg_name, params)["OUTPUT"]
        raster = QgsRasterLayer(output)

    return raster_vector, raster


def check_intersect_raster_vector(path_raster:str, vector_extent:QgsRectangle)->bool:
    """
    Performs an intersection between a raster extents and a vector extents

    Parameters
    ----------

    path_raster:
        Raster uri to fetch extent in
    vector_extent:
        Extent of the vector layer
    
    Returns
    -------
    inter:
        True if both extent intersects else False

    """
    raster = QgsRasterLayer(path_raster, "tile", "gdal")
    raster_extent = raster.extent()
    
    inter = vector_extent.intersects(raster_extent)
    
    return inter
    
    
def get_rasters_intersect_vector(extent:QgsRectangle, path_dir_rasters:str, path_dir_copy:str)->list[str]:
    """
    Fetch the raster inside a folder and copy the ones that intersect the extent of a vector layer

    Parameters
    ----------
    extent: 
        Layer extent
    path_dir_rasters:
        Path of the folder to check
    path_dir_copy_dsm:
        Path of the output directory where the intersected raster will be copied

    Returns
    -------
    list_rasters_copied:
        Pathes of the copied rasters.
    """
    
    list_path_rasters = glob.glob(path_dir_rasters + "/*.tif")
    
    if not list_path_rasters:
        
        list_path_rasters = glob.glob(path_dir_rasters + "/*.TIF")
        
        if not list_path_rasters:
                
            list_path_rasters = glob.glob(path_dir_rasters + "/*.asc")
                
            if not list_path_rasters:
                        
                list_path_rasters = glob.glob(path_dir_rasters + "/*.jp2")
    
    if os.path.splitext(list_path_rasters[0])[1] == ".TIF":

        exts = [".grf", ".HDR", ".TFW", ".TIF"]
        
        list_path_rasters = [os.path.splitext(path)[0] 
                            for path in list_path_rasters]
    
        list_rasters_copied = \
            [[shutil.copy(src = path_raster + ext,
                          dst = path_dir_copy + "/" + \
                                os.path.basename(path_raster) + ext)
              for ext in exts
              if os.path.exists(path_raster + ext)]
             for path_raster in list_path_rasters
             if check_intersect_raster_vector(path_raster = path_raster + \
                                                            exts[3],
                                              vector_extent = extent)]
    else:
        
        list_rasters_copied = \
            [shutil.copy(src = path_raster,
                         dst = path_dir_copy + "/" + \
                               os.path.basename(path_raster))
             for path_raster in list_path_rasters
             if check_intersect_raster_vector(path_raster = path_raster,
                                              vector_extent = extent)]
    
    return list_rasters_copied


def manage_raster_files_extraction_tools(extent:QgsRectangle,
                                         path_dir_dsm:str, path_dir_copy_dsm:str,
                                         path_dir_dem:str, path_dir_copy_dem:str,
                                         path_dir_ortho:str, path_dir_copy_ortho:str,
                                         feedback: QgsFeedback):
    """
    For each input raster folder check the raster that intersects the layer extents 
    and copy them in an output directory

    Parameters
    ----------
    extent: 
        Layer extent
    path_dir_dsm:
        Path of the DSM folder to check
    path_dir_copy_dsm:
        Path of the output directory where the intersected DSM will be copied
    path_dir_dem:
        Path of the DEM folder to check
    path_dir_copy_dem:
        Path of the output directory where the intersected DEM will be copied
    path_dir_ortho:
        Path of the ortho folder to check
    path_dir_copy_ortho:
        Path of the output directory where the intersected ortho will be copied
    feedback: 
        QgsFeedback for progress bar and users communication
    
    Returns
    -------
    None
    """
    
    alg_number = 0
    
    if path_dir_dsm != '':
        alg_number += 1
       
    if path_dir_dem != '':
        alg_number += 1
        
    if path_dir_ortho != '':
        alg_number += 1
        
    # set the number of step of the algorithm    
    step_per_alg = int(100/alg_number)
    
    feedback.pushInfo("Starting processing")
    
    if path_dir_dsm != '':
        
        feedback.pushInfo("DSM raster tiles copy")
        
        _ = get_rasters_intersect_vector(extent,
                                         path_dir_rasters = path_dir_dsm,
                                         path_dir_copy = path_dir_copy_dsm)
    
        feedback.setProgress(step_per_alg)
        
    if path_dir_dem != '':
    
        feedback.pushInfo("DEM raster tiles copy")
    
        _ = get_rasters_intersect_vector(extent,
                                         path_dir_rasters = path_dir_dem,
                                         path_dir_copy = path_dir_copy_dem)
    
        feedback.setProgress(step_per_alg)
        
    if path_dir_ortho != '':
    
        feedback.pushInfo("Ortho IRC raster tiles copy")
    
        _ = get_rasters_intersect_vector(extent,
                                         path_dir_rasters = path_dir_ortho,
                                         path_dir_copy = path_dir_copy_ortho)
        
        feedback.setProgress(step_per_alg)