import numpy as np

def create_bins(r_array, tr_array, bins=[0.3, 2, 7], no_data=-9999):
    """
    From a raster array and a vector array representing our feature 
    create a masked array.
    Value outside the vector feature are considered as no_data
    Value inside the feature are categorized given the bins value

    Parameters
    ----------
    r_array : numpy.array : 
        Raster data to categorize
    tr_array : numpy.array : 
        Mask of our feature
    bins : ite[float]
    no_data : int

    Return
    ------
    mask_ar : numpy.ma.MaskedArray
    """
    ind = np.digitize(r_array, bins)
    mask_ar = np.ma.MaskedArray(ind, 
                                mask=np.logical_or(r_array == no_data,
                                        np.logical_not(tr_array)))
    
    return mask_ar

def overlap_by_category(mask_ar):
    """
    From a catgorized masked array, compute the overlap by category

    Parameters
    ----------
    mask_ar : numpy.ma.MaskedArray

    Return
    ------
    unique : numpy.array :
        Unique value of the mask_ar 
    count : numpy.array : 
        Count of each unique value
    overlap : numpy.array : 
        Overlap percentage between a bin and the feature
    """
    unique, count = np.unique(mask_ar[~mask_ar.mask],
                                return_counts=True)
    overlap = (count / count.sum()) * 100

    return unique, count, overlap

def populate_attr_map(attr_map, overlap, unique, fields, idx_list, id):
    """
    Use case : strata_overlap_from_DHM

    From an array showing the unique value and the corresponding 
    overlap array, compute the number of strata and the dominant strata

    This function is very specific and will only work in this case

    Parameters
    ----------
    attr_map : dict
    overlap : numpy.array : 
        Overlap percentage between a bin and the feature
    unique : numpy.array : 
        Unique value of the mask_ar 
    fields : QgsFields
    idx_list : list : 
        Field index of indicators field.
    id : int : 
        Feature id in the vector layer

    Return
    ------
    poly_attr_map : dict : Updated with the values for the corresponding feature
    """
    fld_dict = {}
    for idx, idx_f in enumerate(idx_list):
        fld_dict[idx_f] = float(round(overlap[unique == idx][0], 2)) \
                            if len(unique[unique == idx]) != 0 else 0
        if fields.names()[idx_f] == "nb_strata":
            fld_dict[idx_f] = \
                len([v for v in list(fld_dict.values())[0:idx] 
                     if v != 0])
        elif fields.names()[idx_f] == "dom_strata":
            fld_dict[idx_f] = \
                [fields.names()[k] for k, v in fld_dict.items()
                 if v == max(fld_dict.values())][0]
            
    attr_map[id] = fld_dict

    return attr_map