import os

def create_folders(workspace:str, folders:list[str])->None:
    """
    Will create subdirectory inside the workspace. 
    Subdirectory will be given the name given by users 
    inside the folders_name list.
    
    Parameters
    ---
    workspace : str
        Path of the workspace where the folders will be created
    folders_name: list[str]
        Name of the folders that will be created
    """
    for folder in folders:
        path = os.path.join(workspace, folder)
        if not os.path.exists(path):
            os.mkdir(path)

def is_file_exists(path:str)->bool:
    """
    Check if a file exsits.

    Parameters:
    ---
    path: Path of the file to checks

    Return
    ---
    exist: bool
        True if the file exists, false otherwise
    """
    exist = False
    if os.path.exists(path):
        exist = True
    
    return exist

def concat(a:int, b:int)->int:
    """
    Concatenate two int together

    Parameters
    ---
    a: int
    b: int

    Return
    ---
    int
    """
    return int(f"{a}{b}")